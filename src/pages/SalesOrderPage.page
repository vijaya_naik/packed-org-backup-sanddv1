<!--*******************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*-->
<!--**
* @author ET Marlabs
* @date 20-Oct-2015
* @description This page is Sales Order main page
*-->
<apex:page showHeader="true" sidebar="false" controller="SalesOrderController" standardStylesheets="false" tabStyle="Sales_Order__c">
    <html lang="en-us" ng-app="App.SalesOrderManager">
       <head>
            
            <title>{!HTMLENCODE($Label.Sales_Order)}</title>
            <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
            <meta charset="UTF-8"/>
            
            <!-- load sf specific bootstrap via static resource -->
            <apex:stylesheet value="{!URLFOR($Resource.ExternalCustomLib,'SF_Hacked_Bootstrap-3.3.4/css/bootstrap.min.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.ExternalLib,'AngularMaterial0.11.4/css/angular-material.min.css')}" />

            <!--load custom css via static resource>-->
            <apex:stylesheet value="{!URLFOR($Resource.Styles,'/style/customStyle.css')}" />
         <apex:stylesheet value="{!URLFOR($Resource.SalesOrder,'/salesOrder/salesOrderCustomStyle.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.ExternalLib,'/AngularUiNotification/angular-csp.css')}" />
            <apex:stylesheet value="{!URLFOR($Resource.ExternalLib,'/AngularUiNotification/angular-ui-notification.min.css')}" />
                     
            <!-- load angular and various other angular services via static resource -->
            <script src="{!URLFOR($Resource.ExternalLib,'/Angular1.4.4/angular.min.js')}" />
            <script src="{!URLFOR($Resource.ExternalLib,'/AngularMaterial0.11.4/js/angular-material.min.js')}" />
            <script src="{!URLFOR($Resource.ExternalLib,'/Angular1.4.4/angular-animate.min.js')}" />
            <script src="{!URLFOR($Resource.ExternalLib,'/Angular1.4.4/angular-aria.min.js')}" />
            <script src="{!URLFOR($Resource.ExternalLib,'/Angular1.4.4/angular-messages.min.js')}" />
            <script src="{!URLFOR($Resource.ExternalLib,'/Angular1.4.4/angular-resource.min.js')}" />
            <script src="{!URLFOR($Resource.ExternalLib,'/Angular1.4.4/angular-route.min.js')}" />
            <script src="{!URLFOR($Resource.ExternalLib,'/AngularUiNotification/angular-ui-notification.min.js')}" />

            <!--load angular bootstrap js via static resource-->
            <script src="{!URLFOR($Resource.ExternalLib, '/UiBootstrap1.0.3/ui-bootstrap-tpls-1.0.3.min.js')}" />
            
            <!--load underscore js library via static resource-->
            <script src="{!URLFOR($Resource.ExternalLib, '/Underscore1.8.3/underscore-min.js')}" />
            
            <!--load app specific js and css via static resource-->
            <script src="{!URLFOR($Resource.SalesOrder, '/salesOrder/salesOrder.js')}" />
            <script src="{!URLFOR($Resource.SalesOrder, '/salesOrder/salesOrderFactory.js')}" />
            <script src="{!URLFOR($Resource.SalesOrder, '/salesOrder/salesOrderMainController.js')}" />
            <script src="{!URLFOR($Resource.SalesOrder, '/salesOrder/salesOrderNewController.js')}" />
            <script src="{!URLFOR($Resource.SalesOrder, '/salesOrder/salesOrderEditController.js')}" />    
            <script src="{!URLFOR($Resource.Common, '/utilities/validations.js')}" />
            <script src="{!URLFOR($Resource.Common, '/utilities/stopEnterKey.js')}" />
            
            <script>
               
                //This is bridging for passing Custom Labels to the JS and can not be moved to any 
                //static resource file.
                window.$Label = window.$Label || {};
                window.$Constant = window.$Constant || {};
                $Constant.accId = '{!JSENCODE($CurrentPage.parameters.accId)}'; 
                $Constant.retUrl = '{!JSENCODE($CurrentPage.parameters.retUrl)}'; 
                $Constant.editPage = '{!JSENCODE($CurrentPage.parameters.edit)}'; 
                $Constant.newPage = '{!JSENCODE($CurrentPage.parameters.new)}'; 
                $Constant.salesOrderId = '{!JSENCODE($CurrentPage.parameters.id)}';
           		$Constant.fetchAccountsList = '{!JSENCODE($RemoteAction.SalesOrderController.fetchAccountsList)}';
           		$Constant.fetchShippingList = '{!JSENCODE($RemoteAction.SalesOrderController.fetchShippingList)}';
           		$Constant.fetchPriceBooks = '{!JSENCODE($RemoteAction.SalesOrderController.fetchPriceBooks)}';
           		$Constant.fetchPriceBookEntries = '{!JSENCODE($RemoteAction.SalesOrderController.fetchPriceBookEntries)}';
           		$Constant.saveSalesOrder = '{!JSENCODE($RemoteAction.SalesOrderController.saveSalesOrder)}';
           		$Constant.fetchSalesOrderRecords = '{!JSENCODE($RemoteAction.SalesOrderController.fetchSalesOrderRecords)}';
           		$Constant.fetchPriceBookEntriesSoli = '{!JSENCODE($RemoteAction.SalesOrderController.fetchPriceBookEntriesSoli)}';
           		$Constant.fetchAccount = '{!JSENCODE($RemoteAction.SalesOrderController.fetchAccount)}';

            </script>
                 
       </head>
        
        <div class="nm soMainDiv" ng-controller="SalesOrderMainController">
            
            <div >
                <div class="ng-view "></div>    
            </div>

            <!-- Loading icon section-->
            <div class="loadingSection" ng-show="loading">
                <div class="sfloadingBackground" ></div>
                <div class="sfloadingIcon">
                    <apex:image url="{!$Resource.LoadingIcon}" title="{!HTMLENCODE($Label.Please_Wait)}"/>
                </div>
            </div> 
              
        </div>
    </html> 
    
</apex:page>