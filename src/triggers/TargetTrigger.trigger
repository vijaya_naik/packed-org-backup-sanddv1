/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 6/4/2015
* @description Trigger on Target object
*/

trigger TargetTrigger on Target__c (after insert, after update,before insert,before update) {

    //Check for the events
    if( (Trigger.isAfter && Trigger.isInsert) || (Trigger.isAfter && Trigger.isUpdate)){
        System.debug('##Trigger Invoked');
        //This is a helper class to give read access to the user selected in the Assigned To field.
        TargetTriggerHelper.AssignTargetToAssignee(Trigger.new, Trigger.oldMap);
        
    }

    if(Trigger.isBefore && Trigger.isInsert){   
        TargetTriggerHelper.taregetUniqueIdentifier(Trigger.new);       
    }

    if(Trigger.isBefore && Trigger.isUpdate){   
        TargetTriggerHelper.onBeforeUpdate(Trigger.new, Trigger.oldMap);  
    }
}