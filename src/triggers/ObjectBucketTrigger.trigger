/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs)
* @date 8/6/2015
* @description This trigger 
*/
trigger ObjectBucketTrigger on System_Object_Bucket__c (before insert,before update) {
    
    List<System_Object_Bucket__c> objBucketList = new List<System_Object_Bucket__c>();
    
    if(Trigger.isInsert || Trigger.isUpdate) {
        /*checkHierarchyBlock method is used to check bucket hierarchy is locked or not.If locked then show message
          otherwise insert or update object bucket records.*/
        objBucketList = BucketSharingHandler.checkHierarchyBlock(trigger.new);
    }

    if(Trigger.isInsert && Trigger.isBefore) {
        if(!objBucketList.isempty()) {
            BucketSharingHandler.onInsert(objBucketList,true);
        }
    }

    if(Trigger.isUpdate && Trigger.isBefore) {
        if(!objBucketList.isempty()) {
            BucketSharingHandler.onUpdate(objBucketList,Trigger.oldMap);
        }
    }
}