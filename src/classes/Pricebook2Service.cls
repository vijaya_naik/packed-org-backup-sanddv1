/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a service Facade
*/
public with sharing class Pricebook2Service {
  

  /*******************************************************************************************************
    * @description : method to retrive pricebook entries based on pricebook name
    * @param : priceBookName - priceBookName name to query
    * @return List of Pricebook2. 
    */
    
    public static List<Pricebook2> fetchPriceBooks(String priceBookName){

        String startsWith = priceBookName + '%';

        if(priceBookName == null){

            return [SELECT Id,IsStandard,Name FROM Pricebook2 WHERE IsActive = true Order By Name LIMIT 10];
        }
        else{

            return [SELECT Id,IsStandard,Name FROM Pricebook2 WHERE IsActive = true AND Name LIKE :startsWith Order By Name];  
        }
        

    } 
}