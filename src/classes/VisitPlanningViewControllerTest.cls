/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date Aug 2015
* @description This class is controller for VisitPlanning Page.
*/
@isTest
private class VisitPlanningViewControllerTest {
    
    /*******************************************************************************************************
* @description: Setting up test data for the test class(Non Setup Objects).
*  
*/
    @testSetup static void visitDataInit() {
        
        list<Account> accList                     = new list<Account>();
        list<SandDV1__period__c> periodList                = new list<SandDV1__Period__c>();
        list<SandDV1__Route__c> routeList                  = new list<SandDV1__Route__c>();
        list<SandDV1__Route_Account__c> routeAccountList   = new list<SandDV1__Route_Account__c>();
        Date todayDate                            = Date.today();
        
        User adminUser = VisitPlanTestData.testAdminUser();
        insert adminUser;
        
        User standardUser = VisitPlanTestData.testStandardUser(adminUser.Id);
        insert standardUser;
        system.assertEquals(standardUser.ManagerID,adminUser.Id);
        
        for(integer i=0; i < 200; i++) {
            Account acc = VisitPlanTestData.testAccount(i);
            acc.SandDV1__Account_Category__c = 'Cold';
            accList.add(acc);
        }
        insert accList;
        
        for(integer i=0; i < 10; i++) {
            Boolean b       = true;//(Math.mod(i,2) == 0 ? true : false);
            Date startDate  = todayDate;
            Date endDate    = startDate.addDays(1 * i);
            periodList.add(VisitPlanTestData.testPeriod(b, startDate, endDate));
        }
        insert periodList;
        
        for(integer i=0; i < 10; i++) {
            Boolean b = true;//(Math.mod(i,2) == 0 ? true : false);
            Date startDate  = todayDate.addDays(i);
            Date endDate    = startDate.addDays(1 * i);
            routeList.add(VisitPlanTestData.testroute(b));
        }
        insert routeList;
        
        for(integer i=0; i < 10; i++) {
            Boolean b = true;//(Math.mod(i,2) == 0 ? true : false);
            Date startDate  = todayDate.addDays(i);
            Date endDate    = startDate.addDays(1 * i);
            routeAccountList.add(VisitPlanTestData.testRouteAccount(routeList[i].Id, accList[i].Id, b));
        }
        insert routeAccountList;
        
        new VisitPlanningController(new ApexPages.StandardController(new SandDV1__Visit_Plan__c()));
    }
    
    /*******************************************************************************************************
* @description: Setting up Holiday test data for the test class(setup Objects).
*  
*/
    @testSetup static void visitDataInit2() {
        
        Date todayDate                            = Date.today();
        Holiday holidayTest    = VisitPlanTestData.testHoliday(todayDate);
        Holiday holidayTest2   = VisitPlanTestData.testHoliday(todayDate.addDays(4));
        Holiday holidayTest3   = VisitPlanTestData.testHoliday(todayDate.addDays(4));
        Holiday [] holidayList = new list<Holiday> {holidayTest, holidayTest2, holidayTest3};
            insert holidayList; 
        System.assertEquals(holidayList.size(), 3);
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate Period picklist.
*  
*/
    private static testMethod void retreiveActivePeriodTest() {
        
        Test.startTest();
        list<SandDV1__Period__c> periodList   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true]; 
        list<SandDV1__Period__c> periodValues = VisitPlanningController.retreivePeriods();
        system.assertEquals(periodList, periodValues);
        Test.stopTest();
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate Route picklist.
*  
*/  
    private static testMethod void retreiveActiveRoutesTest() {
        
        Test.startTest();
        list<SandDV1__Route__c> routeList   = [select Id, Name from SandDV1__Route__c where SandDV1__Active__c = true]; 
        list<SandDV1__Route__c> routeValues = VisitPlanningViewController.retreiveRoutes();
        system.assertEquals(routeList, routeValues);
        Test.stopTest();
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate Account Category picklist.
*  
*/  
    private static testMethod void retreiveAccountCategoryTest() {
        Test.startTest();
        list<selectOption> periodvalues = VisitPlanningViewController.retreiveAccountCategory();
        system.assertNotEquals(periodvalues, null);
        Test.stopTest();
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to populate selected Route related Accounts.
*  
*/ 
    private static testMethod void retreiveRouteAccountsTest() {
        
        Test.startTest();
        map<id,string> customerMap = new map<id, string>();
        map<id,string> customerResMap = new map<id, string>();
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true limit 1];
        SandDV1__Route__c routeInstance     = [select Id, Name from SandDV1__Route__c where SandDV1__Active__c = true limit 1];
        
        for(SandDV1__Route_Account__c routeAcc : [select SandDV1__Account__c, SandDV1__Account__r.Name from SandDV1__Route_Account__c where SandDV1__Route__c =: routeInstance.Id and SandDV1__Active__c = true]) {
            customerMap.put(routeAcc.SandDV1__Account__c, routeAcc.SandDV1__Account__r.Name);
        }
        SandDV1__Visit_Plan__c visitPlan = new SandDV1__visit_Plan__c(SandDV1__Period__c = periodInstance.Id, SandDV1__User__c = userInfo.getUserId());
        insert visitPlan;
        
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningViewController.retreiveAccounts(periodInstance.Id, routeInstance.Id, 'Cold', visitPlan.Id);
        
        for(VisitPlanningModel.CustomerWrapper cusWrap : resWrap.customerValues) {
            customerResMap.put(cusWrap.customerId, cusWrap.customerName);
        }
        system.assertEquals(customerMap, customerResMap);
        system.assertEquals(customerResMap.keySet().containsAll(customerMap.keySet()), true);
        system.assertEquals(customerResMap.Values(), customerMap.Values());
        Test.stopTest();
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality to retrieve all Accounts(no Route is selected) in the org.
*  
*/
    private static testMethod void retreiveAllAccountsTest() {
        
        map<id,string> customerMap = new map<id, string>();
        map<id,string> customerResMap = new map<id, string>();
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true limit 1];
        
        for(Account acc : [select id, Name from Account]) {
            customerMap.put(acc.Id, acc.Name);
        }
        
        SandDV1__Visit_Plan__c visitPlan = new SandDV1__visit_Plan__c(SandDV1__Period__c = periodInstance.Id, SandDV1__User__c = userInfo.getUserId());
        insert visitPlan;
        
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningViewController.retreiveAccounts(periodInstance.Id, Label.Select_Route, 'Cold', visitPlan.Id);
        
        for(VisitPlanningModel.CustomerWrapper cusWrap : resWrap.customerValues) {
            customerResMap.put(cusWrap.customerId, cusWrap.customerName);
        }
        system.assertEquals(customerMap, customerResMap);
        system.assertEquals(customerResMap.keySet().containsAll(customerMap.keySet()), true);
        List<string> customerRes = new list<String>();
        customerRes.addAll(customerResMap.Values());
        customerRes.sort();
        List<string> customerMap1 = new list<String>();
        customerMap1.addAll(customerMap.Values());
        customerMap1.sort();
        system.assertEquals(customerRes, customerMap1);
    }
    
    /*******************************************************************************************************
* @description: Testing the functionality if the Visit Plan is Saved to Draft(Planning).
*  
*/  
    private static testMethod void saveVisitPlan_Test() {
        
        Date todayDate = Date.Today().addDays(8);
        SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c,SandDV1__Active__c,SandDV1__Visit_Plan_Visible__c  from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true and SandDV1__End_Date__c =: todayDate limit 1];
        SandDV1__Visit_Plan__c visitPlan = new SandDV1__visit_Plan__c(SandDV1__Period__c = periodInstance.Id, SandDV1__User__c = userInfo.getUserId());
        insert visitPlan;
        SandDV1__Route__c routeInstance = [select Id from SandDV1__Route__c limit 1];
        VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningViewController.retreiveAccounts(periodInstance.Id, Label.Select_Route, 'Cold', visitPlan.Id);
        system.debug(periodInstance+'periodInstanceperiodInstanceperiodInstanceperiodInstance');
        for(VisitPlanningModel.CustomerWrapper res : resWrap.customerValues) {
            for(VisitPlanningModel.VisitPlanWrapper visitWrap : res.visitPlanWrapperList) {
                if(res.customerName == 'Test Visit Plan Account0') {
                    if(date.parse(visitWrap.dateValue) == Date.today()) {
                        visitWrap.IsVisitPlanned = true;
                    } else if(date.parse(visitWrap.dateValue) == Date.today().addDays(10)) {
                        visitWrap.IsVisitPlanned = true;
                    }
                }
            }
        }
        system.assertEquals(periodInstance.SandDV1__Active__c, true);
        system.assertEquals(periodInstance.SandDV1__Visit_Plan_Visible__c , true);
        SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = true;
        SandDV1__Visit_Plan__c boolRes = VisitPlanningViewController.saveVisitPlan(resWrap, visitPlan.Id, 'Planning', new list<String>());
        VisitPlanningViewController.retreiveVisitPlan(visitPlan.Id);
        Apexpages.StandardController stdCon = new Apexpages.StandardController(boolRes);
        VisitPlanningViewController vpCon = new VisitPlanningViewController(stdCon);
        VisitPlanningViewController.retreiveAccounts(periodInstance.Id, routeInstance.Id, 'Cold', visitPlan.Id);
    }
    
    
    
    /*******************************************************************************************************
* @description: Testing the functionality if the Visit Plan is Saved to Draft(Planning).
*  
*/  
/*
    private static testMethod void saveVisitPlan_SubmittedWithManager() {
        
        Date todayDate = Date.Today().addDays(8);
        User u    = [sELECT Id,IsActive FROM User WHERE ManagerId != null And IsActive =true LIMIT 1];
        system.runAs(u) {
            SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c,SandDV1__Active__c,SandDV1__Visit_Plan_Visible__c  from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true and SandDV1__End_Date__c =: todayDate limit 1];
            SandDV1__Route__c routeInstance    = [Select Id from SandDV1__Route__c limit 1];
            SandDV1__Visit_Plan__c visitPlan = new SandDV1__visit_Plan__c(SandDV1__Period__c = periodInstance.Id, SandDV1__User__c = userInfo.getUserId(), SandDV1__Status__c = 'Planning');
            insert visitPlan;
            VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningViewController.retreiveAccounts(periodInstance.Id, Label.Select_Route, 'Cold', VisitPlan.Id);
            
            for(VisitPlanningModel.CustomerWrapper res : resWrap.customerValues) {
                for(VisitPlanningModel.VisitPlanWrapper visitWrap : res.visitPlanWrapperList) {
                    if(res.customerName == 'Test Visit Plan Account0') {
                        if(date.parse(visitWrap.dateValue) == Date.today()) {
                            visitWrap.IsVisitPlanned = true;
                        } else if(date.parse(visitWrap.dateValue) == Date.today().addDays(10)) {
                            visitWrap.IsVisitPlanned = true;
                        }
                    }
                }
            }
            system.assertEquals(periodInstance.SandDV1__Active__c, true);
            system.assertEquals(periodInstance.SandDV1__Visit_Plan_Visible__c , true);
            SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = true;
            
            SandDV1__Visit_Plan__c boolRes = VisitPlanningViewController.saveVisitPlan(resWrap, visitPlan.Id, 'Submitted', new list<String>());
            VisitPlanningViewController.retreiveVisitPlan(visitPlan.Id);
            VisitPlanningViewController.retreiveAccounts(periodInstance.Id, Label.Select_Route, 'Cold', visitPlan.Id);
        }
        //VisitPlanningController.retreivePeriods();
    }*/
    
    
    /*******************************************************************************************************
* @description: Testing the functionality if the Visit Plan is Saved to Draft(Planning).
*  
*/  
/*
    private static testMethod void saveVisitPlan_SubmittedWithoutManager() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User' limit 1]; 
        User u    = [sELECT Id,IsActive FROM User WHERE profileId =: p.Id And IsActive =true LIMIT 1];
        system.runAs(u) {
            
            SandDV1__Period__c period = VisitPlanTestData.testPeriod(true, Date.today(), Date.today().addDays(15));
            insert period;
            Date todayDate = Date.Today().addDays(8);
            SandDV1__Route__c routeInstance    = [Select Id from SandDV1__Route__c limit 1];
            SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c,SandDV1__Active__c,SandDV1__Visit_Plan_Visible__c  from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true and SandDV1__End_Date__c >: todayDate limit 1];
            SandDV1__Visit_Plan__c visitPlan = new SandDV1__visit_Plan__c(SandDV1__Period__c = periodInstance.Id, SandDV1__User__c = userInfo.getUserId());
            insert visitPlan;
            VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningViewController.retreiveAccounts(periodInstance.Id, Label.Select_Route, 'Cold', visitPlan.Id);
            system.debug(periodInstance+'periodInstanceperiodInstanceperiodInstanceperiodInstance');
            for(VisitPlanningModel.CustomerWrapper res : resWrap.customerValues) {
                for(VisitPlanningModel.VisitPlanWrapper visitWrap : res.visitPlanWrapperList) {
                    if(res.customerName == 'Test Visit Plan Account0') {
                        if(date.parse(visitWrap.dateValue) == Date.today()) {
                            visitWrap.IsVisitPlanned = true;
                        } else if(date.parse(visitWrap.dateValue) == Date.today().addDays(10)) {
                            visitWrap.IsVisitPlanned = true;
                        }
                    }
                }
            }
            system.assertEquals(periodInstance.SandDV1__Active__c, true);
            system.assertEquals(periodInstance.SandDV1__Visit_Plan_Visible__c , true);
            SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = true;
            try {
                SandDV1__Visit_Plan__c boolRes = VisitPlanningViewController.saveVisitPlan(resWrap, visitPlan.Id, 'Submitted', new list<String>());
            } catch(Exception e) {
                system.assertEquals(e.getMessage(), Label.Manager_Not_Defined);
            }
            VisitPlanningViewController.retreiveVisitPlan(visitPlan.Id);
            VisitPlanningViewController.retreiveAccounts(periodInstance.Id, routeInstance.Id, 'Cold', visitPlan.Id);
        }
    }*/
    
    
     /*******************************************************************************************************
* @description: Testing the functionality if the Visit Plan is Saved to Draft(Planning).
*  
*/  
    private static testMethod void saveVisitPlan_ApprovedVisitPlan() {
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator' limit 1]; 
        User u1    = [sELECT Id,IsActive FROM User WHERE profileId =: p.Id And IsActive =true And ManagerId = null LIMIT 1];
        
        User u = new User(Alias = 'standt', Email='standarduser@VisitPlan.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_IN', ProfileId = p.Id, ManagerId = u1.Id,
            TimeZoneSidKey='Asia/Kolkata', UserName='standarduser_1@VisitPlan.com');
        
        system.runAs(u) {
            Date todayDate = Date.Today().addDays(8);
            SandDV1__Route__c routeInstance    = [Select Id from SandDV1__Route__c limit 1];
            SandDV1__Period__c periodInstance   = [Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c,SandDV1__Active__c,SandDV1__Visit_Plan_Visible__c  from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true and SandDV1__End_Date__c =: todayDate limit 1];
            SandDV1__Visit_Plan__c visitPlan = new SandDV1__visit_Plan__c(SandDV1__Period__c = periodInstance.Id, SandDV1__User__c = userInfo.getUserId());
            insert visitPlan;
            VisitPlanningModel.VisitPlanResponseWrapper resWrap = VisitPlanningViewController.retreiveAccounts(periodInstance.Id, Label.Select_Route, 'Cold', visitPlan.Id);
            system.debug(periodInstance+'periodInstanceperiodInstanceperiodInstanceperiodInstance');
            for(VisitPlanningModel.CustomerWrapper res : resWrap.customerValues) {
                for(VisitPlanningModel.VisitPlanWrapper visitWrap : res.visitPlanWrapperList) {
                    if(res.customerName == 'Test Visit Plan Account0') {
                        if(date.parse(visitWrap.dateValue) == Date.today()) {
                            visitWrap.IsVisitPlanned = true;
                        } else if(date.parse(visitWrap.dateValue) == Date.today().addDays(10)) {
                            visitWrap.IsVisitPlanned = true;
                        }
                    }
                }
            }
            system.assertEquals(periodInstance.SandDV1__Active__c, true);
            system.assertEquals(periodInstance.SandDV1__Visit_Plan_Visible__c , true);
            SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = true;
           
                SandDV1__Visit_Plan__c boolRes = VisitPlanningViewController.saveVisitPlan(resWrap, visitPlan.Id, 'Submitted', new list<String>());
            Test.startTest();
            VisitPlanningModel.ApprovalStatus appStatus = new VisitPlanningModel.ApprovalStatus();
            appStatus.status='Approve';
            VisitPlanningViewController.visitPlanApprovalRejection(appStatus, boolRes.Id);
            Test.stopTest();
            /*VisitPlanningViewController.retreiveVisitPlan(visitPlan.Id);
            VisitPlanningViewController.retreiveAccounts(periodInstance.Id, routeInstance.Id, 'Cold', visitPlan.Id);*/
        }
    }
}