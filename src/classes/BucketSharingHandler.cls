/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs).
* @date 8/17/2015.
* @description This class is work as a handler for SystemObjectSharing Trigger.
*/

public with sharing class BucketSharingHandler {
    
    /*******************************************************************************************************
    * @description :- On before insert of system object bucket from parent id field fetch parent id object and 
                      put id value in respective object lookup.
    * @param :- List<SandDV1__System_Object_Bucket__c>.
    * @return :- List<SandDV1__System_Object_Bucket__c>.
    */ 
    public static List<SandDV1__System_Object_Bucket__c> getParentRecord(List<SandDV1__System_Object_Bucket__c> newObjects) {
        Map<String, Schema.SObjectType> tokens = Schema.getGlobalDescribe();
        for(SandDV1__System_Object_Bucket__c objbucket : newObjects) {
            String shareobjectType ;
            Id parentObjectId = objbucket.SandDV1__Parent_Id__c;
            shareobjectType = String.valueof(parentObjectId.getSobjectType());
                if(shareobjectType.contains('__c')) {
                    objbucket.put(shareobjectType,objbucket.SandDV1__Parent_Id__c);
                }
                else {
                    shareobjectType = shareobjectType +'__c';
                    objbucket.put(shareobjectType,objbucket.SandDV1__Parent_Id__c);               
                }
        }
        return newObjects;
    }

    
    /*******************************************************************************************************
    * @description :- On insert of system object bucket create sharing records. and on update of bucket
                    accesslevel create sharing rules.After creating sharing rules update object bucket
                    with share record id.
                    IsObjectBucket = false means on update of bucket(changing accesslevel) we are inserting
                    shareing records. if IsObjectBucket = true means on insert of System object bucket 
                    we are inserting records.
    * @param :-List<SandDV1__System_Object_Bucket__c>,Boolean.
    * @return :- void . 
    */ 
    public static void onInsert(List<SandDV1__System_Object_Bucket__c> newObjects,Boolean IsObjectBucket) {
        try {
            if(IsObjectBucket == true) {
                newObjects = getParentRecord(newObjects);
            }
            Map<Id,SandDV1__Bucket__c> bucketMap = fetchBucketRecords(newObjects);
            if(!bucketMap.isempty()) {
                RecordSharing.assignSharing(newObjects,bucketMap,IsObjectBucket);
            }
        }
        catch(Exception e) {
            for (SandDV1__System_Object_Bucket__c newobject : newObjects) {
                newobject.addError(+System.Label.TriggerError +':-' +e);
            }
        }
    }

    /*******************************************************************************************************
    * @description :- Method to fetch bucket records and create a map of bucket id and records.
    * @param :- List<SandDV1__System_Object_Bucket__c>.
    * @return :- Map<Id,SandDV1__Bucket__c>. 
    */
    public static Map<Id,SandDV1__Bucket__c> fetchBucketRecords(List<SandDV1__System_Object_Bucket__c> newObjects) {
        Map<Id,SandDV1__bucket__c> bucketMap = new Map<Id,SandDV1__bucket__c>();
        Set<Id> bucketIdSet = new Set<Id>();

        for(SandDV1__System_Object_Bucket__c obj :newObjects) {
            bucketIdSet.add(obj.SandDV1__Bucket__c);
        }    

        for(SandDV1__Bucket__c b : BucketService.fetchBucketRecord(bucketIdSet)) {
            bucketMap.put(b.Id,b);
        }
        return bucketMap;
    }

    /*******************************************************************************************************
    * @description :- method on before update of Account Bucket. check if it is deactivated. if yes then delete sharing records.
    * @param :-List<SandDV1__System_Object_Bucket__c>,Map<Id,SandDV1__System_Object_Bucket__c>.
    * @return :- void . 
    */ 
    public static void onUpdate(List<SandDV1__System_Object_Bucket__c> newObjects,Map<Id,SandDV1__System_Object_Bucket__c> objectOldMap) {
        List<SandDV1__System_Object_Bucket__c> objectBucketList = new List<SandDV1__System_Object_Bucket__c>();
        try {
            for(SandDV1__System_Object_Bucket__c objbuck : newObjects) {
                if(objbuck.SandDV1__Deactivated_On__c !=null && objectOldMap.get(objbuck.Id).SandDV1__Deactivated_On__c == null) {
                    objectBucketList.add(objbuck);
                }
            }
            
            if(!objectBucketList.isEmpty()) {
                RecordSharing.deleteshare(objectBucketList);
            }
        }
        catch(Exception e) {
            for (SandDV1__System_Object_Bucket__c newobject : newObjects) {
                newobject.addError(+System.Label.TriggerError +':-' +e);
            }
        }
    }
     
    /*******************************************************************************************************
    * @description:- method is used to check bucket hierarchy is locked or not.
        on insert or update of SandDV1__System_Object_Bucket__c check bucket at top level have system future call = true or false.
        If system future call is true then means bucket hierachy is locked.
    * @param <param name>:- List<SandDV1__System_Object_Bucket__c>.
    * @return <return type> :-List<SandDV1__System_Object_Bucket__c>. 
    */
    public static List<SandDV1__System_Object_Bucket__c> checkHierarchyBlock(List<SandDV1__System_Object_Bucket__c> objectBucketList) {
        Map<Id,Boolean> bucketRootMap = new Map<Id,Boolean>();
        List<SandDV1__System_Object_Bucket__c> newObjectBucketList = new List<SandDV1__System_Object_Bucket__c>();
        
        //bucketRootMap will have root bucket id and future status (checkbox).
        bucketRootMap = BucketService.fetchRootIdFutureStatus(objectBucketList);

        for(SandDV1__System_Object_Bucket__c obj :objectBucketList) {
            if(bucketRootMap.containskey(obj.SandDV1__Bucket__c) && bucketRootMap.get(obj.SandDV1__Bucket__c) == false) {
                newObjectBucketList.add(obj); 
            }
            else if(bucketRootMap.containskey(obj.SandDV1__Bucket__c) && bucketRootMap.get(obj.SandDV1__Bucket__c) == true) {
                obj.addError(+System.Label.Error_Message_For_Future_Call);
            }
        }
        return newObjectBucketList;
    }
    
}