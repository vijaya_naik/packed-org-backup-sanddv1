/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeApplicationController {
    
    /*******************************************************************************************************
    * @description :- Method to check applicable schemes according to scheme criteria and then put data in class.
    * @param :- String salesOrderId
    * @return :- List<SchemeDomain.SchemeBenefitsWrapperV2>
    */
    
    @RemoteAction
    public static List<SchemeDomain.SchemeBenefitsWrapperV2> getApplicableSchemeBenefits(String salesOrderId){
        SandDV1__Sales_Order__c salesOrder = new SandDV1__Sales_Order__c();
        salesOrder = SalesOrderService.getSalesOrder(salesOrderId);

        List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemList = new List<SandDV1__Sales_Order_Line_Item__c>();
        salesOrderLineItemList = SalesOrderLineItemService.getNonFOCSalesOrderLineItems(salesOrderId);

        Map<String, SandDV1__Sales_Order_Item_Benefit__c> salesOrdeItemBenefitsMap = new Map<String, SandDV1__Sales_Order_Item_Benefit__c>();
        salesOrdeItemBenefitsMap = SalesOrderItemBenefitService.getAppliedSalesOrderLineItemBenefitsMap(salesOrderLineItemList);

        List<SchemeDomain.SchemeBenefitsWrapperV2> applicableSchemeBenefits = new List<SchemeDomain.SchemeBenefitsWrapperV2>();
        for(SchemeDomain.SchemeBenefitsWrapperV1 schemeBenefitWrapperV1 :SchemeBenefitSelectionController.getApplicableSchemeBenefits(salesOrder, salesOrderLineItemList)){
            applicableSchemeBenefits.add(SchemeBenefitService.convertSchemeBenefitWrapperToV2(schemeBenefitWrapperV1));
        }

        for(String orderItemBenefitCombinationKey :salesOrdeItemBenefitsMap.keySet()){
            for(SchemeDomain.SchemeBenefitsWrapperV2 schemeBenefitsWrapper :applicableSchemeBenefits){
                for(SchemeDomain.BenefitWrapper benefitWrapper :schemeBenefitsWrapper.schemeBenefitsList){
                    if((String) schemeBenefitsWrapper.salesOrderLineItem.Id + (String) benefitWrapper.schemeBenefit.Id == orderItemBenefitCombinationKey){
                        benefitWrapper.isSelected = true;
                        benefitWrapper.isSelectedOld = true;
                    }
                }
            }
        }

        return applicableSchemeBenefits;
    }

    /*******************************************************************************************************
    * @description :- Method to apply selected schemes and create scheme benefit line item and sales order records depending 
                        upon scheme type(FOC,Amount,percentage).
    * @param :- List<SchemeDomain.SchemeBenefitsWrapperV2> schemeBenefitWrapperList
    * @return :- chemeDomain.ApplySchemeMessageInfo
    */
    @RemoteAction
    public static SchemeDomain.ApplySchemeMessageInfo applySchemeBenefits(List<SchemeDomain.SchemeBenefitsWrapperV2> schemeBenefitWrapperList){
        List<SandDV1__Sales_Order_Item_Benefit__c> salesOrderItemBenefitsToInsert = new List<SandDV1__Sales_Order_Item_Benefit__c>();
        Set<String> orderItemBenefitCombinationSet = new Set<String>();
        Boolean iserror = false;
        
        SchemeDomain.ApplySchemeMessageInfo msgInfo = new SchemeDomain.ApplySchemeMessageInfo();

        for(SchemeDomain.SchemeBenefitsWrapperV2 schemeBenefitsWrapper :schemeBenefitWrapperList){
            decimal calsalesprice = 0.0;
            decimal totaldoscountprice = 0.0;
            for(SchemeDomain.BenefitWrapper benefitWrapper :schemeBenefitsWrapper.schemeBenefitsList){

                if(benefitWrapper.isSelected != benefitWrapper.isSelectedOld){
                    if(benefitWrapper.isSelected){
                        //check discount should not greater then unit price.
                        if(benefitWrapper.schemeBenefit.SandDV1__Type__c == ConfigurationService.getSchemeConfiguration('SandDV1__Scheme_Type_Amount__c')) {
                            totaldoscountprice = totaldoscountprice + benefitWrapper.schemeBenefit.SandDV1__Value__c;
                        }
                        else if(benefitWrapper.schemeBenefit.SandDV1__Type__c == ConfigurationService.getSchemeConfiguration('SandDV1__Scheme_Type_Percentage__c')) {
                            totaldoscountprice  = totaldoscountprice  + ((benefitWrapper.schemeBenefit.SandDV1__Value__c*schemeBenefitsWrapper.salesOrderLineItem.SandDV1__Unit_Price__c)/100);
                        }
                      
                            SandDV1__Sales_Order_Item_Benefit__c salesOrderItemBenefit = new SandDV1__Sales_Order_Item_Benefit__c();
                            salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c = schemeBenefitsWrapper.salesOrderLineItem.Id;
                            salesOrderItemBenefit.SandDV1__Scheme_Benefit__c = benefitWrapper.schemeBenefit.Id;
                            salesOrderItemBenefit.SandDV1__SchemeId__c = benefitWrapper.schemeBenefit.SandDV1__Scheme_Benefit_Range__r.SandDV1__Scheme__c;
                            salesOrderItemBenefitsToInsert.add(salesOrderItemBenefit);
                    }
                    else{
                        orderItemBenefitCombinationSet.add((String) schemeBenefitsWrapper.salesOrderLineItem.Id + (String) benefitWrapper.schemeBenefit.Id);
                    }
                }
            }

            if(schemeBenefitsWrapper.salesOrderLineItem.SandDV1__Unit_Price__c != null) {
                calsalesprice = schemeBenefitsWrapper.salesOrderLineItem.SandDV1__Unit_Price__c - totaldoscountprice ;
            }
            if(calsalesprice < 0.0) {
                iserror = true;
                msgInfo.applySchemeReturnMessage = System.Label.SchemeDiscountMessage;
            }
        }
        
        Savepoint sp = Database.setSavepoint();
        try{
            if(iserror == false) {
                if(orderItemBenefitCombinationSet.size() > 0){
                    System.debug(orderItemBenefitCombinationSet);
                    //before delete
                    SecurityUtils.checkObjectIsDeletable(SandDV1__Sales_Order_Item_Benefit__c.SObjectType);
                    delete SalesOrderItemBenefitService.getSalesOrderItemBenefitsWithCombination(orderItemBenefitCombinationSet);
                }
                
                if(salesOrderItemBenefitsToInsert.size() > 0){
                    //before insert
                    List<Schema.SObjectField> salesOrderItembenefitFieldsList = FieldAccessibilityUtility.fetchSalesOrderItembenefitFields();
                    SecurityUtils.checkInsert(SandDV1__Sales_Order_Item_Benefit__c.SObjectType,salesOrderItembenefitFieldsList);
                    insert salesOrderItemBenefitsToInsert;
                }
            }
            msgInfo.applySchemeID = schemeBenefitWrapperList[0].salesOrder.Id;
           return msgInfo;
        }
        catch(exception e){
            Database.rollback(sp);
            msgInfo.applySchemeReturnMessage = e.getMessage() ;
            return msgInfo;
           //throw new CustomException(e.getMessage());

        }
    }
}