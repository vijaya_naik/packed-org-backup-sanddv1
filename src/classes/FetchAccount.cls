public with sharing class FetchAccount {
    @InvocableMethod(label='Fetch Account' description='Fetch an Account')
    public static void assignRecordsToBucket(List<AccountUpdateRequest> requests) {
        System.debug('##1Inside the Bucket Assignor Method');
        for(AccountUpdateRequest req : requests) {
            Account a = new Account(Id = req.recordId, Phone = req.rEmail);
            update a;
        }    
            
    }
    
    public class AccountUpdateRequest {
        @InvocableVariable(label='Record Id' description='Id of Account' required=true)
        public Id recordId;

        @InvocableVariable(label='Record Email' description='Email of the Account' required=true)
        public String rEmail;
    }    
}