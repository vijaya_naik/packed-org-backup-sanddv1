public with sharing class SalesOrderItemBenefitService {
    public static String FOC = 'FOC';
    public static String AMOUNT = 'Amount';
    public static String PERCENTAGE = 'Percentage';

    public static void applySchemeBenefits(List<SandDV1__Sales_Order_Item_Benefit__c> salesOrderItemBenefitsList){
        Set<String> salesOrderLineItemsIdSet = new Set<String>();
        Set<String> schemeBenefitsIdSet = new Set<String>();
        Set<Id> schemeIdSet = new Set<Id>();
        
        for(SandDV1__Sales_Order_Item_Benefit__c salesOrderLineItemBenefit :salesOrderItemBenefitsList){
            if(salesOrderLineItemBenefit.SandDV1__Sales_Order_Line_Item__c != null){
                salesOrderLineItemsIdSet.add(salesOrderLineItemBenefit.SandDV1__Sales_Order_Line_Item__c);
            }
            
            if(salesOrderLineItemBenefit.SandDV1__Scheme_Benefit__c != null){
                schemeBenefitsIdSet.add(salesOrderLineItemBenefit.SandDV1__Scheme_Benefit__c);
            }
            if(salesOrderLineItemBenefit.SandDV1__SchemeId__c != null) {
                schemeIdSet.add(salesOrderLineItemBenefit.SandDV1__SchemeId__c);
            }
        }

        Map<String, SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemsMap = new Map<String, SandDV1__Sales_Order_Line_Item__c>();
        salesOrderLineItemsMap = ServiceFacade.getSalesOrderLineItemsMap(salesOrderLineItemsIdSet);
        System.debug('salesOrderLineItemsMap.........'+salesOrderLineItemsMap);
        Map<String, SandDV1__Scheme_Benefit__c> schemeBenefitsMap = new Map<String, SandDV1__Scheme_Benefit__c>();
        schemeBenefitsMap = ServiceFacade.getSchemeBenefitsMap(schemeBenefitsIdSet);
        System.debug('schemeBenefitsMap.........'+schemeBenefitsMap);
        List<SandDV1__Sales_Order_Line_Item__c> focLineItemsToInsert = new List<SandDV1__Sales_Order_Line_Item__c>();
        List<SandDV1__Sales_Order_Line_Item__c> lineItemsToUpdate = new List<SandDV1__Sales_Order_Line_Item__c>();

        for(SandDV1__Sales_Order_Item_Benefit__c salesOrderItemBenefit :salesOrderItemBenefitsList){
            if(schemeBenefitsMap.containsKey(salesOrderItemBenefit.SandDV1__Scheme_Benefit__c)){
                SandDV1__Scheme_Benefit__c schemeBenefit = schemeBenefitsMap.get(salesOrderItemBenefit.SandDV1__Scheme_Benefit__c);
                if(schemeBenefit.SandDV1__Type__c == AMOUNT || schemeBenefit.SandDV1__Type__c == PERCENTAGE || schemeBenefit.SandDV1__Type__c == FOC){
                    if(salesOrderLineItemsMap.containsKey(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c)){
                        if(schemeBenefit.SandDV1__Type__c == AMOUNT){
                            salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount__c = salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount__c == null ? 
                                schemeBenefit.SandDV1__Value__c : salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount__c + schemeBenefit.SandDV1__Value__c;
                        }
                        else
                        if(schemeBenefit.SandDV1__Type__c == PERCENTAGE){
                            salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount_Percentage__c = salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount_Percentage__c == null ? 
                                schemeBenefit.SandDV1__Value__c : salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount_Percentage__c + schemeBenefit.SandDV1__Value__c;
                        }
                        else
                        if(schemeBenefit.SandDV1__Type__c == FOC){
                            focLineItemsToInsert.add(ServiceFacade.getSalesOrderLineItem(schemeBenefit, salesOrderItemBenefit.Id, salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Sales_Order__c));
                        }
                      
                    }
                }
            }
        }
        if(!schemeIdSet.isempty()) {
            updateSchemeRecord(schemeIdSet,true);
        }
        insert focLineItemsToInsert;
        update salesOrderLineItemsMap.values();
    }
    
    /*******************************************************************************************************
    * @description:- Method to update Scheme Record with Scheme Applied Checkbox When 
                        Scheme is applied to order.
    * @param :- Set<Id> scheme Id Set.
    * @return :- void
    */
    public static void updateSchemeRecord(Set<Id> schemeIdSet,Boolean schemeApplied) {
        
        List<SandDV1__Scheme__c> UpdateSchemeList = new List<SandDV1__Scheme__c>();
        
        Map<Id,Integer> countMap = new Map<Id,Integer>();
        //Set<Id> newSchemeIdSet = new Set<Id>();
        System.debug('schemeApplied ...........'+schemeApplied );
        if(schemeApplied == false) {
            List<SandDV1__Sales_Order_Item_Benefit__c> salesOrderBenefitList = fetchSalesOrderItemBenefitRecords(schemeIdSet);
            System.debug('salesOrderBenefitList .......'+salesOrderBenefitList);
            
            for(SandDV1__Sales_Order_Item_Benefit__c salesBenefit: salesOrderBenefitList) {
                if(countMap.containskey(salesBenefit.SandDV1__SchemeId__c)) {
                    countMap.put(salesBenefit.SandDV1__SchemeId__c,countMap.get(salesBenefit.SandDV1__SchemeId__c) + 1);
                }
                else {
                    countMap.put(salesBenefit.SandDV1__SchemeId__c,1);
                }     
            }
        }
        System.debug('schemeIdSet...........'+schemeIdSet);
        System.debug('countMap...........'+countMap);
        for(Id schem :countMap.keyset()) {
            if(schemeIdSet.contains(schem)) {
                schemeIdSet.remove(schem);
            }
        }
        System.debug('schemeIdSet...........'+schemeIdSet);
        List<SandDV1__Scheme__c> SchemeList = ServiceFacade.fetchAllSchemeRecords(schemeIdSet);
        for(SandDV1__Scheme__c scheme: SchemeList) {
            if(scheme.SandDV1__Scheme_Applied__c != schemeApplied) {
                scheme.SandDV1__Scheme_Applied__c = schemeApplied;
                UpdateSchemeList.add(scheme); 
            }
        }
        update UpdateSchemeList;
    }
    
    public static void afterDeleteSalesOrderBenefit(List<SandDV1__Sales_Order_Item_Benefit__c> salesOrderItemBenefitsList){
        Set<Id> schemeIdSet = new Set<Id>();
        for(SandDV1__Sales_Order_Item_Benefit__c salesOrderLineItemBenefit :salesOrderItemBenefitsList){
            if(salesOrderLineItemBenefit.SandDV1__SchemeId__c != null) {
                schemeIdSet.add(salesOrderLineItemBenefit.SandDV1__SchemeId__c);
            }
        }
        if(!schemeIdSet.isempty()) {
            updateSchemeRecord(schemeIdSet,false);
        }
    }

    public static void revokeSchemeBenefits(List<SandDV1__Sales_Order_Item_Benefit__c> salesOrderItemBenefitsList){
        Set<String> salesOrderLineItemsIdSet = new Set<String>();
        Set<String> schemeBenefitsIdSet = new Set<String>();

        for(SandDV1__Sales_Order_Item_Benefit__c salesOrderLineItemBenefit :salesOrderItemBenefitsList){
            if(salesOrderLineItemBenefit.SandDV1__Sales_Order_Line_Item__c != null){
                salesOrderLineItemsIdSet.add(salesOrderLineItemBenefit.SandDV1__Sales_Order_Line_Item__c);
            }
            
            if(salesOrderLineItemBenefit.SandDV1__Scheme_Benefit__c != null){
                schemeBenefitsIdSet.add(salesOrderLineItemBenefit.SandDV1__Scheme_Benefit__c);
            }
        }
        

        Map<String, SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemsMap = new Map<String, SandDV1__Sales_Order_Line_Item__c>();
        salesOrderLineItemsMap = ServiceFacade.getSalesOrderLineItemsMap(salesOrderLineItemsIdSet);

        Map<String, SandDV1__Scheme_Benefit__c> schemeBenefitsMap = new Map<String, SandDV1__Scheme_Benefit__c>();
        schemeBenefitsMap = ServiceFacade.getSchemeBenefitsMap(schemeBenefitsIdSet);

        Set<String> salesOrderItemBenefitIdSet = new Set<String>();
        List<SandDV1__Sales_Order_Line_Item__c> lineItemsToUpdate = new List<SandDV1__Sales_Order_Line_Item__c>();

        for(SandDV1__Sales_Order_Item_Benefit__c salesOrderItemBenefit :salesOrderItemBenefitsList){
            if(schemeBenefitsMap.containsKey(salesOrderItemBenefit.SandDV1__Scheme_Benefit__c)){
                SandDV1__Scheme_Benefit__c schemeBenefit = schemeBenefitsMap.get(salesOrderItemBenefit.SandDV1__Scheme_Benefit__c);
                if(schemeBenefit.SandDV1__Type__c == AMOUNT || schemeBenefit.SandDV1__Type__c == PERCENTAGE || schemeBenefit.SandDV1__Type__c == FOC){
                    if(salesOrderLineItemsMap.containsKey(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c)){
                         if(schemeBenefit.SandDV1__Type__c == AMOUNT){
                            salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount__c = salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount__c == null ? 
                                0 - schemeBenefit.SandDV1__Value__c : salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount__c - schemeBenefit.SandDV1__Value__c;
                        }
                        else
                        if(schemeBenefit.SandDV1__Type__c == PERCENTAGE){
                            salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount_Percentage__c = salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount_Percentage__c == null ? 
                                0 - schemeBenefit.SandDV1__Value__c : salesOrderLineItemsMap.get(salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c).SandDV1__Discount_Percentage__c - schemeBenefit.SandDV1__Value__c;
                        }
                        else
                        if(schemeBenefit.SandDV1__Type__c == FOC){
                            salesOrderItemBenefitIdSet.add(salesOrderItemBenefit.Id);
                        }
                    }
                }
            }
        }
        
        delete ServiceFacade.getSalesOrderLineItemForBenefits(salesOrderItemBenefitIdSet);
        update salesOrderLineItemsMap.values();
        //update lineItemsToUpdate;
    }


    public static List<SandDV1__Sales_Order_Item_Benefit__c> getAppliedSalesOrderLineItemBenefitsList(List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemList){
        return new List<SandDV1__Sales_Order_Item_Benefit__c>(
            [SELECT Id, SandDV1__Sales_Order_Line_Item__c, SandDV1__Scheme_Benefit__c, SandDV1__Order_Item_Benefit_Combination__c
                FROM SandDV1__Sales_Order_Item_Benefit__c
                WHERE SandDV1__Sales_Order_Line_Item__c IN :salesOrderLineItemList
            ]
        );
    }


    public static Map<String, SandDV1__Sales_Order_Item_Benefit__c> getAppliedSalesOrderLineItemBenefitsMap(List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemList){
        Map<String, SandDV1__Sales_Order_Item_Benefit__c> salesOrderItemBenefitsMap = new Map<String, SandDV1__Sales_Order_Item_Benefit__c>();

        for(SandDV1__Sales_Order_Item_Benefit__c salesOrdeItemBenefit :getAppliedSalesOrderLineItemBenefitsList(salesOrderLineItemList)){
            salesOrderItemBenefitsMap.put(salesOrdeItemBenefit.SandDV1__Order_Item_Benefit_Combination__c, salesOrdeItemBenefit);
        }

        return salesOrderItemBenefitsMap;
    }


    public static List<SandDV1__Sales_Order_Item_Benefit__c> getSalesOrderItemBenefitsWithCombination(Set<String> orderItemBenefitCombinationSet){
        return new List<SandDV1__Sales_Order_Item_Benefit__c>(
            [SELECT Id, SandDV1__Sales_Order_Line_Item__c, SandDV1__Scheme_Benefit__c, SandDV1__Order_Item_Benefit_Combination__c 
                FROM SandDV1__Sales_Order_Item_Benefit__c
                WHERE SandDV1__Order_Item_Benefit_Combination__c IN :orderItemBenefitCombinationSet
            ]
        );
    }
    
    public static List<SandDV1__Sales_Order_Item_Benefit__c> fetchSalesOrderItemBenefitRecords(Set<Id> schemeIdSet) {
        return [SELECT Id, SandDV1__Sales_Order_Line_Item__c, SandDV1__Scheme_Benefit__c,SandDV1__SchemeId__c,SandDV1__Order_Item_Benefit_Combination__c 
                FROM SandDV1__Sales_Order_Item_Benefit__c
                WHERE SandDV1__SchemeId__c IN :schemeIdSet
            ];
    }
    
    public static List<SandDV1__Sales_Order_Item_Benefit__c> fetchSalesOrderItemBenefits(Set<Id> salesOrderLIID) {
        return [Select Id,Name,SandDV1__Order_Item_Benefit_Combination__c,SandDV1__Sales_Order_Line_Item__c,SandDV1__Scheme_Benefit__c from
            SandDV1__Sales_Order_Item_Benefit__c where SandDV1__Sales_Order_Line_Item__c In :salesOrderLIID];
    }
    
    /*******************************************************************************************************
    * @description :- delete Sales Order Item Benefit related with Sales Order Line Item .
    * @param <param name> .
    * @return <return type> . 
    */
    public static void deleteSalesBenefit(Set<Id> salesOrderLIIdSet) {
        List<SandDV1__Sales_Order_Item_Benefit__c> salesbenefitList = ServiceFacade.fetchSalesOrderItemBenefits(salesOrderLIIdSet);
        if(!salesbenefitList.isEmpty()) {
            delete salesbenefitList;
        }
    }
}