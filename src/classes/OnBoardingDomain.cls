/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class OnBoardingDomain {
    public OnBoardingDomain() {
        
    }
    
    /*public class onBoardingDocWrapper {
        public SandDV1__OnBoarding_Document__c boardingDoc = new SandDV1__OnBoarding_Document__c();
        public integer attachemntSize = 0;
        public List<Attachment> documentList = new List<Attachment>();
    }*/
    
    
    public class OnBoardingMessageInfo {         
         public String   boardingId{get;set;}
         public String   messagetype{get;set;}
         public String   boardingMessage{get;set;}
    }
    
    public class AttachmentWrapper {
        public Attachment attachObj;
        public String attachUrl;
        public Boolean IsLocked;
    }
    
    public class DocumentWrapper {
        public SandDV1__OnBoarding_Document__c boardingDocObj;
        public String statusColor;
        public Boolean IsSetting;
        public Boolean IsSelected;
        public Boolean IsAccountActive;
    }
}