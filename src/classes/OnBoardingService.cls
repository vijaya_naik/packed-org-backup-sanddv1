/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class OnBoardingService {
    public OnBoardingService () {
        
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__OnBoarding_Document__c Records
    * @param :- Id.
    * @return :- list<SandDV1__OnBoarding_Document__c>.
    */
    @RemoteAction
    public static list<SandDV1__OnBoarding_Document__c> fetchOnBoardingDocuments(Set<Id> docIdSet) {
        return [Select Id,Name,SandDV1__Approval_Process_Needed__c,SandDV1__Comments__c,SandDV1__Is_Mandatory__c ,SandDV1__System_Custom_Setting_ID__c,SandDV1__Description__c,SandDV1__Status__c,SandDV1__Type__c 
                from SandDV1__OnBoarding_Document__c where Id In:docIdSet];
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__OnBoarding_Document__c Records
    * @param :- Id.
    * @return :- list<SandDV1__OnBoarding_Document__c>.
    */
    @RemoteAction
    public static list<SandDV1__OnBoarding_Document__c> fetchAccountDocs(Set<Id> accountIdSet) {
        return [Select Id,SandDV1__AccountId__c,SandDV1__Approval_Process_Needed__c,Name,SandDV1__Comments__c,SandDV1__Is_Mandatory__c,SandDV1__System_Custom_Setting_ID__c,SandDV1__Description__c,SandDV1__Status__c,
            SandDV1__Type__c from SandDV1__OnBoarding_Document__c where SandDV1__AccountId__c In:accountIdSet];
    }

    /*******************************************************************************************************
    * @description :- Method to save attachment and document with account id
    * @param :- SandDV1__OnBoarding_Document__c docObj,String postContent,String fName.
    * @return :- String .
    */
    public static String doUploadAttachment(SandDV1__OnBoarding_Document__c docObj,String attachmentBody, String attachmentName) {
        String docId = null;
        Savepoint sp = Database.setSavepoint();
        try { 
            if(docObj != null) {
                //Before Upsert
                List<Schema.SObjectField> onBoardingFieldsList = FieldAccessibilityUtility.fetchOnBoardingFields();
                SecurityUtils.checkInsert(SandDV1__OnBoarding_Document__c.SObjectType,onBoardingFieldsList);
                SecurityUtils.checkUpdate(SandDV1__OnBoarding_Document__c.SObjectType,onBoardingFieldsList);
                upsert docObj;
                docId = docObj.Id;
            }
           if(docObj.Id != null) {
            Attachment a = new Attachment (ParentId = docObj.Id,Name = attachmentName);
                if(attachmentBody != null) {
                    String newBody = '';
                    newBody = attachmentBody;
                    a.Body = EncodingUtil.base64Decode(newBody);
                    //Before Upsert
                    List<Schema.SObjectField> attachmentFieldsList = FieldAccessibilityUtility.fetchAttachmentFields();
                    SecurityUtils.checkInsert(Attachment.SObjectType,attachmentFieldsList);
                    insert a;
                    
                }
                
            }
            return docId ;
        }
        catch(Exception e) {
            Database.rollback(sp);
            System.debug('e.......'+e);
            throw new CustomException(e.getMessage());
           // return null;
        }
    }

    /*******************************************************************************************************
    * @description :- Method to delete attachment.
    * @param :- Attachment .
    * @return :- void .
    */
    @RemoteAction
    public static void deleteAttachment(Attachment attachObj) {
        if(attachObj!=null) {
                //Before Delete
            SecurityUtils.checkObjectIsDeletable(Attachment.SObjectType);
            delete attachObj;
        }
    }
}