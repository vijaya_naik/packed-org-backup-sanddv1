/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author Extentor
* @date Sep 2015
* @description This class if for providing testdata to VisitPlanningController test class(VisitPlanTest).
*/

@isTest
public class VisitPlanTestData {

/*******************************************************************************************************
* @description Populates the User Data of System Admin Profile.
* @return User - returns the user record. 
*/
    public static User testAdminUser() {

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'standt', Email='standarduser@VisitPlan.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_IN', ProfileId = p.Id, 
            TimeZoneSidKey='Asia/Kolkata', UserName='standarduser@VisitPlan.com');
        return u;
    }

/*******************************************************************************************************
* @description Populates the User Data of Standard User Profile.
* @return User - returns the user record. 
*/

    public static User testStandardUser(id adminId) {

        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'standt', Email='standarduser@VisitPlan2.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_IN', ProfileId = p.Id, ManagerID = adminId,
            TimeZoneSidKey='Asia/Kolkata', UserName='standarduser@VisitPlan2.com');
        return u;
    }

/*******************************************************************************************************
* @description Populates the Account object data.
* @return User - returns the Account record. 
*/
    public static Account testAccount(integer i) {

        Account acc = new Account();
        acc.put('Name','Test Visit Plan Account'+i);
        return acc;
    }


/*******************************************************************************************************
* @description Populates the SandDV1__Period__c object data.
* @return User - returns the SandDV1__Period__c record. 
*/
    public static SandDV1__Period__c testPeriod(Boolean isActive, Date startDate, Date endDate) {

        SandDV1__Period__c period = new SandDV1__Period__c();
        period.put('Name', 'Test Period');
        period.put('SandDV1__Active__c',isActive);
        period.put('SandDV1__Start_Date__c',startDate);
        period.put('SandDV1__End_Date__c',endDate);
        period.put('SandDV1__Visit_Plan_Visible__c',true);
        return Period;
    }

/*******************************************************************************************************
* @description Populates the SandDV1__Route__c object data.
* @return User - returns the SandDV1__Route__c record. 
*/
    public static SandDV1__Route__c testRoute(boolean b) {

        SandDV1__Route__c route = new SandDV1__Route__c();
        route.put('Name', 'Test Route');
        route.put('SandDV1__Active__c', b);
        return route;
    }

/*******************************************************************************************************
* @description Populates the SandDV1__Route_Account__c object data.
* @return User - returns the SandDV1__Route_Account__c record. 
*/
    public static SandDV1__Route_Account__c testRouteAccount(Id routeId, Id accountId, boolean b) {

        SandDV1__Route_Account__c routeAcc = new SandDV1__Route_Account__c();
        routeAcc.put('SandDV1__Route__c', routeId);
        routeAcc.put('SandDV1__Account__c', accountId);
        routeAcc.put('SandDV1__Active__c', b);
        return routeAcc;
    }

/*******************************************************************************************************
* @description Populates the Holiday object data.
* @return User - returns the Holiday record. 
*/  
    public static Holiday testHoliday(Date d) {

        Holiday holidayInstance       = new Holiday();
        holidayInstance.put('Name', 'Test Holiday');
        holidayInstance.put('ActivityDate', d);
        return holidayInstance;
    }
  
/*******************************************************************************************************
* @description Populates the Visit Plan object data.
* @return User - returns the SandDV1__Visit_Plan__c record. 
*/  
    public static SandDV1__Visit_Plan__c testVisitPlan(Id periodId, Id userId) {
        
        SandDV1__Visit_Plan__c visitPlan = new SandDV1__Visit_Plan__c();
        visitPlan.put('SandDV1__Period__c', periodId);
        visitPlan.put('SandDV1__User__c', userId);
        return visitPlan;
    }
    
/*******************************************************************************************************
* @description Populates the Visit object data.
* @return User - returns the SandDV1__Visit__c record. 
*/  
    public static SandDV1__Visit__c testVisit(Id accountId, Id visitPlanId) {
        
        SandDV1__Visit__c visit = new SandDV1__Visit__c();
        visit.put('SandDV1__Account__c', accountId);
        visit.put('SandDV1__Visit_Plan__c', visitPlanId);
        return visit;
    }
    
}