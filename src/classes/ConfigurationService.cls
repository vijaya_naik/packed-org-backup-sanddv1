/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/


/**
* @author ET Marlabs.
* @date 18 Aug 2015
* @description This is the service class responsible for all the configuration related stuff
*/

global with sharing class ConfigurationService {
    
    
    public static final String OnBoardingAccountActiveStatus = 'Active';
    public static final String OnBoardingDraftStatus = 'Draft';
    public static final String OnBoardingPendingApprovalStatus = 'Pending Approval';
    public static final String OnBoardingNeedApprovalStatus = 'Need Approval';
    public static final String OnBoardingApprovedStatus = 'Approved';
    public static final String OnBoardingDraftStatusColor = fetchOnBoardingStatusColor('Draft');
    public static final String OnBoardingPendingApprovalStatusColor = fetchOnBoardingStatusColor('Need Approval');
    public static final String OnBoardingNeedApprovalStatusColor = fetchOnBoardingStatusColor('Pending Approval');
    public static final String OnBoardingApprovedStatusColor = fetchOnBoardingStatusColor('Approved');
    
    //for scheme service.
    //public static final String schemeTypePercentage = 'Percentage';
    //public static final String schemeTypeAmount = 'Amount';
    //public static final String schemeTypeFOC = 'FOC';
    //public static final String Scheme_Criteria_Type = 'Criteria';
    //public static final String Scheme_Assignment_Type = 'Assignment';
    //public static final String scheme_Active_Status = 'Active';
    //public static final String scheme_InActive_Status = 'In-Active';

    public static final Integer LimitRecords = 3;

    private static List<String> classNames = new List<String>();
    private static List<Filterable> filterInstances = new List<Filterable>();

    /*public ConfigurationService() {
            
    }*/
    
    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static Set<String> getStatusSet(String objType,String devName){
        Set<String> statusSet = new Set<String>();
        List<Order_Status_for_Access_Control__mdt> orderStatusList = new List<Order_Status_for_Access_Control__mdt>();
        orderStatusList = [Select Id,SandDV1__Object_Type__c,SandDV1__Status__c,DeveloperName,MasterLabel from Order_Status_for_Access_Control__mdt 
                            where SandDV1__Object_Type__c =: objType And DeveloperName =: devName];

        for(Order_Status_for_Access_Control__mdt order :orderStatusList){
            String[] statusArray = order.SandDV1__Status__c.split(',');
            for( Integer i = 0; i<statusArray.size(); i++)
            {
                statusSet.add(statusArray[i]);
            }
        }
        return statusSet;
    }
    
    /*******************************************************************************************************
    * @description This method will fetch scheme criteria values from custom meta data
    * @param .
    * @return List<Scheme_Criteria_Values__mdt>. 
    */
    public static List<Scheme_Criteria_Values__mdt> fetchSchemeCriteriaMetadate() {
        return [Select Id,SandDV1__FieldApi__c,SandDV1__IsPickList__c,SandDV1__ReturnType__c,SandDV1__ActiveAPI__c,SandDV1__ActiveApiValue__c,SandDV1__SchemeType__c,SandDV1__IsAssignment__c,SandDV1__CriteriaValue__c,SandDV1__ObjectApi__c,
                QualifiedApiName  from Scheme_Criteria_Values__mdt where SandDV1__ReturnType__c != ''];
    }
    
    /*******************************************************************************************************
    * @description This method will fetch the list of all the filter class intances related to a process
    *               based on the Process Class Name.
    * @param String.
    * @return List<IProcessInvocableFilter>. 
    */
    
    public static List<Filterable> fetchFilters(String processClassName) {
        //Query on the designated Custom Metadata Type to fetch the required filter classes 
        //based on the Process Interface name provided
        for(String className : classNames) {
            Type classType = Type.forName(className);
            filterInstances.add((Filterable)classType.newInstance());
        }

        return filterInstances;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch OnBoardingDocumentSetting (custom setting).
    * @param :- Id.
    * @return :- List<SandDV1__OnBoardingDocumentSetting__c>. 
    */
    public static List<SandDV1__OnBoardingDocumentSetting__c> fetchOnBoardingDocSetting() {

        return SandDV1__OnBoardingDocumentSetting__c.getall().values();
    }

    /*******************************************************************************************************
    * @description :- Method to get the default Visit Plan target for the organization.
    * @keyValue :- string value or NULL.
    * @return list<SandDV1__Visit_Plan_Default_Target__c> - returns list of SandDV1__Visit_Plan_Default_Target__c. 
    */  
    public static Map<String, SandDV1__Visit_Plan_Default_Target__c> fetchVisitPlanDefaultTarget(String keyValue) {

        Map<String, SandDV1__Visit_Plan_Default_Target__c> defaultTarget = new Map<String, SandDV1__Visit_Plan_Default_Target__c>();
        if(keyValue != null && keyValue != '') {
        defaultTarget.put(keyValue, SandDV1__Visit_Plan_Default_Target__c.getall().get(keyValue));
        } else {
           defaultTarget = SandDV1__Visit_Plan_Default_Target__c.getall();
        }
        return defaultTarget;
    }
    
     /*******************************************************************************************************
    * @description :- Method to fetch Mandatory OnBoardingDocumentSetting (custom setting).
    * @param :- Id.
    * @return :- List<SandDV1__OnBoardingDocumentSetting__c>. 
    */
    public static List<SandDV1__OnBoardingDocumentSetting__c> fetchMandatoryOnBoardingDocSetting() {

        return [Select SandDV1__Description__c,SandDV1__Approval_Process_Needed__c,SandDV1__Is_Mandatory__c,SandDV1__Type__c,Name from SandDV1__OnBoardingDocumentSetting__c where SandDV1__Is_Mandatory__c =:True];
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__OnBoardingStatusColor__c(custom setting).
    * @param :- Id.
    * @return :- List<SandDV1__OnBoardingStatusColor__c>. 
    */
    public static List<SandDV1__OnBoardingStatusColor__c> fetchOnBoardingStatusColor() {

        return SandDV1__OnBoardingStatusColor__c.getall().values();
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__OnBoardingStatusColor__c(custom setting).
    * @param :- Id.
    * @return :- List<SandDV1__OnBoardingStatusColor__c>. 
    */
    public static String fetchOnBoardingStatusColor(String name) {

        return SandDV1__OnBoardingStatusColor__c.getInstance(name) != null ? SandDV1__OnBoardingStatusColor__c.getInstance(name).SandDV1__Color_Code__c : '';
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Target view size
    * @param :- 
    * @return :- Integer 
    */
    public static Integer getTargetViewSize(){

        return ConstantsClass.TargetViewSize;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch aspect values from the custom settings.If not found then we get the 
                      default value from the constants class
    * @param :- 
    * @return :- Map<String,String> 
    */
    public static Map<String,String> getAspectValues(){

        Map<String,String> aspectMap = new Map<String,String>();
        for(SandDV1__Aspect_Values__c aspect:SandDV1__Aspect_Values__c.getall().values())
        aspectMap.put(aspect.Name,aspect.SandDV1__Value__c);
        return !aspectMap.isEmpty() ? aspectMap : ConstantsClass.getAspectValues();
    }

    /*******************************************************************************************************
    * @description :- Method to fetch TargetStatus values from the custom settings.If not found then we get the 
                      default value from the constants class
    * @param :- 
    * @return :- Map<String,String> 
    */
    public static Map<String,String> getTargetStatusValues(){

        Map<String,String> statusMap = new Map<String,String>();
        for(SandDV1__Target_Status_Values__c status:SandDV1__Target_Status_Values__c.getall().values())
        statusMap.put(status.Name,status.SandDV1__Value__c);
        return !statusMap.isEmpty() ? statusMap : ConstantsClass.getTargetStatusValues();
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Sales Order status values
    * @param :- 
    * @return :- Map<String,String> 
    */
    public static Map<String,String> getSalesOrderStatusValues(){

        Map<String,String> statusMap = new Map<String,String>();
        for(SandDV1__SalesOrder_Status_Values__c status:SandDV1__SalesOrder_Status_Values__c.getall().values())
        statusMap.put(status.Name,status.SandDV1__Value__c);
        return !statusMap.isEmpty() ? statusMap : ConstantsClass.getSalesOrderStatusValues();
    }

    /*******************************************************************************************************
    * @description :- Method to fetch allowTargetAcceptance value
    * @param :- 
    * @return :- Boolean 
    */
    public static Boolean getAllowTargetAcceptance(){

        SandDV1__Target_Configuration__c targetConfig = getTargetConfiguration();
        return targetConfig != null ? targetConfig.SandDV1__AllowTargetAcceptance__c : ConstantsClass.AllowTargetAcceptance;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch AllowAcceptedTargetsEdit value
    * @param :- 
    * @return :- Boolean 
    */
    public static Boolean getAllowAcceptedTargetsEdit(){

        SandDV1__Target_Configuration__c targetConfig = getTargetConfiguration();
        return targetConfig != null ? targetConfig.SandDV1__AllowAcceptedTargetsEdit__c : ConstantsClass.AllowAcceptedTargetsEdit;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch AllowTargetCreation value
    * @param :- 
    * @return :- Boolean 
    */
    public static Boolean getAllowTargetCreation(){

        SandDV1__Target_Configuration__c targetConfig = getTargetConfiguration();
        return targetConfig != null ? targetConfig.SandDV1__AllowTargetCreation__c : ConstantsClass.AllowTargetCreation;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch AllowSandBagging value
    * @param :- 
    * @return :- Boolean 
    */
    public static Boolean getAllowSandBagging(){

        SandDV1__Target_Configuration__c targetConfig = getTargetConfiguration();
        return targetConfig != null ? targetConfig.SandDV1__AllowSandBagging__c : ConstantsClass.AllowSandBagging;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch AllowLowerTargets value
    * @param :- 
    * @return :- Boolean 
    */
    public static Boolean getAllowLowerTargets(){

        SandDV1__Target_Configuration__c targetConfig = getTargetConfiguration();
        return targetConfig != null ? targetConfig.SandDV1__AllowLowerTargets__c : ConstantsClass.AllowLowerTargets;
    }


    /*******************************************************************************************************
    * @description :- Method to fetch DigitLimit value
    * @param :- 
    * @return :- Integer 
    */
    public static Integer getDigitLimit(){

        SandDV1__Target_Configuration__c targetConfig = getTargetConfiguration();
        Decimal digitlimit = targetConfig != null ? targetConfig.SandDV1__DigitLimit__c : ConstantsClass.DigitLimit;
        return digitlimit.intValue();
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Target Configuration from custom setting
    * @param :- 
    * @return :- SandDV1__Target_Configuration__c 
    */
    public static SandDV1__Target_Configuration__c getTargetConfiguration(){

        return SandDV1__Target_Configuration__c.getInstance('Target Configuration'); //Target Configuration is the record name in SandDV1__Target_Configuration__c custom setting
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme data from Scheme Configuration Meta data
    * @param :- Field type that should be returned
    * @return :- String 
    */
    public static String getSchemeConfiguration(String fieldType){

        Scheme_Configuration__mdt scheme_Config = [SELECT Id,SandDV1__Scheme_Active_Status__c,SandDV1__Scheme_Assignment_Type__c,SandDV1__Scheme_Criteria_Type__c,SandDV1__Scheme_InActive_Status__c,
                SandDV1__Scheme_Type_Amount__c,SandDV1__Scheme_Type_FOC__c,SandDV1__Scheme_Type_Percentage__c FROM SandDV1__Scheme_Configuration__mdt LIMIT 1];
        return  (string)scheme_Config.get(fieldType);
    }
}