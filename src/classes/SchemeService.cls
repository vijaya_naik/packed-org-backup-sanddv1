/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeService {

  public static final String Scheme_Criteria_Type = ConfigurationService.getSchemeConfiguration('SandDV1__Scheme_Criteria_Type__c');
  public static final String Scheme_Assignment_Type = ConfigurationService.getSchemeConfiguration('SandDV1__Scheme_Assignment_Type__c'); 
  public static final String scheme_Active_Status = ConfigurationService.getSchemeConfiguration('SandDV1__Scheme_Active_Status__c');
  public static final String scheme_InActive_Status = ConfigurationService.getSchemeConfiguration('SandDV1__Scheme_InActive_Status__c');

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme records using scheme Id.
    * @param :- String schemeId.
    * @return :- List<SandDV1__Scheme__c>.
    */
    public static List<SandDV1__Scheme__c> fetchSchemeRec(String schemeID){
        return [Select Id,Name,SandDV1__Scheme_Name__c,SandDV1__Description__c,SandDV1__Type__c,SandDV1__Valid_From__c,SandDV1__Exclusive_Scheme__c,SandDV1__Expires_On__c,SandDV1__Budget_Limit__c,SandDV1__Status__c 
                from SandDV1__Scheme__c where Id =: schemeID];
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Scheme records using scheme Id Set.
    * @param :- Set<Id> schemeIdSet.
    * @return :- List<SandDV1__Scheme__c>.
    */
    public static List<SandDV1__Scheme__c> fetchAllSchemeRecords(Set<Id> schemeIdSet){
        return [Select Id,Name,SandDV1__Scheme_Name__c,SandDV1__Scheme_Applied__c,SandDV1__Description__c,SandDV1__Type__c,SandDV1__Valid_From__c,SandDV1__Exclusive_Scheme__c,SandDV1__Expires_On__c,SandDV1__Budget_Limit__c,SandDV1__Status__c from SandDV1__Scheme__c where Id IN:schemeIdSet];
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch Scheme,Scheme Condition,Scheme Benefit,Scheme Benefit Range records when Scheme ID not null.
    * @param :- String schemeId.
    * @return :- SchemeDomain.schemeWrapperAll.
    */
    public static SchemeDomain.schemeWrapperAll fetchAllSchemeRecords(String schemeId) {
        Map<String,SchemeDomain.criteriaWrapper> criteriamap = new Map<String,SchemeDomain.criteriaWrapper>();
        Set<Id> benfitSet = new Set<Id>();
        SchemeDomain.schemeWrapperAll newSchemeWrap = new SchemeDomain.schemeWrapperAll();
            for(SchemeDomain.criteriaWrapper cw :SchemeController.getCriteria()) {
               criteriamap.put(cw.criteriavalue,cw);
            }

            for(SandDV1__Scheme__c scheme : SchemeService.fetchSchemeRec(schemeId)) {
                newSchemeWrap.schemeRecord.schemeObj = scheme;
                newSchemeWrap.schemeRecord.schemeType = scheme.SandDV1__Type__c;
                if(scheme.SandDV1__Status__c == SchemeService.scheme_Active_Status) {
                    newSchemeWrap.schemeRecord.IsActive = true;
                }
                else {
                    newSchemeWrap.schemeRecord.IsActive = false;
                }
                newSchemeWrap.schemeRecord.validFromDate = string.valueof(scheme.SandDV1__Valid_From__c);
                newSchemeWrap.schemeRecord.expireOnDate = string.valueof(scheme.SandDV1__Expires_On__c);
                
            }
            for(SandDV1__Scheme_Condition__c schemeCondition : ServiceFacade.fetchSchemeConditionRec(schemeId)) {
                if(schemeCondition.SandDV1__System_Scheme_Type__c == SchemeService.Scheme_Criteria_Type) {
                    newSchemeWrap.sCriteriaRecord = schemeCondition;
                }
                if(schemeCondition.SandDV1__System_Scheme_Type__c == SchemeService.Scheme_Assignment_Type) {
                    (newSchemeWrap.sAssignmentRecord).add(schemeCondition);
                }
            }
            for(SandDV1__Scheme_Benefit_Range__c benefitRange : ServiceFacade.fetchSchemeBenefitRange(schemeId)) {
                benfitSet.add(benefitRange.Id);
                SchemeDomain.benfitRangeWrapper brwrap = new SchemeDomain.benfitRangeWrapper();
                brwrap.min = String.valueof(benefitRange.SandDV1__Min__c);
                brwrap.max = String.valueof(benefitRange.SandDV1__Max__c);
                if(benefitRange.SandDV1__Max__c == null) {
                    brwrap.range = String.valueof(benefitRange.SandDV1__Min__c) +'-';
                }
                else {
                    brwrap.range = String.valueof(benefitRange.SandDV1__Min__c) +'-'+ String.valueof(benefitRange.SandDV1__Max__c);
                }
                
                brwrap.uniqueId = benefitRange.Id;
                brwrap.recordId = benefitRange.Id;
                (newSchemeWrap.sBenefitRangeRecord).add(brwrap); 
            }
            for(SandDV1__Scheme_Benefit__c benefit : ServiceFacade.fetchSchemeBenefit(benfitSet)) {
                SchemeDomain.benfitWrapper bwrap = new SchemeDomain.benfitWrapper();
                bwrap.productId = benefit.SandDV1__Product__c;
                bwrap.productName = benefit.SandDV1__Product__r.Name;
                if(benefit.SandDV1__Scheme_Benefit_Range__r.SandDV1__Max__c == null) {
                    bwrap.range = String.valueof(benefit.SandDV1__Scheme_Benefit_Range__r.SandDV1__Min__c) +'-';
                }
                else {
                    bwrap.range = String.valueof(benefit.SandDV1__Scheme_Benefit_Range__r.SandDV1__Min__c) +'-'+ String.valueof(benefit.SandDV1__Scheme_Benefit_Range__r.SandDV1__Max__c);
                }
                bwrap.uniqueId =benefit.SandDV1__Scheme_Benefit_Range__c;
                bwrap.value= benefit.SandDV1__Value__c;
                bwrap.selectedSCType = benefit.SandDV1__Type__c;
                bwrap.selectedLineItem = benefit.SandDV1__Level__c;
                bwrap.recordId = benefit.Id;
                (newSchemeWrap.sBenefitRecord).add(bwrap); 
            }
        
        return newSchemeWrap;
    }
    
    /*******************************************************************************************************
    * @description :- Method to Save Scheme,SchemeCondition,Scheme Benefit Range,Scheme Benefit records.
    * @param :- SchemeDomain.schemeWrapper schemeRecord,SandDV1__Scheme_Condition__c schemeConditionRecord,
                                List<SandDV1__Scheme_Condition__c> assignmentCriteriaList,List<SchemeDomain.benfitRangeWrapper> benfitRangeList,
                                List<SchemeDomain.benfitWrapper> benefitList,List<SchemeDomain.benfitRangeWrapper> deleteschemeBenefitList,List<SandDV1__Scheme_Condition__c> deletedAssignmentScheme,
                                SandDV1__Scheme_Condition__c deleteSchemeCriteria.
    * @return :- String .
    */
    public static String SaveScheme(SchemeDomain.schemeWrapper schemeRecord,SandDV1__Scheme_Condition__c schemeConditionRecord,
                                List<SandDV1__Scheme_Condition__c> assignmentCriteriaList,List<SchemeDomain.benfitRangeWrapper> benfitRangeList,
                                List<SchemeDomain.benfitWrapper> benefitList,List<SchemeDomain.benfitRangeWrapper> deleteschemeBenefitList,List<SandDV1__Scheme_Condition__c> deletedAssignmentScheme,
                                SandDV1__Scheme_Condition__c deleteSchemeCriteria) {
        Savepoint sp = Database.setSavepoint();
        try {
            String returnSchemeID = null;
            Set<Id> benefitSet = new Set<Id>();
            Set<Id> benefitRangeSet = new Set<Id>();
            Map<String,String> rangeMap = new Map<String,String>();
            Map<String,Id> benefitRangeMap = new Map<String,Id>();
            List<SandDV1__Scheme_Condition__c> schemeConditionList = new List<SandDV1__Scheme_Condition__c>();
            List<SandDV1__Scheme_Benefit_Range__c> schemeBenefitRangeList = new List<SandDV1__Scheme_Benefit_Range__c>();
            List<SandDV1__Scheme_Benefit__c> schemeBenefitList = new List<SandDV1__Scheme_Benefit__c>();

            List<SandDV1__Scheme_Benefit_Range__c> deleteRangeList = new List<SandDV1__Scheme_Benefit_Range__c>();
            List<SandDV1__Scheme_Benefit__c> deleteBenefitList = new List<SandDV1__Scheme_Benefit__c>();


            SandDV1__Scheme__c newscheme = new SandDV1__Scheme__c();
            if(schemeRecord != null) { 
                newscheme.SandDV1__Scheme_Name__c = schemeRecord.schemeObj.SandDV1__Scheme_Name__c;
                newscheme.SandDV1__Budget_Limit__c = schemeRecord.schemeObj.SandDV1__Budget_Limit__c;
                newscheme.SandDV1__Type__c = schemeRecord.schemeType;
                newscheme.SandDV1__Description__c = schemeRecord.schemeObj.SandDV1__Description__c;
                newscheme.SandDV1__Valid_From__c = date.valueof(schemeRecord.validFromDate);
                newscheme.SandDV1__Expires_On__c = date.valueof(schemeRecord.expireOnDate);
                newscheme.Id = schemeRecord.schemeObj.Id;
                
                if(schemeRecord.IsActive == true) {
                    newscheme.SandDV1__Status__c = Label.Scheme_Active;
                }
                else {
                    newscheme.SandDV1__Status__c = Label.Scheme_InActive;
                }
                //Before Upsert
                List<Schema.SObjectField> schemeFieldsList = FieldAccessibilityUtility.fetchSchemeFields();
                SecurityUtils.checkInsert(SandDV1__Scheme__c.SObjectType,schemeFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Scheme__c.SObjectType,schemeFieldsList);
                upsert newscheme;
                returnSchemeID = newscheme.Id;
            }
            
            if(deleteSchemeCriteria.Id != null) {
                //Before Delete
                SecurityUtils.checkObjectIsDeletable(SandDV1__Scheme_Condition__c.SObjectType);
                delete deleteSchemeCriteria;
            }
            if(schemeConditionRecord != null && newscheme.Id != null) {
                if(schemeConditionRecord.SandDV1__Parameter__c != null && schemeConditionRecord.SandDV1__Parameter__c != '') { 
                    SandDV1__Scheme_Condition__c schemecondition = new SandDV1__Scheme_Condition__c();
                        schemecondition.SandDV1__Parameter__c = schemeConditionRecord.SandDV1__Parameter__c;
                        schemecondition.SandDV1__Value__c = String.valueof(schemeConditionRecord.SandDV1__Value__c);
                        schemecondition.SandDV1__Scheme__c = newscheme.Id;
                        schemecondition.SandDV1__System_Value__c = String.valueof(schemeConditionRecord.SandDV1__System_Value__c);
                        schemecondition.SandDV1__System_Scheme_Type__c = SchemeService.Scheme_Criteria_Type;
                        schemecondition.Id = schemeConditionRecord.Id;
                        schemecondition.SandDV1__FieldApi__c = schemeConditionRecord.SandDV1__FieldApi__c;
                        schemecondition.SandDV1__ObjectApi__c = schemeConditionRecord.SandDV1__ObjectApi__c;
                        schemecondition.SandDV1__DataType__c = schemeConditionRecord.SandDV1__DataType__c;
                    schemeConditionList.add(schemecondition);
                } 
            } 

            if(assignmentCriteriaList.size()>0 && newscheme.Id != null) {
                for(SandDV1__Scheme_Condition__c sc :assignmentCriteriaList) {
                    if(sc.SandDV1__Parameter__c != null && sc.SandDV1__Parameter__c != '') { 
                        SandDV1__Scheme_Condition__c schemecondition = new SandDV1__Scheme_Condition__c();
                            schemecondition.SandDV1__Parameter__c = sc.SandDV1__Parameter__c;
                            schemecondition.SandDV1__Value__c = sc.SandDV1__Value__c;
                            schemecondition.SandDV1__System_Value__c = sc.SandDV1__System_Value__c;
                            schemecondition.SandDV1__Scheme__c = newscheme.Id;
                            schemecondition.SandDV1__System_Scheme_Type__c = SchemeService.Scheme_Assignment_Type;
                            schemecondition.Id = sc.Id;
                            schemecondition.SandDV1__FieldApi__c = sc.SandDV1__FieldApi__c;
                            schemecondition.SandDV1__ObjectApi__c = sc.SandDV1__ObjectApi__c;
                            schemecondition.SandDV1__DataType__c = sc.SandDV1__DataType__c;
                        schemeConditionList.add(schemecondition);
                    }
                }
            }

            if(!schemeConditionList.isEmpty()) {
                //Before Upsert  
                List<Schema.SObjectField> schemeConditionFieldsList = FieldAccessibilityUtility.fetchSchemeConditionFields();                
                SecurityUtils.checkInsert(SandDV1__Scheme_Condition__c.SObjectType,schemeConditionFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Scheme_Condition__c.SObjectType,schemeConditionFieldsList);
                upsert schemeConditionList;
            }

            if(benfitRangeList.size()>0 && newscheme.Id != null) {
                for(SchemeDomain.benfitRangeWrapper bRange : benfitRangeList) {
                    SandDV1__Scheme_Benefit_Range__c benefitRange = new SandDV1__Scheme_Benefit_Range__c();
                    if(bRange.max != null){
                        benefitRange.SandDV1__Max__c = decimal.valueof(bRange.max);
                    }
                    else{
                        benefitRange.SandDV1__Max__c = null;
                    }
                        benefitRange.SandDV1__Min__c = decimal.valueof(bRange.min);
                        benefitRange.SandDV1__Scheme__c = newscheme.Id;
                        benefitRange.Id = null;
                        benefitRangeSet.add(bRange.recordId);
                    schemeBenefitRangeList.add(benefitRange);
                    rangeMap.put(bRange.min+'-'+bRange.max,bRange.uniqueId);
                }
            }
            
            if(!deletedAssignmentScheme.isempty()) {
                //Before Delete
                SecurityUtils.checkObjectIsDeletable(SandDV1__Scheme_Condition__c.SObjectType);
                delete deletedAssignmentScheme;
            }
            for(SchemeDomain.benfitRangeWrapper bRange : deleteschemeBenefitList) {
                if(bRange.recordId != null && bRange.recordId != '') {
                    benefitRangeSet.add(bRange.recordId);
                }
            }
            deleteRangeList = [select Id from SandDV1__Scheme_Benefit_Range__c where Id In:benefitRangeSet];
            if(!deleteRangeList.isEmpty()){
                //Before Delete
                SecurityUtils.checkObjectIsDeletable(SandDV1__Scheme_Benefit_Range__c.SObjectType);
                delete deleteRangeList;
            }
            if(!schemeBenefitRangeList.isEmpty()){
                //Before Upsert
                List<Schema.SObjectField> schemeBenefitRangeFieldsList = FieldAccessibilityUtility.fetchSchemeBenefitRangeFields();
                SecurityUtils.checkInsert(SandDV1__Scheme_Benefit_Range__c.SObjectType,schemeBenefitRangeFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Scheme_Benefit_Range__c.SObjectType,schemeBenefitRangeFieldsList);
                upsert schemeBenefitRangeList;
            }
            for(SandDV1__Scheme_Benefit_Range__c sbr : schemeBenefitRangeList) {
                benefitRangeMap.put(rangeMap.get(String.valueof(sbr.SandDV1__Min__c)+'-'+String.valueof(sbr.SandDV1__Max__c)),sbr.Id);
            }
            if(benefitList.size()>0 && newscheme.Id != null) {
                for(SchemeDomain.benfitWrapper benefit : benefitList) {
                    SandDV1__Scheme_Benefit__c newbenefit = new SandDV1__Scheme_Benefit__c();
                        newbenefit.SandDV1__Level__c = benefit.selectedLineItem;
                        if(benefit.productId == '') {
                            benefit.productId = null;
                        }
                        newbenefit.SandDV1__Product__c = benefit.productId;
                        newbenefit.SandDV1__Scheme_Benefit_Range__c = benefitRangeMap.get(benefit.uniqueId);
                        newbenefit.SandDV1__Type__c = benefit.selectedSCType;
                        newbenefit.SandDV1__Value__c = benefit.value;
                        newbenefit.Id = null;
                        benefitSet.add(benefit.recordId);
                    schemeBenefitList.add(newbenefit);
                }
            }
            
            deleteBenefitList = [select Id,SandDV1__Scheme_Benefit_Range__c from SandDV1__Scheme_Benefit__c where SandDV1__Scheme_Benefit_Range__c NOT In:benefitRangeSet AND Id In:benefitSet];

            if(!deleteBenefitList.isEmpty()) {
                //Before Delete
                SecurityUtils.checkObjectIsDeletable(SandDV1__Scheme_Benefit_Range__c.SObjectType);
                delete deleteBenefitList;
            }

            if(!schemeBenefitList.isEmpty()) {
                //Before Upsert
                List<Schema.SObjectField> schemeBenefitFieldsList = FieldAccessibilityUtility.fetchSchemeBenefitields();
                SecurityUtils.checkInsert(SandDV1__Scheme_Benefit__c.SObjectType,schemeBenefitFieldsList);
                SecurityUtils.checkUpdate(SandDV1__Scheme_Benefit__c.SObjectType,schemeBenefitFieldsList);
                upsert schemeBenefitList;
            }
            return returnSchemeID ;
        }
        catch(Exception e) {
            Database.rollback(sp);
            System.debug('e.......'+e);
            throw new CustomException(e.getMessage());
           // return null;
        }
    }
    
    //vijay
    /*******************************************************************************************************
    * @description :- Method to fetch Active Scheme and scheme condition records.
    * @param :- null.
    * @return :- List<SandDV1__Scheme__c>.
    */
    public static List<SandDV1__Scheme__c> getAllActiveSchemes() {
        return new List<SandDV1__Scheme__c>([SELECT Id, Name, SandDV1__Has_Primary_Condition__c,
            (SELECT Id, Name, SandDV1__FieldApi__c, SandDV1__ObjectApi__c,SandDV1__System_Value__c,SandDV1__Parameter__c,SandDV1__DataType__c ,SandDV1__Scheme__c, SandDV1__Value__c FROM SandDV1__Scheme_Condition__r) 
            FROM SandDV1__Scheme__c 
            WHERE SandDV1__Status__c = :scheme_Active_Status]);
    }

    /*******************************************************************************************************
    * @description :- Method to check scheme is applicable or not.
    * @param :- SandDV1__Scheme__c scheme, SandDV1__Sales_Order__c salesOrder, SandDV1__Sales_Order_Line_Item__c salesOrderLineItem,
                        Product2 productObj,Account accountObj.
    * @return :- Boolean.
    */
    public static Boolean isSchemeApplicable(SandDV1__Scheme__c scheme, SandDV1__Sales_Order__c salesOrder, SandDV1__Sales_Order_Line_Item__c salesOrderLineItem,
                        Product2 productObj,Account accountObj) {
        Boolean isSchemeApplicable = true;

        for(SandDV1__Scheme_Condition__c schemeCondition :scheme.SandDV1__Scheme_Condition__r) {
            //modified for product and account
            /*if(!ServiceFacade.isConditionApplicable(schemeCondition, salesOrder, salesOrderLineItem)){
                isSchemeApplicable = false;
                break;
            }*/
            if(!ServiceFacade.isConditionApplicable(schemeCondition, salesOrder, salesOrderLineItem,productObj,accountObj)) {
                isSchemeApplicable = false;
                break;
            }
            //end
        }
        return isSchemeApplicable;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch applicable schemes and put data in wrapper class.
    * @param :- List<SandDV1__Scheme__c> schemesList, SandDV1__Sales_Order__c salesOrder, 
                SandDV1__Sales_Order_Line_Item__c salesOrderLineItem,Product2 productObj,Account accountObj.
    * @return :- SchemeDomain.ApplicableSchemesWrapper.
    */
    public static SchemeDomain.ApplicableSchemesWrapper getApplicableSchemes(List<SandDV1__Scheme__c> schemesList, SandDV1__Sales_Order__c salesOrder, 
                                    SandDV1__Sales_Order_Line_Item__c salesOrderLineItem,Product2 productObj,Account accountObj) {
        SchemeDomain.ApplicableSchemesWrapper applicableSchemesWrapper = new SchemeDomain.ApplicableSchemesWrapper();
        applicableSchemesWrapper.salesOrder = salesOrder;
        applicableSchemesWrapper.salesOrderLineItem = salesOrderLineItem;
        
        for(SandDV1__Scheme__c scheme :schemesList) {
            //modified for product and account
            /*if(isSchemeApplicable(scheme, salesOrder, salesOrderLineItem)){
                applicableSchemesWrapper.schemesList.add(scheme);
            }*/
            if(isSchemeApplicable(scheme, salesOrder, salesOrderLineItem,productObj,accountObj)) {
                applicableSchemesWrapper.schemesList.add(scheme);
            }
            //end
        }

        return applicableSchemesWrapper;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch applicable schemes and put data in wrapper class.
    * @param :- List<SandDV1__Scheme__c> schemesList, SandDV1__Sales_Order__c salesOrder, 
                                                List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemsList.
    * @return :- List<SchemeDomain.ApplicableSchemesWrapper>.
    */
    public static List<SchemeDomain.ApplicableSchemesWrapper> getApplicableSchemes(List<SandDV1__Scheme__c> schemesList, SandDV1__Sales_Order__c salesOrder, 
                                                List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemsList) {
        List<SchemeDomain.ApplicableSchemesWrapper> applicableSchemesWrapperList = new List<SchemeDomain.ApplicableSchemesWrapper>();

        //modified for Product and Account 
        Set<Id> productSet = new Set<Id>();
        Map<Id,Product2> prodMap = new Map<Id,Product2>();
        Map<Id,Account> accountMap = new Map<Id,Account>();
        for(SandDV1__Sales_Order_Line_Item__c salesOrderLineItem :salesOrderLineItemsList) {
            productSet.add(salesOrderLineItem.SandDV1__Part__c);
        }
        for(Product2 prod : [Select Id,SandDV1__Brand__c,SandDV1__Category__c,Name from Product2 where Id In:productSet]) {
            prodMap.put(prod.Id,prod);
        }
        if(salesOrder.SandDV1__Buyer__c != null) {
            for(Account acc : [Select Id,Name,SandDV1__Account_Category__c from Account where Id =:salesOrder.SandDV1__Buyer__c]) {
                accountMap.put(acc.Id,acc);
            }
        }
        
        for(SandDV1__Sales_Order_Line_Item__c salesOrderLineItem :salesOrderLineItemsList){
            applicableSchemesWrapperList.add(getApplicableSchemes(schemesList, salesOrder, salesOrderLineItem,prodMap.get(salesOrderLineItem.SandDV1__Part__c),accountMap.get(salesOrder.SandDV1__Buyer__c)));
        }
        //end
        return applicableSchemesWrapperList;
    }
}