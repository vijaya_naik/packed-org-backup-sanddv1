/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan(ET Marlabs).
* @date 8/21/2015..
* @description This class is used to deactive bucket using custom button.
*/

public with sharing class BucketDeActivationController {
  Id currentRecordID;

  public BucketDeActivationController(ApexPages.StandardController stdController) {
      currentRecordID = ApexPages.currentPage().getParameters().get('id');
  }
  
  /*******************************************************************************************************
  * @description :- method is used to call deactivation method from bucket service class and return page to current bucket id..
  * @param :- null.
  * @return :- PageReference. 
  */
  public PageReference deActivationCall() {
      String rootBucketID = null;
      Boolean isBlocked = false;
      PageReference pg = new PageReference('/'+currentRecordID);
      Set<Id> rootBucketSet = new Set<Id>();
      Set<Id> bucketSet = new Set<Id>();
      bucketSet.add(currentRecordID);
      BucketService.executeDeActiveBucket(bucketSet);
      return pg;
  }

}