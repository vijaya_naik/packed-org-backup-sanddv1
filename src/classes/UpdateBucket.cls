/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
/**
* @author Ankita Trehan (ET Marlabs).
* @date 
* @description :- This Class is used to update bucket using http callout after completion of future call.
*/

global with sharing class UpdateBucket {

    /*******************************************************************************************************
    * @description :- InvocableMethod is used to update bucket using http callout after completion of future call.
    * @param :- List<BucketUpdateRequest> requests
    * @return :- void.
    */
    
	@InvocableMethod(label='Update Bucket' description='Update System Future Call Field in Bucket')
    global static void AssignRecordsToBucket(List<BucketUpdateRequest> requests) {
        for(BucketUpdateRequest req : requests) {
            SandDV1__Bucket__c bucket = new SandDV1__Bucket__c(Id = req.recordId,SandDV1__System_Future_Call__c = req.systemfuturecall);
            update bucket;
        }          
    }

	global class BucketUpdateRequest {
        @InvocableVariable(label='Record Id' description='Id of Bucket' required=true)
        global Id recordId;

        @InvocableVariable(label='Record System Future Call' description='Check Future Call is Completed or not' required=true)
        global Boolean systemfuturecall;
    }  

}