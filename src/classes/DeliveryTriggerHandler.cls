/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class DeliveryTriggerHandler {
    
    private static DeliveryTriggerHandler instance;

    
    public static DeliveryTriggerHandler getInstance() {
        if (instance == null) {
            instance = new DeliveryTriggerHandler();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description :- Method is used to make status open when delicery order created.
    * @param :- List<SandDV1__Delivery_Order__c> newObjects.
    * @return :- void . 
    */
    
    public void onBeforeInsert(List<SandDV1__Delivery_Order__c> newObjects) {
        for(SandDV1__Delivery_Order__c delivery : newObjects) {
            delivery.SandDV1__Status__c = System.Label.DeliveryOrderOpenStatus;
        }
    }

    /*******************************************************************************************************
    * @description :- Method is used to make Quantity = 0 if user enter Quantity = blank.
    * @param :- List<SandDV1__Delivery_Order_Line_Item__c> newObjects.
    * @return :- void . 
    */
    
    public void onBeforeInsertUpdate(List<SandDV1__Delivery_Order_Line_Item__c> newObjects) {
        for(SandDV1__Delivery_Order_Line_Item__c doli : newObjects) {
            if(doli.SandDV1__Quantity__c == null){
                doli.SandDV1__Quantity__c = 0;
            }
        }
    }
    
    /*******************************************************************************************************
    * @description :- Method is used on after insert to fetch delivery order Qunatity and 
                        then update salesorder filled qunatity.
    * @param :- List<SandDV1__Delivery_Order_Line_Item__c> newObjects.
    * @return :- void.
    */
    
    public void onAfterInsert(List<SandDV1__Delivery_Order_Line_Item__c> newObjects) {

        Map<Id,decimal> addQunatityMap = new Map<Id,decimal>();

        for(SandDV1__Delivery_Order_Line_Item__c doli : newObjects) {
            if(doli.SandDV1__System_SalesOrderLine_Item__c != null){
                addQunatityMap.put(doli.SandDV1__System_SalesOrderLine_Item__c,doli.SandDV1__Quantity__c);
            }
        }
        
        if(!addQunatityMap.isempty()){
            updateSalesOrder(addQunatityMap);
        }
    }

    /*******************************************************************************************************
    * @description :- Method is used on after update to fetch delivery order Qunatity (check old and new value of qunatity) and 
                    then update salesorder filled qunatity. If old Quantity > new Quantity then subtract Quantity 
                    from salesorder otherwise add qunatity.
    * @param :- List<SandDV1__Delivery_Order_Line_Item__c> newObjects,Map<Id,SandDV1__Delivery_Order_Line_Item__c> oldDelivbery;
    * @return :- void.
    */
    
    public void onAfterUpdate(List<SandDV1__Delivery_Order_Line_Item__c> newObjects,Map<Id,SandDV1__Delivery_Order_Line_Item__c> oldDelivbery) {
        Map<Id,decimal> addQunatityMap = new Map<Id,decimal>();

        for(SandDV1__Delivery_Order_Line_Item__c dOLI : newObjects) {
            if(dOLI.SandDV1__Quantity__c == null) {
                dOLI.SandDV1__Quantity__c = 0;
            }
            if(oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c == null) {
                oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c = 0;
            }
            if(dOLI.SandDV1__Quantity__c != oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c && dOLI.SandDV1__Quantity__c > oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c) {
                decimal newQuantity = dOLI.SandDV1__Quantity__c - oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c;
                if(dOLI.SandDV1__System_SalesOrderLine_Item__c != null){
                    addQunatityMap.put(dOLI.SandDV1__System_SalesOrderLine_Item__c,newQuantity);
                }
            }
            if(dOLI.SandDV1__Quantity__c != oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c && dOLI.SandDV1__Quantity__c < oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c) {
                decimal newQuantity = dOLI.SandDV1__Quantity__c - oldDelivbery.get(dOLI.Id).SandDV1__Quantity__c;
                if(dOLI.SandDV1__System_SalesOrderLine_Item__c != null){
                    addQunatityMap.put(dOLI.SandDV1__System_SalesOrderLine_Item__c,newQuantity);
                }
            }
        }
        if(!addQunatityMap.isempty()){
            updateSalesOrder(addQunatityMap);
        }
    }

    /*******************************************************************************************************
    * @description :- on before delete subtract sales order line item quantity from delivery order.
    * @param :- List<SandDV1__Delivery_Order_Line_Item__c> oldObjects
    * @return :- void.
    */
    
    public void onBeforeDelete(List<SandDV1__Delivery_Order_Line_Item__c> oldObjects,Boolean isDelete) {
        Map<Id,decimal> addQunatityMap = new Map<Id,decimal>();
        Set<String> statusSet = ConfigurationService.getStatusSet('SandDV1__Delivery_Order__c','Allow_Delivery_Delete');

        for(SandDV1__Delivery_Order_Line_Item__c dOLI : oldObjects) {
            if(!statusSet.contains(dOLI.SandDV1__System_DeliveryOrder_Status__c) && isDelete == true){
                dOLI.adderror(+system.Label.Can_tDeleteRecordMessage);
            }
            else{
                if(dOLI.SandDV1__Quantity__c == null){
                    dOLI.SandDV1__Quantity__c = 0;
                }
                decimal newQuantity = -dOLI.SandDV1__Quantity__c;
                if(dOLI.SandDV1__System_SalesOrderLine_Item__c != null){
                    addQunatityMap.put(dOLI.SandDV1__System_SalesOrderLine_Item__c,newQuantity);
                }
            }
            
        }
        if(!addQunatityMap.isempty()){
            updateSalesOrder(addQunatityMap);
        }
        
    }

    /*******************************************************************************************************
    * @description :- on before delete subtract sales order line item quantity from delivery order.
    * @param :- List<SandDV1__Delivery_Order_Line_Item__c> oldObjects
    * @return :- void.
    */
    
    /*public void onBeforeDeliveryLineDelete(List<SandDV1__Delivery_Order_Line_Item__c> oldObjects) {
        Map<Id,decimal> addQunatityMap = new Map<Id,decimal>();
        Set<String> statusSet = ConfigurationService.getStatusSet('SandDV1__Delivery_Order__c','Allow_Delivery_Delete');

        for(SandDV1__Delivery_Order_Line_Item__c dOLI : oldObjects) {
            if(dOLI.SandDV1__Quantity__c == null){
                dOLI.SandDV1__Quantity__c = 0;
            }
            decimal newQuantity = -dOLI.SandDV1__Quantity__c;
            if(dOLI.SandDV1__System_SalesOrderLine_Item__c != null){
                addQunatityMap.put(dOLI.SandDV1__System_SalesOrderLine_Item__c,newQuantity);
            } 
        }
        if(!addQunatityMap.isempty()){
            updateSalesOrder(addQunatityMap);
        }
    }*/

    
    /*******************************************************************************************************
    * @description :- method used to update salesorder line item quantity.
    * @param :- Map<Id,decimal> addQunatityMap.
    * @return :- void.
    */
    
    public void updateSalesOrder(Map<Id,decimal> addQunatityMap) {
        Map<Id,SandDV1__Sales_Order_Line_Item__c> salesOrderLineItemMap = new Map<Id,SandDV1__Sales_Order_Line_Item__c>();
        List<SandDV1__Sales_Order_Line_Item__c> updateSalesOrderList = new List<SandDV1__Sales_Order_Line_Item__c>();
        try{
            updateSalesOrderList  = ServiceFacade.fetchSalesOLIRecords(addQunatityMap.keyset());
            
            for(SandDV1__Sales_Order_Line_Item__c soli : updateSalesOrderList) {
                if(soli.SandDV1__Fulfilled_Quantity__c == null) {
                    soli.SandDV1__Fulfilled_Quantity__c = 0;
                }
                soli.SandDV1__Fulfilled_Quantity__c = soli.SandDV1__Fulfilled_Quantity__c + addQunatityMap.get(soli.Id);
                salesOrderLineItemMap.put(soli.Id,soli);
            }

            if(!updateSalesOrderList.isEmpty()) {
                update updateSalesOrderList  ;
            } 
        }
        catch(Exception e){
            System.debug('e.....'+e);
        }
    }

    /*******************************************************************************************************
    * @description :- on delete of delivery order check sales order line item quantity from delivery order.
    * @param :- List<SandDV1__Delivery_Order__c> oldDeliveryList.
    * @return :- void.
    */
    public void onDeleteofDeliveryOrder(List<SandDV1__Delivery_Order__c> oldDeliveryList) {
        Set<Id> deliverySet = new Set<Id>();
        Set<String> statusSet = ConfigurationService.getStatusSet('SandDV1__Delivery_Order__c','Allow_Delivery_Delete');
        Boolean isError = false;

        for(SandDV1__Delivery_Order__c deliveryOrder : oldDeliveryList) {
            if(!statusSet.contains(deliveryOrder.SandDV1__Status__c)){
                isError = true;
                deliveryOrder.adderror(+system.Label.Can_tDeleteRecordMessage);
            }
            else {
                deliverySet.add(deliveryOrder.Id);
            }
            
        }
        if(isError == false) {
            List<SandDV1__Delivery_Order_Line_Item__c> deliveryOrderLineItemList = ServiceFacade.fetchDeliveryOLIRecords(deliverySet);
            onBeforeDelete(deliveryOrderLineItemList,true);
        }
       
    }

    /*******************************************************************************************************
    * @description :- on update of delivery order check sales order line item quantity from delivery order.
    * @param :- Map<Id,SandDV1__Delivery_Order__c> newObjectMap,Map<Id,SandDV1__Delivery_Order__c> oldObjectMap.
    * @return :- void
    */
    public void onUpdateDeliveryOrder(Map<Id,SandDV1__Delivery_Order__c> newObjectMap,Map<Id,SandDV1__Delivery_Order__c> oldObjectMap) {
        Set<Id> deliverySet = new Set<Id>();
        for(Id deliveryId : newObjectMap.keyset()) {
            if(newObjectMap.get(deliveryId).SandDV1__Status__c == System.Label.DeliveryOrderCancelStatus && 
                newObjectMap.get(deliveryId).SandDV1__Status__c != oldObjectMap.get(deliveryId).SandDV1__Status__c) {
                deliverySet.add(deliveryId);
            }
        }

        List<SandDV1__Delivery_Order_Line_Item__c> deliveryOrderLineItemList = ServiceFacade.fetchDeliveryOLIRecords(deliverySet);
        onBeforeDelete(deliveryOrderLineItemList,false);
    }

   
}