/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 07-Jan-2016
* @description This is a service class for Complaint.
*/
public with sharing class ComplaintService {

	/*******************************************************************************************************
	* @description .Method to fetch complaints
	* @param <param name> .accId - Account Id
	* @return <return type> .List of Complaint object
	*/
  
	public static List<SandDV1__Complaint__c> fetchComplaints(String accId){

		return [SELECT SandDV1__Account__c,SandDV1__Complaint_Duration__c,CreatedDate,Id,SandDV1__IsClosed__c,Name,SandDV1__Priority__c,SandDV1__Type__c,SandDV1__Status__c FROM SandDV1__Complaint__c 
	              WHERE SandDV1__IsClosed__c = false AND SandDV1__Account__c = :accId ORDER BY SandDV1__Complaint_Duration__c DESC NULLS FIRST];
	}
}