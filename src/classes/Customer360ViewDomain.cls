/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 03-Nov-2015
* @description This is a Domain class for Customer360ViewController.
*/

public with sharing class Customer360ViewDomain {
  
  /*******************************************************************************************************
    * @description :Wrapper class of account,contact,visit etc
    * @param 
    * @return 
    */
  public class CustomerWrapper{ 

        public Account acc {get;set;}
        public List<Contact> contactList {get; set;}
        public ComplaintWrapper complaintWrap {get; set;}
        public OpenActivitiesWrapper openActivityWrap {get; set;}
        public List<SandDV1__Visit__c> visitList {get; set;}
        public List<SandDV1__Sales_Order__c> salesOrderList  {get; set;}
        public List<SandDV1__Delivery_Order__c> deliveryOrderList {get; set;}
        public Boolean isCheckIn {get;set;}
        public Boolean isCheckOut {get;set;}

        public CustomerWrapper(){

            this.complaintWrap = new ComplaintWrapper(); 
            this.openActivityWrap = new OpenActivitiesWrapper();
            this.isCheckIn = false;
            this.isCheckOut = false;
        }         

    }


    public class ComplaintWrapper {

        public Integer escalated {get;set;}
        public Integer high {get;set;}
        public Integer medium {get;set;}
        public Integer low {get;set;}

        public ComplaintWrapper(){

            this.escalated = 0;
            this.high = 0;
            this.medium = 0;
            this.low = 0;

        }
    }


    public class OpenActivitiesWrapper {

        public Integer overDue {get;set;}
        public Integer high {get;set;}
        public Integer medium {get;set;}
        public Integer low {get;set;}

        public OpenActivitiesWrapper(){

            this.overDue = 0;
            this.high = 0;
            this.medium = 0;
            this.low = 0;

        }
    }
}