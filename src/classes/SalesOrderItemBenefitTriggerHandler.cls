public with sharing class SalesOrderItemBenefitTriggerHandler {
    public static void onAfterInsert(List<SandDV1__Sales_Order_Item_Benefit__c> newSalesOrderItemBenefitList){
        SalesOrderItemBenefitService.applySchemeBenefits(newSalesOrderItemBenefitList);
    }


    public static void onBeforeDelete(List<SandDV1__Sales_Order_Item_Benefit__c> oldSalesOrderItemBenefitList){
        SalesOrderItemBenefitService.revokeSchemeBenefits(oldSalesOrderItemBenefitList);
    }
    
    public static void onAfterDelete(List<SandDV1__Sales_Order_Item_Benefit__c> oldSalesOrderItemBenefitList){
        SalesOrderItemBenefitService.afterDeleteSalesOrderBenefit(oldSalesOrderItemBenefitList);
    }


    public static void onBeforeInsert(List<SandDV1__Sales_Order_Item_Benefit__c> newSalesOrderItemBenefitList){
        setOrderItemBenefitCombinationField(newSalesOrderItemBenefitList);
    }


    public static void onBeforeUpdate(List<SandDV1__Sales_Order_Item_Benefit__c> newSalesOrderItemBenefitList){
        setOrderItemBenefitCombinationField(newSalesOrderItemBenefitList);
    }


    private static void setOrderItemBenefitCombinationField(List<SandDV1__Sales_Order_Item_Benefit__c> newSalesOrderItemBenefitList){
        for(SandDV1__Sales_Order_Item_Benefit__c salesOrderItemBenefit :newSalesOrderItemBenefitList){
            salesOrderItemBenefit.SandDV1__Order_Item_Benefit_Combination__c = (String) salesOrderItemBenefit.SandDV1__Sales_Order_Line_Item__c + (String) salesOrderItemBenefit.SandDV1__Scheme_Benefit__c;
        }
    }
}