/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a service Facade
*/
public with sharing class ShippingAddressService {
	

	/*******************************************************************************************************
    * @description : method to retrive shipping based on account Id and query string
    * @param : accId - account Id,shipName - ship name to query
    * @return List of shipping address . 
    */
    
    public static List<SandDV1__Shipping_Address__c> fetchShippingByAccId(String accId){


        return [SELECT Id,Name,SandDV1__Account__c,SandDV1__Shipping_City__c,SandDV1__Shipping_Country__c,SandDV1__Shipping_State_Province__c,
        SandDV1__Shipping_Street__c,SandDV1__Shipping_Zip_Postal_Code__c FROM SandDV1__Shipping_Address__c WHERE SandDV1__Account__c =:accId ORDER BY Name];  

    }
}