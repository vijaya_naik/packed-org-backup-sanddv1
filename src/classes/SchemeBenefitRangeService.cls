/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeBenefitRangeService {
    /*******************************************************************************************************
    * @description :- method to fetch scheme benefit range records and create a map (key as scheme id and values as scheme benefit range records)
    * @param :-Set<String> schemeIdSet
    * @return :- Map<String, List<SandDV1__Scheme_Benefit_Range__c>>
    */
    
    public static Map<String, List<SandDV1__Scheme_Benefit_Range__c>> getSchemeBenefitRangeMap(Set<String> schemeIdSet){
        Map<String, List<SandDV1__Scheme_Benefit_Range__c>> schemeBenefitRangeMap = new Map<String, List<SandDV1__Scheme_Benefit_Range__c>>();
        
        for(SandDV1__Scheme_Benefit_Range__c schemeBenefitRange :getSchemeBenefitRangeList(schemeIdSet)){
            if(schemeBenefitRangeMap.containsKey(schemeBenefitRange.SandDV1__Scheme__c)){
                schemeBenefitRangeMap.get(schemeBenefitRange.SandDV1__Scheme__c).add(schemeBenefitRange);
            }
            else{
                schemeBenefitRangeMap.put(schemeBenefitRange.SandDV1__Scheme__c, new List<SandDV1__Scheme_Benefit_Range__c>{schemeBenefitRange});
            }
        }
        
        return schemeBenefitRangeMap;
    }
    
    /*******************************************************************************************************
    * @description :- method to fetch scheme benefit range records with set of scheme id's.
    * @param :-Set<String> schemeIdSet.
    * @return :- List<SandDV1__Scheme_Benefit_Range__c>.
    */
    public static List<SandDV1__Scheme_Benefit_Range__c> getSchemeBenefitRangeList(Set<String> schemeIdSet){
        return new List<SandDV1__Scheme_Benefit_Range__c>([SELECT Id, Name, SandDV1__Scheme__c, SandDV1__Min__c, SandDV1__Max__c 
            FROM SandDV1__Scheme_Benefit_Range__c 
            WHERE SandDV1__Scheme__c IN :schemeIdSet]);
    }

    /*******************************************************************************************************
    * @description :- method to fetch fetch applicable scheme benefit range records according to salesorder line item quantity
    * @param :-SchemeDomain.ApplicableSchemesWrapper schemeWrapper, Map<String, List<SandDV1__Scheme_Benefit_Range__c>> schemeBenefitRangeMap
    * @return :- SchemeDomain.SchemeBenefitRangeWrapper
    */
    public static SchemeDomain.SchemeBenefitRangeWrapper getApplicableSchemeBenefitRange(SchemeDomain.ApplicableSchemesWrapper schemeWrapper, Map<String, List<SandDV1__Scheme_Benefit_Range__c>> schemeBenefitRangeMap){
        SchemeDomain.SchemeBenefitRangeWrapper schemeBenefitRangeWrapper = new SchemeDomain.SchemeBenefitRangeWrapper();
        schemeBenefitRangeWrapper.salesOrder = schemeWrapper.salesOrder;
        schemeBenefitRangeWrapper.salesOrderLineItem = schemeWrapper.salesOrderLineItem;

        for(SandDV1__Scheme__c scheme :schemeWrapper.schemesList){
            if(schemeBenefitRangeMap.containsKey(scheme.Id)){
                for(SandDV1__Scheme_Benefit_Range__c schemeBenefitRange :schemeBenefitRangeMap.get(scheme.Id)){
                    if(
                        schemeBenefitRange.SandDV1__Min__c <= schemeWrapper.salesOrderLineItem.SandDV1__Quantity__c &&
                        (
                            schemeBenefitRange.SandDV1__Max__c == null ||
                            schemeBenefitRange.SandDV1__Max__c >= schemeWrapper.salesOrderLineItem.SandDV1__Quantity__c
                        )
                    ){
                        schemeBenefitRangeWrapper.schemeBenefitRangeList.add(schemeBenefitRange);
                    }
                }
            }
        }

        return schemeBenefitRangeWrapper;
    }

    /*******************************************************************************************************
    * @description :- method to fetch fetch list of applicable scheme benefit range records according to salesorder line item quantity
    * @param :-List<SchemeDomain.ApplicableSchemesWrapper> schemeWrapperList, Map<String, List<SandDV1__Scheme_Benefit_Range__c>> schemeBenefitRangeMap
    * @return :- List<SchemeDomain.SchemeBenefitRangeWrapper>
    */
    public static List<SchemeDomain.SchemeBenefitRangeWrapper> getApplicableSchemeBenefitRange(List<SchemeDomain.ApplicableSchemesWrapper> schemeWrapperList, Map<String, List<SandDV1__Scheme_Benefit_Range__c>> schemeBenefitRangeMap){
        List<SchemeDomain.SchemeBenefitRangeWrapper> schemeBenefitRangeWrapperList = new List<SchemeDomain.SchemeBenefitRangeWrapper>();

        for(SchemeDomain.ApplicableSchemesWrapper schemeWrapper :schemeWrapperList){
            schemeBenefitRangeWrapperList.add(getApplicableSchemeBenefitRange(schemeWrapper, schemeBenefitRangeMap));
        }

        return schemeBenefitRangeWrapperList;
    }
    
    /*******************************************************************************************************
    * @description :- method to fetch schemebenefit range records according to scheme id
    * @param :-String schemeID
    * @return :-List<SandDV1__Scheme_Benefit_Range__c>
    */
    public static List<SandDV1__Scheme_Benefit_Range__c> fetchSchemeBenefitRange(String schemeID){
        return [Select Id,Name,SandDV1__Max__c,SandDV1__Min__c,SandDV1__Scheme__c from SandDV1__Scheme_Benefit_Range__c where SandDV1__Scheme__c =: schemeID];
    }
}