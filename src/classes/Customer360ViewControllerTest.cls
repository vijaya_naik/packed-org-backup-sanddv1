/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 03-Nov-2015
* @description This is a Test class for Customer360ViewController.
*/

@isTest(seeAllData = false)
private class Customer360ViewControllerTest {
    
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static Account testAccount;
    static SandDV1__Sales_Order__c testSalesOrder;
    static SandDV1__Visit__c testVisit;
    static SandDV1__Visit_Plan__c testVisitPlan;
    static SandDV1__Complaint__c testComplaint;
    static SandDV1__Complaint__c testComplaint2;
    static SandDV1__Complaint__c testComplaint3;
    static SandDV1__Period__c testPeriod;
    static Task testTask;
    static Task testTask2;
    static Task testTask3;

    /*******************************************************************************************************
    * @description: Setting up test data for the test class.
    *  
    */

    static void testDataInit() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.assertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        testUser.ManagerId = sysAdmin.Id;
        insert testUser;

        System.runAs(sysAdmin){

            testAccount = Initial_Test_Data.createAccount('Test Account', 500,false);
            insert testAccount;

            testSalesOrder = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',null);
            insert testSalesOrder;

            testPeriod = Initial_Test_Data.createPeriod('Quarter',null,20);
            insert testPeriod;

            testVisitPlan = Initial_Test_Data.createVisitPlan(testPeriod.Id,testUser.Id);
            insert testVisitPlan;
            System.assertEquals(testVisitPlan.SandDV1__Period__c,testPeriod.Id);

            testVisit = Initial_Test_Data.createVisit(testVisitPlan.Id,testAccount.Id);
            insert testVisit;
            System.assertEquals(testVisit.SandDV1__Visit_Plan__c,testVisitPlan.Id);

            List<Task> taskList = new List<Task>();
            testTask = Initial_Test_Data.createTask(testAccount.Id,'High',testUser.Id,2);
            testTask2 = Initial_Test_Data.createTask(testAccount.Id,'Medium',testUser.Id,3);
            testTask3 = Initial_Test_Data.createTask(testAccount.Id,'Low',testUser.Id,-3);
            taskList.add(testTask);
            taskList.add(testTask2);
            taskList.add(testTask3);

            insert taskList;

            List<SandDV1__Complaint__c> compList = new List<SandDV1__Complaint__c>();

            testComplaint = Initial_Test_Data.createComplaint(testAccount.Id, 'High');
            compList.add(testComplaint);

            testComplaint2 = Initial_Test_Data.createComplaint(testAccount.Id, 'Medium');
            compList.add(testComplaint2);

            testComplaint3 = Initial_Test_Data.createComplaint(testAccount.Id, 'Low');
            compList.add(testComplaint3);

            insert compList;
        }
    }

    private static testMethod void test_method_one() {

        testDataInit();
        System.runAs(testUser){
            Test.startTest();

                Customer360ViewController.fetchAllDetails(testAccount.Id, testVisit.Id);
                testVisit.SandDV1__Status__c = 'In Progress';
                Customer360ViewController.visitCheckInOut(testVisit);
                Customer360ViewController.fetchAllDetails(testAccount.Id, testVisit.Id);


            Test.stopTest();
        }
    }
}