/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author Extentor
* @date Aug 2015
* @description This class is for providing model to VisitPlanningController in the application
*/

Public with sharing class VisitPlanningModel {
    
    /*******************************************************************************************************
* @description VisitPlan wrapper response.
*/
    public class VisitPlanResponseWrapper {
        
        public PeriodWrapper periodValues;
        public list<TotalVisitPlannedWrapper> totalVisitValues;
        public list<VisitTargetWrapper> visitTargetValues;
        public list<CustomerWrapper> customerValues;
        
        
        public VisitPlanResponseWrapper() {
            
            periodValues         =  new PeriodWrapper();
            totalVisitValues     =  new list<TotalVisitPlannedWrapper>();
            visitTargetValues    =  new list<VisitTargetWrapper>();
            customerValues       =  new list<CustomerWrapper>();
            
        }
    }
    
    /*******************************************************************************************************
* @description Period wrapper with Date & Month.
*/
    public class PeriodWrapper {
        
        public String tableName;
        public list<String> monthWithYear;
        public list<Integer> numberOfDaysInMonth;
        public list<Integer> dayOfMonth; 
        
        public PeriodWrapper() {
            
            tableName              =  '';
            monthWithYear          =  new list<String>(); //set
            dayOfMonth             =  new list<Integer>();
            numberOfDaysInMonth    =  new list<Integer>();
        }
    }
    
     /*******************************************************************************************************
* @description Customer Data Wrapper
*/
    public class CustomerWrapper {
        
        public string customerName;
        public string customerCategory;
        public Decimal creditLimit;
        public string customerId;
        public string toolTipVal;
        public list<VisitPlanWrapper>  visitPlanWrapperList;
        public integer customerTargetValue;
        public string customerValues;
        
        public CustomerWrapper(String customerName, Id customerId, String customerCategory, Decimal creditLimit) {
            
            this.customerName             =   customerName;
            this.customerId               =   customerId;
            this.customerCategory         =   customerCategory;
            this.creditLimit              =   creditLimit;
            this.visitPlanWrapperList     =   new list<VisitPlanWrapper>();
            this.customerTargetValue      =   0;
            this.customerValues           = '<div><b>Customer:</b&nbsp;'+customerName+'</div>';
            if(customerName != null && customerName != '') {
                this.toolTipVal           =   'mouseenter';
                //customerName.length() > 12 ? 'mouseenter' : 'never';
            } else {
                this.toolTipVal           =   'never';
            }
            
        }
    }
    
     /*******************************************************************************************************
* @description Visit Plan wrapper with planned and holiday details.
*/
    public class VisitPlanWrapper {
        
        public Boolean isVisitPlanned;
        public Boolean isHoliday;
        public String  holidayReason;
        public String  dateValue;
        public String  visitId;
        
        public VisitPlanWrapper(Boolean isVisitPlanned, Boolean isHoliday, List<String> holidayReason, String dateValue, String visitId) {
            
            this.isVisitPlanned  =  isVisitPlanned;
            this.isHoliday       =  isHoliday;
            this.dateValue       =  dateValue;
            this.visitId         =  visitId;
            
            if(isHoliday) {
                this.holidayReason   =  '';
                for(string str : holidayReason) {
                    this.holidayReason += str +'\n\r';
                }
            }
            if(this.holidayReason != null) {
                this.holidayReason = this.holidayReason.Substring(0,this.holidayReason.length()-1);
            }
        }
    }
    
     /*******************************************************************************************************
* @description Total Number of visits planned Count.
*/
    public class TotalVisitPlannedWrapper {
        
        public Integer checkedIndexValue;
        public Integer checkedRowCount;
        public Boolean holidayFlag;
        
        public TotalVisitPlannedWrapper(Integer checkedIndexValue, Integer checkedRowCount, Boolean holidayFlag) {
            
            this.checkedIndexValue    =  checkedIndexValue;
            this.checkedRowCount      =  checkedRowCount;
            this.holidayFlag          =  holidayFlag;
        }
    }


    /*******************************************************************************************************
* @description Total Number of minimum visits targeted by the organisation.
*/
    public class VisitTargetWrapper {
        
        public Integer checkedIndexValue;
        public Integer checkedRowCount;
        public Boolean holidayFlag;
        
        public VisitTargetWrapper(Integer checkedIndexValue, Integer checkedRowCount, Boolean holidayFlag) {
            
            this.checkedIndexValue    =  checkedIndexValue;
            this.checkedRowCount      =  checkedRowCount; //!= null ? checkedRowCount : null;
            this.holidayFlag          =  holidayFlag;
        }
    }
    
    public class VisitWrapper {
        
        public list<SandDV1__Visit__c> todayVisits;
        public list<SandDV1__Visit__c> yesterdayVisits;
        public list<SandDV1__Visit__c> currentMonthVisits;
        
        public VisitWrapper(list<SandDV1__Visit__c> todayVisits, list<SandDV1__Visit__c> yesterdayVisits, list<SandDV1__Visit__c> currentMonthVisits) {
            
            this.todayVisits        = new list<SandDV1__Visit__c>(todayVisits);
            this.yesterdayVisits    = new list<SandDV1__Visit__c>(yesterdayVisits);
            this.currentMonthVisits = new list<SandDV1__Visit__c>(currentMonthVisits);
        }
    }

    public class ApprovalStatus {
        
        public String comments;
        public String status;

        public ApprovalStatus() {

            this.comments = '';
            this.status   = '';
        }
    }
}