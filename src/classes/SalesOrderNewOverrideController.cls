/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 
* @description This class is for SalesOrderNewOverridePage
*/

public with sharing class SalesOrderNewOverrideController {
  
    public String returnURL {get;set;}
    public String accountId {get;set;}
    public Boolean blockOrderCreation {get;set;}
    public Double creditLimit {get;set;}

    public SalesOrderNewOverrideController(ApexPages.StandardController con){

        Account acc;
        System.PageReference pr = con.cancel();
        returnURL = pr.getUrl();
        SObject so = con.getRecord();
        SandDV1__Sales_Order__c salesOrder = (SandDV1__Sales_Order__c)so;
        accountId = String.valueOf(salesOrder.SandDV1__Buyer__c);  

        if(accountId != null){

            /*Fetching Account credit limit and Block order creation to check the 
            User is able to create salesorder or not*/
            acc = [SELECT Id,SandDV1__Credit_Limit__c,SandDV1__Block_Order_Creation__c FROM Account WHERE Id =:accountId];
            creditLimit = acc.SandDV1__Credit_Limit__c;
            blockOrderCreation = acc.SandDV1__Block_Order_Creation__c;
        } 
     
    }
}