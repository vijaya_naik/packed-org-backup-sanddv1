/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 07-Jan-2016
* @description This is a service class for Visit.
*/
public with sharing class VisitService {
	
	/*******************************************************************************************************
	* @description .Method to fetch Visit
	* @param <param name> .accId - Account Id
	* @return <return type> .List of Visit object
	*/

	public static List<SandDV1__Visit__c> fetchVisits(String accId,Integer limitRecords){

		return [SELECT SandDV1__Account__c,Id,Name,SandDV1__System_Visit_Plan_Date__c,SandDV1__Visit_Plan__c,SandDV1__Status__c FROM SandDV1__Visit__c 
	                            WHERE SandDV1__Account__c = :accId ORDER BY SandDV1__Visit_Plan__c DESC NULLS FIRST LIMIT :limitRecords];  
	}

	/*******************************************************************************************************
	* @description .Method to fetch Visit
	* @param <param name> .visitId - Visit Id
	* @return <return type> .List of Visit object
	*/

	public static List<SandDV1__Visit__c> fetchVisitsById(String visitId){

		return [SELECT Id,SandDV1__Status__c FROM SandDV1__Visit__c WHERE Id = :visitId];  
	}


	/*******************************************************************************************************
	* @description .Method to fetch Visits
	* @param <param name> .visitDate - query based on the visit date
	* @return <return type> .List of Visit object
	*/

	public static List<SandDV1__Visit__c> fetchVisits(Date visitDate){

		return [SELECT Id,Name,SandDV1__Visit_Plan_Date__c,SandDV1__Account__r.Name FROM SandDV1__Visit__c WHERE SandDV1__Visit_Plan_Date__c =:visitDate AND CreatedById =:userinfo.getUserId() ORDER BY Name];  
	}
}