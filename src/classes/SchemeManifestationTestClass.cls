/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/*
Test Class is written to cover Scheme Module.
*/
@isTest(seeAllData = false)
private class SchemeManifestationTestClass {
    /*******************************************************************************************************
    * @description Intial values for the test run
    * @param null
    * @return null 
    */
    static UserRole adminRole;
    static User adminUser;
    static Account accountObj;
    static Product2 productObj;
    static Pricebook2 pricebookObj;
    static SandDV1__Sales_Order__c salesOrderObj;
    static SandDV1__Scheme__c schemeObj;
    static SandDV1__Scheme_Benefit_Range__c SchemeBenefitRange;
    static SandDV1__Scheme_Benefit__c schemeBenefit;
    static SandDV1__Scheme_Condition__c schemeCondition;
    static SandDV1__Scheme_Condition__c schemeCondition1;
    //static SandDV1__Sales_Order_Item_Benefit__c  salesOrderBenefit;

    static List<SandDV1__Sales_Order_Line_Item__c> salesOrderLIList = new List<SandDV1__Sales_Order_Line_Item__c>();
    static List<SandDV1__Scheme_Condition__c> schemeAssignmentList = new List<SandDV1__Scheme_Condition__c>();

    static void init() {
        adminRole = Initial_Test_Data.createRole('Parent role', null);
        System.assertequals(adminRole.name,'Parent role');
        insert adminRole;

        adminUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',adminRole.Id);
        insert adminUser;
        System.runAs(adminUser){
        //create dummy Account
        accountobj = InitializeTest.createAccount();
        insert accountobj;
        //create dummy product
        productObj = InitializeTest.createProduct();
        insert productObj;
        //create dummy price book
        pricebookObj = InitializeTest.createPricebook();
        insert pricebookObj;
        
        //create dummy sales order 
        salesOrderObj = InitializeTest.createSalesOrder(accountobj.Id,'Open');
        insert salesOrderObj;
        System.assertEquals(salesOrderObj.SandDV1__Status__c,'Open');

        // create multiple sales order line item under sales order 
        for(integer i=0;i<5;i++){
            SandDV1__Sales_Order_Line_Item__c salesOrderLIObj = InitializeTest.createSalesOrderLineItem(salesOrderObj.Id,productObj.Id,0,pricebookObj.Id,10,i);
            salesOrderLIList.add(salesOrderLIObj);
        }
        insert salesOrderLIList;

        //create Scheme
        schemeObj = InitializeTest.createScheme('TestScheme','Active',1);
        insert schemeObj;

        //create SchemeBenefitRange
        SchemeBenefitRange = InitializeTest.createSchemeBenefitRange(schemeObj.Id,30.00,1.00);
        insert SchemeBenefitRange;

        //create Scheme Benefit
        schemeBenefit = InitializeTest.createSchemeBenefit(productObj.Id,20,SchemeBenefitRange.Id,'FOC');
        insert schemeBenefit;
        System.assertEquals(schemeBenefit.SandDV1__Type__c,'FOC');

        String orderbenefitcombi = String.Valueof(salesOrderObj.Id) + String.Valueof(schemeBenefit.Id);
        //create Sales Order Benefit 
        //salesOrderBenefit = InitializeTest.createSalesOrderBenefit(salesOrderLIList[1].Id,schemeBenefit.Id,orderbenefitcombi);
        //insert salesOrderBenefit;
        //create Scheme Condition
        schemeCondition = InitializeTest.createSchemeCondition('SandDV1__Part__c','SandDV1__Sales_Order_Line_Item__c',productObj.Id,schemeObj.Id,'','');
        insert schemeCondition;

        //create Scheme Condition1   
        schemeCondition1 = InitializeTest.createSchemeCondition('Name','Product2',productObj.Id,schemeObj.Id,'Product [SKU]','Criteria');
        insert schemeCondition1;

        for(integer i=0;i<1;i++){
        //create Scheme Condition1  
        SandDV1__Scheme_Condition__c assignment = InitializeTest.createSchemeCondition('SandDV1__Category__c','Account',accountobj.Id,schemeObj.Id,'Customer Category','Assignment');
            schemeAssignmentList.add(assignment);
        }
        }
    }

    @isTest static void ManifestationTest() {

        init();
        System.runAs(adminUser){
        List<SchemeDomain.benfitRangeWrapper> benfitRangeList = new List<SchemeDomain.benfitRangeWrapper>();
        List<SchemeDomain.benfitWrapper> benefitList = new List<SchemeDomain.benfitWrapper>();
        List<SchemeDomain.benfitRangeWrapper> deleteschemeBenefitList = new List<SchemeDomain.benfitRangeWrapper>();
        List<SandDV1__Scheme_Condition__c> deletedAssignmentSchemeList = new List<SandDV1__Scheme_Condition__c>();
        
        SchemeDomain.criteriaWrapper critwrap = new SchemeDomain.criteriaWrapper();
        critwrap.schemeType = 'search';
        critwrap.objectApi = 'Product2';
        critwrap.fieldApi = 'Name';
        critwrap.returnType = 'list';
        critwrap.criteriavalue = 'Product [SKU]';
        critwrap.IsAssignment = false;

        SchemeDomain.schemeWrapper schemewrap = new SchemeDomain.schemeWrapper ();
        schemewrap.schemeObj = schemeObj;
        schemewrap.schemeType = 'Scheme Type 1';
        schemewrap.validFromDate = String.valueof(System.today());
        schemewrap.expireOnDate = String.valueof(System.today()+10);
        schemewrap.IsActive = true;

        SchemeDomain.benfitRangeWrapper benefitRange = new SchemeDomain.benfitRangeWrapper();
        benefitRange.min = '1';
        benefitRange.max = '10';
        benefitRange.range = '1-10';
        benefitRange.uniqueId = 'ab';
        benefitRange.recordId = null;
        benfitRangeList.add(benefitRange);

        SchemeDomain.benfitWrapper benefitWrapp = new SchemeDomain.benfitWrapper();
        benefitWrapp.productId = productObj.Id;
        benefitWrapp.productName = productObj.Name;
        benefitWrapp.range = '1-10';
        benefitWrapp.uniqueId = 'ab';
        benefitWrapp.value = 100.00;
        benefitWrapp.selectedSCType = 'FOC';
        benefitWrapp.selectedLineItem  = 'Line Item';
        benefitWrapp.recordId = null;
        benefitList.add(benefitWrapp);
        
        System.assertEquals(benefitWrapp.selectedLineItem,'Line Item');

        SchemeController.selectSchemeType();
        SchemeController.getCriteriaList();
        SchemeController.getValue('Product [SKU]');
        SchemeController.fetchCriteriaValue(critwrap,'Lap');
        SchemeController.getSchemeBenefitTypePicklist();
        SchemeController.getSchemeBenefitLevels();
        SchemeController.fetchSchemeCriteriaInfo();
        SchemeController.fetchAssignmentCriteriaInfo();
        SchemeController.fetchSchemeInfo();
        SchemeController.fetchProductList('Lap');
        SchemeController.fetchAllSchemeRecords(schemeObj.Id);
        SandDV1__Scheme_Condition__c cnd = new SandDV1__Scheme_Condition__c();
        SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = false;
        try {
            SchemeController.Save(schemewrap,schemeCondition1,schemeAssignmentList,benfitRangeList,benefitList,deleteschemeBenefitList,deletedAssignmentSchemeList,cnd);
        }
        catch(Exception e){
            Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to insert field') ? true : false;
            System.AssertEquals(expectedExceptionThrown, true);
        }
        //SchemeController.Save(schemewrap,schemeCondition1,schemeAssignmentList,benfitRangeList,benefitList,deleteschemeBenefitList,deletedAssignmentSchemeList,cnd);
        }
    }
    
}