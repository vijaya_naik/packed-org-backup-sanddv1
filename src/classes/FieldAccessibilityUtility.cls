/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class FieldAccessibilityUtility {

	public FieldAccessibilityUtility() {
		
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Delivery_Order__c object fields which are using while upsert of delivery order record.
						It is used in delivery order Service.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchDeliveryOrderFields() {

		List<Schema.SObjectField> deliveryFieldsList = new List<Schema.SObjectField>{SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Shipping_Address_Name__c,
			SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Shipping_Street__c,SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Shipping_City__c,
            SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Shipping_Country__c,SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Shipping_State_Province__c,
            SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Buyer__c,SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Date__c,SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__No_of_Parts__c,
            SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__PO_Reference__c,SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__PO_Status__c,
            SandDV1__Delivery_Order__c.SObjectType.fields.SandDV1__Sales_Order__c};

        return deliveryFieldsList;
	}
	
	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Delivery_Order_Line_Item__c object fields which are using while upsert of delivery order line item record.
					It is used in delivery order Service.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchDeliveryOrderLineItemFields() {

		List<Schema.SObjectField> deliveryOLIFieldsList = new List<Schema.SObjectField>{SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__Part__c,
			SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__Price_Book__c,SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__Quantity__c,
            SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__Net_Amount__c,SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__System_SalesOrderLine_Item__c,
            SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__Sales_Price__c,SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__Unit_Price__c,
            SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__UOM__c,SandDV1__Delivery_Order_Line_Item__c.SObjectType.fields.SandDV1__Type__c};
            
        return deliveryOLIFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Scheme__c object fields which are using while upsert of SandDV1__Scheme__c record.
						It is used in Scheme Service Class.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchSchemeFields() {

		List<Schema.SObjectField> schemeFieldsList = new List<Schema.SObjectField>{SandDV1__Scheme__c.SObjectType.fields.SandDV1__Scheme_Name__c,
			SandDV1__Scheme__c.SObjectType.fields.SandDV1__Budget_Limit__c,SandDV1__Scheme__c.SObjectType.fields.SandDV1__Type__c,
            SandDV1__Scheme__c.SObjectType.fields.SandDV1__Description__c,SandDV1__Scheme__c.SObjectType.fields.SandDV1__Valid_From__c,
            SandDV1__Scheme__c.SObjectType.fields.SandDV1__Expires_On__c,SandDV1__Scheme__c.SObjectType.fields.SandDV1__Status__c};            
        return schemeFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Scheme_Condition__c object fields which are using while upsert of SandDV1__Scheme_Condition__c record.
						It is used in Scheme Service Class.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchSchemeConditionFields() {

		List<Schema.SObjectField> schemeConditionFieldsList = new List<Schema.SObjectField>{SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__Parameter__c,
			SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__Value__c,SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__System_Value__c,
            SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__System_Scheme_Type__c,SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__FieldApi__c,
            SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__ObjectApi__c,SandDV1__Scheme_Condition__c.SObjectType.fields.SandDV1__DataType__c};            
        return schemeConditionFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Scheme_Benefit_Range__c object fields which are using while upsert of SandDV1__Scheme_Benefit_Range__c record.
						It is used in Scheme Service Class.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchSchemeBenefitRangeFields() {

		List<Schema.SObjectField> schemeBenefitRangeFieldsList = new List<Schema.SObjectField>{SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.SandDV1__Max__c,
			SandDV1__Scheme_Benefit_Range__c.SObjectType.fields.SandDV1__Min__c};            
        return schemeBenefitRangeFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Scheme_Benefit__c object fields which are using while upsert of SandDV1__Scheme_Benefit__c record.
						It is used in Scheme Service Class.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchSchemeBenefitields() {

		List<Schema.SObjectField> schemeBenefitFieldsList = new List<Schema.SObjectField>{SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Level__c,
			SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Product__c,SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Type__c,SandDV1__Scheme_Benefit__c.SObjectType.fields.SandDV1__Value__c};            
        
        return schemeBenefitFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__OnBoarding_Document__c object fields which are using while upsert of SandDV1__OnBoarding_Document__c record.
						It is used in Scheme Service Class.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchOnBoardingFields() {

		List<Schema.SObjectField> onBoardingFieldsList = new List<Schema.SObjectField>{SandDV1__OnBoarding_Document__c.SObjectType.fields.SandDV1__Approval_Process_Needed__c,
			SandDV1__OnBoarding_Document__c.SObjectType.fields.Name,SandDV1__OnBoarding_Document__c.SObjectType.fields.SandDV1__Comments__c,SandDV1__OnBoarding_Document__c.SObjectType.fields.SandDV1__Is_Mandatory__c,
			SandDV1__OnBoarding_Document__c.SObjectType.fields.SandDV1__System_Custom_Setting_ID__c,SandDV1__OnBoarding_Document__c.SObjectType.fields.SandDV1__Description__c,
			SandDV1__OnBoarding_Document__c.SObjectType.fields.SandDV1__Status__c,SandDV1__OnBoarding_Document__c.SObjectType.fields.SandDV1__Type__c};            
        
        return onBoardingFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of Attachment object fields which are using while upsert of Attachment record.
						It is used in Scheme Service Class.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchAttachmentFields() {

		List<Schema.SObjectField> attachmentFieldsList = new List<Schema.SObjectField>{Attachment.SObjectType.fields.Body,
			Attachment.SObjectType.fields.Name};            
        
        return attachmentFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Sales_Order_Item_Benefit__c object fields which are using while insert of SandDV1__Sales_Order_Item_Benefit__c record.
						It is used in Scheme Service Class.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchSalesOrderItembenefitFields() {

		List<Schema.SObjectField> salesOrderItembenefitFieldsList = new List<Schema.SObjectField>{SandDV1__Sales_Order_Item_Benefit__c.SObjectType.fields.SandDV1__Sales_Order_Line_Item__c,
			SandDV1__Sales_Order_Item_Benefit__c.SObjectType.fields.SandDV1__Scheme_Benefit__c,SandDV1__Sales_Order_Item_Benefit__c.SObjectType.fields.SandDV1__SchemeId__c};            
        
        return salesOrderItembenefitFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Sales_Order__c object fields which are using while upsert of Sales order record.
						It is used in Sales order Service.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchSalesOrderFields() {

		List<Schema.SObjectField> salesOrderFieldsList = new List<Schema.SObjectField>{
			SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Address_Name__c,
			SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Street__c,
			SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_City__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Country__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_State_Province__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Shipping_Zip_Postal_Code__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Buyer__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Date__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Discount__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__No_of_Parts__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__PO_Reference__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__PO_Status__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Price_Book__c,
            SandDV1__Sales_Order__c.SObjectType.fields.SandDV1__Remarks__c};

        return salesOrderFieldsList;
	}


	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Sales_Order_Line_Item__c object fields which are using while upsert of Sales order line item record.
						It is used in Sales order Service.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchSalesOrderLineItemFields() {

		List<Schema.SObjectField> salesOLIFieldsList = new List<Schema.SObjectField>{
			SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Part__c,
			SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Price_Book__c,
			SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Quantity__c,
            SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Net_Price__c,
            SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Sales_Price__c,
            SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Unit_Price__c,
            SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__UOM__c,
            SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Discount__c,
            SandDV1__Sales_Order_Line_Item__c.SObjectType.fields.SandDV1__Type__c};
            
        return salesOLIFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Target__c object fields which are using while upsert of Target record.
						It is used in Targets Service.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchTargetFields() {

		List<Schema.SObjectField> targetFieldsList = new List<Schema.SObjectField>{
			SandDV1__Target__c.SObjectType.fields.SandDV1__Aspect__c,
			SandDV1__Target__c.SObjectType.fields.SandDV1__Assigned_To__c,
			SandDV1__Target__c.SObjectType.fields.SandDV1__Period__c,
            SandDV1__Target__c.SObjectType.fields.SandDV1__Reason_For_Revision__c,
            SandDV1__Target__c.SObjectType.fields.SandDV1__Parent__c,
            SandDV1__Target__c.SObjectType.fields.SandDV1__Status__c,
            SandDV1__Target__c.SObjectType.fields.SandDV1__Value__c};
            
        return targetFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Target_Line_Item__c object fields which are using while upsert of Target line item record.
						It is used in Targets Service.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchTargetLineItemFields() {

		List<Schema.SObjectField> targetsOLIFieldsList = new List<Schema.SObjectField>{
			SandDV1__Target_Line_Item__c.SObjectType.fields.SandDV1__Aspect__c,
			SandDV1__Target_Line_Item__c.SObjectType.fields.SandDV1__Period__c,
            SandDV1__Target_Line_Item__c.SObjectType.fields.SandDV1__Value__c};
            
        return targetsOLIFieldsList;
	}

	/*******************************************************************************************************
	* @description :- Method to return list of SandDV1__Visit__c object fields which are using while upsert of Visit record.
						It is used in Customer 360 view Service.
	* @param :- null
	* @return :- List<Schema.SObjectField>.
	*/
	public static List<Schema.SObjectField> fetchVisitFields() {

		List<Schema.SObjectField> visitFieldsList = new List<Schema.SObjectField>{
			SandDV1__Visit__c.SObjectType.fields.SandDV1__Account__c,
			SandDV1__Visit__c.SObjectType.fields.SandDV1__Checkin_DateTime__c,
			SandDV1__Visit__c.SObjectType.fields.Checkin_Location__Latitude__s,
			SandDV1__Visit__c.SObjectType.fields.Checkin_Location__Longitude__s,
            SandDV1__Visit__c.SObjectType.fields.SandDV1__Checkout_DateTime__c,
            SandDV1__Visit__c.SObjectType.fields.Checkout_Location__Latitude__s,
            SandDV1__Visit__c.SObjectType.fields.Checkout_Location__Longitude__s,
            SandDV1__Visit__c.SObjectType.fields.SandDV1__Status__c,
            SandDV1__Visit__c.SObjectType.fields.SandDV1__Visit_Plan_Date__c,
            SandDV1__Visit__c.SObjectType.fields.SandDV1__Type__c};
            
        return visitFieldsList;
	}


}