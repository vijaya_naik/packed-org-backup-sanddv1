public with sharing class SalesOrderCustomMobileController {


	private ApexPages.StandardController con; 
	public SalesOrderCustomMobileController(ApexPages.StandardController stdController) { 
		con = stdController; 
	} 
	public PageReference saveNew() {
		 con.save(); 
		 Schema.DescribeSObjectResult describeResult = con.getRecord().getSObjectType().getDescribe(); 
		 PageReference pr = new PageReference('/' + describeResult.getKeyPrefix() + '/e'); 
		 pr.setRedirect(true); return pr; 
	}
}