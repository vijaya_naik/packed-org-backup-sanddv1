/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
@isTest(seeAllData = false)
private class OnBoardingDocumentTest {
static UserRole adminRole;
static User adminUser;
static Account accountObj;
static SandDV1__OnBoarding_Document__c docObj;
static SandDV1__OnBoarding_Document__c docObj1;
static SandDV1__OnBoardingStatusColor__c colorSetting;
static SandDV1__OnBoardingStatusColor__c colorSetting1;
static SandDV1__OnBoardingDocumentSetting__c docObjSetting ;
static SandDV1__OnBoardingDocumentSetting__c docObjSetting1 ;
static Attachment attachObj;
static Attachment attachObj1;

      static void init() {
        adminRole = Initial_Test_Data.createRole('Parent role', null);
        System.assertequals(adminRole.name,'Parent role');
        insert adminRole;

        adminUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',adminRole.Id);
        insert adminUser;
        System.runAs(adminUser) {
        //create dummy Account
        accountobj = InitializeTest.createAccount();
        System.assertEquals(accountobj.Name,'testaccount');
        insert accountobj;
        colorSetting = InitializeTest.createOnBoardingStatusColor('Draft','#000000');
        insert colorSetting;
        colorSetting1 = InitializeTest.createOnBoardingStatusColor('Approved','#006400');
        insert colorSetting1;
        docObjSetting  = InitializeTest.createOnBoardingDocumentSetting('jpg',true,'License Docs','License');
        insert docObjSetting  ;
        docObjSetting1 = InitializeTest.createOnBoardingDocumentSetting('pdf',true,'Certificates Docs','Education Files');
        insert docObjSetting1 ;
        docObj = InitializeTest.createOnBoardingDocument(accountobj.Id,'Draft',docObjSetting.Id);
        insert docObj ;
        attachObj = InitializeTest.createAttachment(docObj.Id,'dummy test attach');
        insert attachObj ;
        attachObj1 = InitializeTest.createAttachment(docObj.Id,'dummy test attach11');
        insert attachObj1 ;
        System.assertEquals(attachObj1.ParentId,docObj.Id);
        }
      }
      
      @isTest static void OnBoardingControllerTest() {

            init();
            System.runAs(adminUser) {
            OnBoardingController boradingDocController = new OnBoardingController();
            OnBoardingController.OnBoardingDraftStatus = 'Draft';
            System.assertEquals(OnBoardingController.OnBoardingDraftStatus,'Draft');
            OnBoardingController.OnBoardingPendingApprovalStatus = 'Pending Approval';
            OnBoardingController.OnBoardingDraftStatusColor = '#000000';
            OnBoardingController.OnBoardingPendingApprovalStatusColor = '#006400';
            OnBoardingController.getOnBoardingDocuments(accountobj.Id,'0');
            SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = false;
            try {
                OnBoardingController.doUploadAttachment(docObj,'Test Doc','Dummy Test File');
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to insert field') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
            
            OnBoardingController.deleteAttachment(attachObj);
            OnBoardingController.fetchAttachments(docObj);
            OnBoardingController.submitForApproval(docObj.Id);
            }
      }
      
       @isTest static void OnBoardingAccountStatus() {
           init();
           System.runAs(adminUser) {
           docObj1 = InitializeTest.createOnBoardingDocument(accountobj.Id,'Draft',docObjSetting1.Id);
           insert docObj1 ;
           docObj.SandDV1__Status__c = 'Approved';
           update docObj;
           System.assertEquals(docObj.SandDV1__Status__c,'Approved');
           docObj1.SandDV1__Status__c = 'Approved';
           update docObj1;
           accountobj.SandDV1__Status__c = 'Active';
           update accountobj;
           try {
               delete docObj1 ;
           }
           catch(Exception e) {
                System.debug('e......'+e);
           }
           }
       }
}