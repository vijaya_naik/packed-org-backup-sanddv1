/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 6/4/2015
* @description Trigger helper class for the trigger on Target object
*/
  
public class TargetTriggerHelper {
    
   
    /*******************************************************************************************************
    * @description : Method to give read access to the user selected in the Assigned To field. 
    * @param : List<SandDV1__Target__c> newTargets, Map<Id, SandDV1__Target__c> oldTargets
    * @return : List<SandDV1__Target__c>, Map<Id, SandDV1__Target__c>
    */
    
    public static void AssignTargetToAssignee(List<SandDV1__Target__c> newTargets, Map<Id, SandDV1__Target__c> oldTargets) {
        
        try {
            //List of sharing table to be created
            List<Target__Share> targetShareList = new List<Target__Share>();
            //List of sharing table to be deleted
            List<Target__Share> targetShareListDeleted = new List<Target__Share>();
            //Map of Ids of old Users to New Users
            Map<Id, Id> targetIDMap = new Map<Id,Id>();
            
            //Loop through the newTargets
            for(SandDV1__Target__c trg : newTargets) {
                //Check for the status 
                if(trg.SandDV1__Status__c != null) {
                    //Check for old value should be Draft and new should be recieved
                    if(((oldTargets == null && trg.SandDV1__Status__c == Label.Assigned) || 
                            (oldTargets != null && oldTargets.get(trg.Id).SandDV1__Status__c == Label.Draft && 
                            trg.SandDV1__Status__c == Label.Assigned)) && trg.SandDV1__Assigned_To__c != null) {
                        //Create new instance of sharing Object
                        Target__Share trgtShare = new Target__Share();
                        trgtShare.ParentId = trg.Id;
                        trgtShare.UserOrGroupId = trg.SandDV1__Assigned_To__c;
                        trgtShare.AccessLevel = Label.Edit;
                        trgtShare.RowCause = Schema.Target__Share.RowCause.SandDV1__Target_Assignment_Access__c;

                        //Add to list
                        targetShareList.add(trgtShare);
                    }
                    else if(trg.SandDV1__Status__c != Label.Draft && oldTargets.get(trg.Id).SandDV1__Assigned_To__c != trg.SandDV1__Assigned_To__c) {
                        targetIDMap.put(trg.Id,trg.SandDV1__Assigned_To__c);
                    }
                }
            }

            if(targetIDMap.size() > 0) {
                for(Target__Share shareRcrdToBeDeleted : [SELECT ParentId, AccessLevel, UserOrGroupId FROM 
                                                    Target__Share WHERE ParentId IN: targetIDMap.keySet()]) {
                    Target__Share trgtShare = new Target__Share();
                    trgtShare.ParentId = shareRcrdToBeDeleted.ParentId;
                    trgtShare.UserOrGroupId = targetIDMap.get(shareRcrdToBeDeleted.ParentId);
                    trgtShare.AccessLevel = Label.Read;              
                    targetShareList.add(trgtShare); 
                    targetShareListDeleted.add(shareRcrdToBeDeleted);
                    //System.debug('##3Inside update'); 
                }
            }

            //Check for the size and upsert the target share
            Database.SaveResult[] targetShareInsertResult = Database.insert(targetShareList,false);
            Database.DeleteResult[] targetShareDeletResult = Database.delete(targetShareListDeleted,false);
        } catch(Exception e) {
            System.debug('ERROR IS HERE::::' + String.valueOf(e));
        }
    }

    /*******************************************************************************************************
    * @description :- Method is used to fetch unique identifier.
    * @param :- List<SandDV1__Target__c>.
    * @return :- void. 
    */
    public static void taregetUniqueIdentifier(List<SandDV1__Target__c> newtargetlist) {
        for(SandDV1__Target__c target : newtargetlist) {
            target.SandDV1__System_Unique_Target_Indentifier__c = target.SandDV1__Period__c+'-'+target.OwnerID+'-'+
                 target.SandDV1__Assigned_To__c+'-'+target.SandDV1__Aspect__c;
        }
    }

    /*******************************************************************************************************
    * @description :- Method is get unique identifier on before update of target only if some fields are changed
                        such as period,ownerid,assigned to and aspect (any field from these fields).
    * @param <param name> :- List<SandDV1__Target__c>,Map<Id, SandDV1__Target__c>.
    * @return <return type> :- void. 
    */
    public static void onBeforeUpdate(List<SandDV1__Target__c> newtargetlist,Map<Id, SandDV1__Target__c> oldTargets) {
        List<SandDV1__Target__c> targetList = new List<SandDV1__Target__c>();
        for(SandDV1__Target__c target :newtargetlist) {
            if(target.SandDV1__Period__c != oldTargets.get(target.Id).SandDV1__Period__c || target.OwnerID != oldTargets.get(target.Id).OwnerID ||
                target.SandDV1__Assigned_To__c != oldTargets.get(target.Id).SandDV1__Assigned_To__c  || target.SandDV1__Aspect__c != oldTargets.get(target.Id).SandDV1__Aspect__c) {
                    targetList.add(target);
            }
        }
        if(!targetList.isempty()) {
            taregetuniqueidentifier(targetList);
        }
    }
}