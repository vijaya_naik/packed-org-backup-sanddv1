//Helper Class to achieve Hierarchichal Custom Settings Like
//functionality with Custom Metadata Types
public with sharing class HierarchicalCustomMetadata {
	
	public static Boolean fetchToggleValue(String moduleName) {
		/*A dynamic query gets generated here 
		and will fetch value based on module name and user/profile level
		from the Toggle_Hierarchical Custom Metadata Type.
		If no data exists the function returns the Org Level value for the
		same module/funnctionality*/
		//Currently just a mock will return true for Admin, false otherwise
		Profile profileId = [select Name,Id from profile where Name = :System.Label.System_Administrator limit 1];
		if(UserInfo.getProfileId() == profileId.Id) {
			return true;
		}
		else {
			return false;
		}
	}
}