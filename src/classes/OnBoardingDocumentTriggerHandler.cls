/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 12-11-2015
* @description This is a Trigger on SandDV1__OnBoarding_Document__c.
*/
public with sharing class OnBoardingDocumentTriggerHandler {
    public OnBoardingDocumentTriggerHandler() {
        
    }
    private static OnBoardingDocumentTriggerHandler instance;

    
    public static OnBoardingDocumentTriggerHandler getInstance() {
        if (instance == null) {
            instance = new OnBoardingDocumentTriggerHandler();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description :- If SandDV1__OnBoarding_Document__c status is approved then don't allow deletion.
    * @param :- List<SandDV1__OnBoarding_Document__c> oldObjects
    * @return :- void.
    */
    public void onBeforeDelete(List<SandDV1__OnBoarding_Document__c> oldObjects) {
        for(SandDV1__OnBoarding_Document__c doc : oldObjects) {
            if(doc.SandDV1__Status__c == ConfigurationService.OnBoardingApprovedStatus) {
                doc.adderror(+system.Label.Can_tDeleteRecordMessage);
            }
        }
    }

}