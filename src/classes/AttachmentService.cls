/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class AttachmentService{
    public AttachmentService() {
        
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Attachment Records
    * @param :- Id.
    * @return :- list<Attachment>.
    */
    @RemoteAction
    public static list<Attachment> fetchAttachment (ID parentid) {
        return [Select Id,Name,ParentId,LastModifiedDate,CreatedById,CreatedBy.Name from Attachment where ParentId =:parentid];
    }
}