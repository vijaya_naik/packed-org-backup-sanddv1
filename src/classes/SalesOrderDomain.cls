/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a Domain class for Salesorder
*/

public with sharing class SalesOrderDomain {
  
  /*******************************************************************************************************
    * @description :Wrapper class to store SandDV1__Credit_Limit__c and SandDV1__Block_Order_Creation__c
    * @param 
    * @return 
    */
    public class CreditBlockOrderWrapper {

        public Double creditLimit {get; set;}
        public Boolean blockOrderCreation {get; set;}

        public creditBlockOrderWrapper(){

            this.creditLimit = 0;
            this.blockOrderCreation = false;
        }
    }
    

    /*******************************************************************************************************
    * @description :Wrapper class to store SandDV1__Sales_Order__c and List of OrderLineItemWrapper
    * @param 
    * @return 
    */
    public class SalesOrderWrapper { 
        
        public SandDV1__Sales_Order__c salesOrder = new SandDV1__Sales_Order__c();
        public List<OrderLineItemWrapper> orderLineItemWrapList = new List<OrderLineItemWrapper>();
    }


    /*******************************************************************************************************
    * @description :Wrapper class to store SandDV1__Sales_Order_Line_Item__c and List of SandDV1__Sales_Order_Line_Item__c
    * @param 
    * @return 
    */
    public class OrderLineItemWrapper {
        public SandDV1__Sales_Order_Line_Item__c salesOrderLineItem = new SandDV1__Sales_Order_Line_Item__c();
        public List<SandDV1__Sales_Order_Line_Item__c> salesBenefitLineItemList = new List<SandDV1__Sales_Order_Line_Item__c>();
    }
}