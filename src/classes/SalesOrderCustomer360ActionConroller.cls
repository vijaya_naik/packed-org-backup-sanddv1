/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 08-Dec-2015
* @description This is a Controller class for SalesOrderCustomer360ActionPage
*/
public with sharing class SalesOrderCustomer360ActionConroller {
	public String accountId {get;set;}

	public SalesOrderCustomer360ActionConroller(ApexPages.StandardController stdController) 
    { 

        String sId = String.valueOf(stdController.getId());  

        if(sId != null){

            SandDV1__Sales_Order__c so = [SELECT Id,SandDV1__Buyer__c FROM SandDV1__Sales_Order__c WHERE Id =:sId];
            accountId = so.SandDV1__Buyer__c;
            
        } 
    }
}