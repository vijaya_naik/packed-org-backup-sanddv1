/*************************************************************************
* 
* Et MARLABS CONFIDENTIAL
* __________________
* 
*  [2011] - [2016] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author ET MARLABS
* @date April 2016
* @description Post Installation Script to run after Installation of the Package.
*/
global class PostInstallationClass implements InstallHandler  {
    
  global void onInstall(InstallContext context) {
      //Inserting Custom Setting Data.
      Visit_Plan_Default_Target__c vp = new Visit_Plan_Default_Target__c(Name='Default', Number_Of_Visits__c=5);
      insert vp;
  }
}