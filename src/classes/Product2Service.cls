/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 27-Nov-2015
* @description This is a product2 Service
*/
public with sharing class Product2Service {
  

      /*******************************************************************************************************
    * @description : method to retrive product2
    * @param : priceBookName - priceBookName name to query
    * @return List of Pricebook2. 
    */
    public static List<Product2> fetchProductList(String prodName){
        String startsWith = prodName + '%';
        return [Select Id,Name,IsActive from Product2 where Name LIKE :startsWith AND IsActive =true Order By Name];
    }
}