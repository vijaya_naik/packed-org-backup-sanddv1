/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 24-Nov-2015
* @description This is a service class for TargetsController
*/
public with sharing class TargetsService {
  

  /*******************************************************************************************************
  * @description .Method to fetch Targets
  * @param <param name> 
  * @return <return type> .List of targets
  */
  
  public static List<SandDV1__Target__c> fetchTargets(){

    return [SELECT Id, Name, Owner.Name,SandDV1__Status__c,SandDV1__Period__r.SandDV1__Periodicity__c,SandDV1__Period__r.SandDV1__System_Start_and_End_Date__c,
    SandDV1__Active__c,Owner.Id, SandDV1__Period__r.Name,SandDV1__Period__r.SandDV1__Start_Date__c,SandDV1__Period__r.SandDV1__End_Date__c,SandDV1__Assigned_To__r.Name,SandDV1__Aspect__c FROM SandDV1__Target__c 
    WHERE SandDV1__Assigned_To__c =: userinfo.getUserId() OR CreatedById =:userinfo.getUserId()];
  }


  /*******************************************************************************************************
    * @description Method to delete a target
    * @param targetId- Target Id
    * @return null 
    */
    
    public static void deleteTarget(String targetId) {

        try{

            SecurityUtils.checkObjectIsDeletable(SandDV1__Target__c.SObjectType);
            SandDV1__Target__c tar = [SELECT Id FROM SandDV1__Target__c WHERE Id=:targetId];
            delete tar;
        }
        catch(Exception e) {

            throw new CustomException(e.getMessage());
        }
    }


    /*******************************************************************************************************
    * @description Method to check status values from constants class and assign labels
    * @param targetInstance- Target
    * @return null 
    */
    public static String fetchStatusLabels(SandDV1__Target__c targetInstance){
         
         String returnmessage = '';

         if(ConfigurationService.getTargetStatusValues().get('isAccepted') == targetInstance.SandDV1__Status__c 
          && targetInstance.SandDV1__Parent__c == null){
          returnmessage = Label.Target_Accepted_Message_Without_Parent;
         } 
         else if(ConfigurationService.getTargetStatusValues().get('isAccepted') == targetInstance.SandDV1__Status__c 
          && targetInstance.SandDV1__Parent__c != null){
          returnmessage = Label.Target_Accepted_Message_With_Parent;
         } 
         else if(ConfigurationService.getTargetStatusValues().get('isDraft') == targetInstance.SandDV1__Status__c 
          && targetInstance.SandDV1__Parent__c == null){
          returnmessage = Label.Target_Draft_Message_Without_Parent;
         } 
         else if(ConfigurationService.getTargetStatusValues().get('isRevised') == targetInstance.SandDV1__Status__c 
          && targetInstance.SandDV1__Parent__c != null){
          returnmessage = Label.Target_Revised_Message_With_Parent;
         }

         return returnmessage;
    }

    /*******************************************************************************************************
    * @description Method to fetch Subordinates of the current user.
    * @param 
    * @return Map of user 
    */
    public static Map<Id,User> fetchSubordinates(){
        Map<Id,User> userMap = new Map<Id,User>();
        set<Id> setofChildID                         = new Set<ID>();
        set<Id> userRoleID                           = new Set<ID>();  
        List<UserRole> subordinatesRoleList          = new List<UserRole>();

        List<User> currentUserList = [SELECT id,Name,UserRoleId,isActive FROM User WHERE id=:userinfo.getUserId() 
                                                      and isActive=:True limit 1];
        for(User userobj : currentUserList){
            if(userobj.UserRoleId!=null) {
               userRoleID.add(userobj.UserRoleId);
            }
        }
        subordinatesRoleList =[SELECT Id,ParentRoleId,Name FROM UserRole WHERE ParentRoleId IN:userRoleID];
 
        for(UserRole uroleObj:subordinatesRoleList)
        {
            setofChildID.add(uroleObj.ID);
        }
        for(User u :[SELECT Id,Name,ManagerId,UserRoleId,isActive FROM User WHERE UserRoleId IN:setofChildID]){
            userMap.put(u.Id,u);
        }

        return userMap;
    }


    /*******************************************************************************************************
    * @description Method to fetch targets and target line item of parent and child
    * @param 
    * @return TargetInfoWrapper 
    */

    public static TargetsDomain.TargetInfoWrapper fetchTargetData(String masterTargetId,String periodId) {
        
        TargetsDomain.TargetInfoWrapper wraptargetinst = new TargetsDomain.TargetInfoWrapper();
 
        set<Id> parentIDSet = new Set<ID>();   
        Map<Id,String> parentNameMap = new Map<Id,String>();   
        Map<Id,SandDV1__Target__c> existingTargetMap = new Map<Id,SandDV1__Target__c>();
        Map<Id,User> userMap = new Map<Id,User>();
        Map<Id,SandDV1__Target__c> existingUserWithTargetMap = new Map<Id,SandDV1__Target__c>();
        Map<Id,List<TargetsDomain.PeriodTLIWrapper>> existingLineItemMap = new Map<Id,List<TargetsDomain.PeriodTLIWrapper>>();
        List<TargetsDomain.PeriodTLIWrapper> newtargetlineitemList = new List<TargetsDomain.PeriodTLIWrapper>();
        List<TargetsDomain.PeriodTLIWrapper>   childTargetList = new List<TargetsDomain.PeriodTLIWrapper>();
        List<TargetsDomain.TargetInfoWrapper>     userTargetList  = new List<TargetsDomain.TargetInfoWrapper>();
        String periodname = '';
        
        for(SandDV1__Target__c msttarget :[SELECT Id,Name,SandDV1__Aspect__c,SandDV1__Assigned_To__c,SandDV1__Period__c,SandDV1__Period__r.SandDV1__System_Start_and_End_Date__c,SandDV1__Period__r.SandDV1__Start_Date__c,
                                    SandDV1__Period__r.Name,SandDV1__Period__r.SandDV1__End_Date__c,SandDV1__Status__c,SandDV1__Parent__c, SandDV1__Value__c,SandDV1__Reason_For_Revision__c, Owner.Name, Owner.Id 
                                    FROM SandDV1__Target__c WHERE Id =:masterTargetId]){

            parentIDSet.add(msttarget.SandDV1__Period__c);
            periodname = msttarget.SandDV1__Period__r.Name;
            if(msttarget.SandDV1__Parent__c != null){
                  wraptargetinst.hasParent = true;
            }
            else{
                wraptargetinst.hasParent = false;
            }
            if(ConfigurationService.getTargetStatusValues().get('isDraft') != msttarget.SandDV1__Status__c){
                  wraptargetinst.isDraft = false;
            }
            if(ConfigurationService.getTargetStatusValues().get('isAssigned') == msttarget.SandDV1__Status__c){
                  wraptargetinst.isAssigned = true;
            }
            else if(ConfigurationService.getTargetStatusValues().get('isRevised') == msttarget.SandDV1__Status__c){
                  wraptargetinst.isRevised = true;
            }
            else if(ConfigurationService.getTargetStatusValues().get('isDraft') == msttarget.SandDV1__Status__c){
                  wraptargetinst.isDraft = true;
            }
            else if(ConfigurationService.getTargetStatusValues().get('isAccepted') == msttarget.SandDV1__Status__c){
                  wraptargetinst.isAccepted = true;
            }
            else if(ConfigurationService.getTargetStatusValues().get('isSuspended') == msttarget.SandDV1__Status__c){
                  wraptargetinst.isSuspended = true;
            }
            wraptargetinst.targetObj     = msttarget;
           
        }

        List<SandDV1__Target_Line_Item__c> targetLineItemList = [SELECT Id,SandDV1__Aspect__c,SandDV1__Period__r.SandDV1__Start_Date__c,SandDV1__Period__c,SandDV1__Period__r.SandDV1__System_Start_and_End_Date__c,SandDV1__Period__r.Name,SandDV1__Period__r.SandDV1__End_Date__c,SandDV1__Target__c,SandDV1__Value__c 
                           FROM SandDV1__Target_Line_Item__c WHERE SandDV1__Target__c =:masterTargetId];
     
        for(SandDV1__Target_Line_Item__c childtarget :targetLineItemList){
            parentIDSet.add(childtarget.SandDV1__Period__c);
            TargetsDomain.PeriodTLIWrapper pwrap = new TargetsDomain.PeriodTLIWrapper();
            pwrap.tliObj            = childtarget;
            pwrap.periodName        = childtarget.SandDV1__Period__r.Name;
            pwrap.periodEndDate     = String.valueOf(childtarget.SandDV1__Period__r.SandDV1__End_Date__c);
            pwrap.periodStartDate   = String.valueOf(childtarget.SandDV1__Period__r.SandDV1__Start_Date__c);
            
            childTargetList.add(pwrap);

            TargetsDomain.PeriodTLIWrapper newline = new TargetsDomain.PeriodTLIWrapper();
            newline.periodName            = childtarget.SandDV1__Period__r.Name;
            newline.periodEndDate         = String.valueOf(childtarget.SandDV1__Period__r.SandDV1__End_Date__c);
            newline.periodStartDate       = String.valueOf(childtarget.SandDV1__Period__r.SandDV1__Start_Date__c);
            newline.tliObj.SandDV1__Period__c      = childtarget.SandDV1__Period__c;
            newline.tliObj.SandDV1__Value__c       = 0.0;
            newtargetlineitemList.add(newline);
        }
       
        for(Period__C p :[SELECT ID,Name,SandDV1__Start_Date__c FROM SandDV1__Period__c WHERE Id In:parentIDSet]){
            parentNameMap.put(p.ID,p.Name);
        }
        wraptargetinst.periodTliWrapList = childTargetList;
        userMap = fetchSubordinates(); //Fetch Subordinates of the current user

        if(wraptargetinst.targetObj.SandDV1__Status__c == 'Accepted') {
            
            for(SandDV1__Target__c usertarget : [SELECT Id,Name,SandDV1__Aspect__c,SandDV1__Assigned_To__c,Owner.Name, Owner.Id,SandDV1__Period__c,SandDV1__Period__r.SandDV1__System_Start_and_End_Date__c,SandDV1__Period__r.Name,SandDV1__Period__r.SandDV1__End_Date__c,SandDV1__Period__r.SandDV1__Start_Date__c,SandDV1__Status__c,SandDV1__Parent__c,
                SandDV1__Value__c,SandDV1__Reason_For_Revision__c FROM SandDV1__Target__c WHERE SandDV1__Parent__c =:masterTargetId AND SandDV1__Assigned_To__c IN:userMap.keyset()]) {
                existingUserWithTargetMap.put(usertarget.SandDV1__Assigned_To__c,usertarget);
                existingTargetMap.put(usertarget.id,usertarget);
            }
            for(SandDV1__Target_Line_Item__c usertargetlineitem :[SELECT Id,SandDV1__Aspect__c,SandDV1__Period__c,SandDV1__Period__r.SandDV1__System_Start_and_End_Date__c,SandDV1__Period__r.SandDV1__Start_Date__c,SandDV1__Period__r.SandDV1__End_Date__c,SandDV1__Target__c,SandDV1__Value__c 
                         FROM SandDV1__Target_Line_Item__c WHERE SandDV1__Target__c In:existingTargetMap.keyset()]){
                if(existingLineItemMap.containskey(existingTargetMap.get(usertargetlineitem.SandDV1__Target__c).SandDV1__Assigned_To__c)){
                    TargetsDomain.PeriodTLIWrapper newperiod = new TargetsDomain.PeriodTLIWrapper();
                    newperiod.tliObj = usertargetlineitem;
                    newperiod.periodName = parentNameMap.get(usertargetlineitem.SandDV1__Period__c);
                    newperiod.periodEndDate = String.valueOf(usertargetlineitem.SandDV1__Period__r.SandDV1__End_Date__c);
                    newperiod.periodStartDate = String.valueOf(usertargetlineitem.SandDV1__Period__r.SandDV1__Start_Date__c);
                    existingLineItemMap.get(existingTargetMap.get(usertargetlineitem.SandDV1__Target__c).SandDV1__Assigned_To__c).add(newperiod);
                }
                else{
                    List<TargetsDomain.PeriodTLIWrapper> periodtargetlineList = new List<TargetsDomain.PeriodTLIWrapper>();
                    TargetsDomain.PeriodTLIWrapper newperiod = new TargetsDomain.PeriodTLIWrapper();
                    newperiod.tliObj = usertargetlineitem;
                    newperiod.periodName = parentNameMap.get(usertargetlineitem.SandDV1__Period__c);
                    newperiod.periodEndDate = String.valueOf(usertargetlineitem.SandDV1__Period__r.SandDV1__End_Date__c);
                    newperiod.periodStartDate = String.valueOf(usertargetlineitem.SandDV1__Period__r.SandDV1__Start_Date__c);
                    periodtargetlineList.add(newperiod);
                    existingLineItemMap.put(existingTargetMap.get(usertargetlineitem.SandDV1__Target__c).SandDV1__Assigned_To__c,periodtargetlineList);
                }
            }
        }
        for(Id uid :userMap.keyset()) {
            wraptargetinst.hasSubordinates = true;
            //if(wraptargetinst.targetObj.SandDV1__Status__c == 'Accepted') {
            if(wraptargetinst.targetObj.SandDV1__Status__c == ConfigurationService.getTargetStatusValues().get('isAccepted')) {
                TargetsDomain.TargetInfoWrapper childtarget = new TargetsDomain.TargetInfoWrapper();
                if(existingUserWithTargetMap.get(uid) != null){
                    childtarget.targetObj = existingUserWithTargetMap.get(uid);
                    childtarget.userInstance       = userMap.get(uid);
                    childtarget.periodTliWrapList  = existingLineItemMap.get(uid);    
                    if(ConfigurationService.getTargetStatusValues().get('isDraft') != existingUserWithTargetMap.get(uid).SandDV1__Status__c){
                          childtarget.isDraft = false;
                    }
                    if(ConfigurationService.getTargetStatusValues().get('isAssigned') == existingUserWithTargetMap.get(uid).SandDV1__Status__c){
                          childtarget.isAssigned = true;
                    }
                    else if(ConfigurationService.getTargetStatusValues().get('isRevised') == existingUserWithTargetMap.get(uid).SandDV1__Status__c){
                          childtarget.isRevised = true;
                    }
                    else if(ConfigurationService.getTargetStatusValues().get('isDraft') == existingUserWithTargetMap.get(uid).SandDV1__Status__c){
                          childtarget.isDraft = true;
                    }
                    else if(ConfigurationService.getTargetStatusValues().get('isAccepted') == existingUserWithTargetMap.get(uid).SandDV1__Status__c){
                          childtarget.isAccepted = true;
                    }
                    else if(ConfigurationService.getTargetStatusValues().get('isSuspended') == existingUserWithTargetMap.get(uid).SandDV1__Status__c){
                          childtarget.isSuspended = true;
                    }

                    userTargetList.add(childtarget);
                }
                else {
                      childtarget.targetObj.SandDV1__Parent__c         = wraptargetinst.targetObj.Id;
                      childtarget.targetObj.SandDV1__Aspect__c         = wraptargetinst.targetObj.SandDV1__Aspect__c;
                      childtarget.targetObj.SandDV1__Assigned_To__c    = uid;
                      childtarget.targetObj.SandDV1__Reason_For_Revision__c  = '';
                      childtarget.targetObj.SandDV1__Period__c         = wraptargetinst.targetObj.SandDV1__Period__c;
                      childtarget.targetObj.SandDV1__Value__c          = 0;
                      childtarget.userInstance                         = userMap.get(uid);
                      childtarget.periodTliWrapList                    = newtargetlineitemList; 
                      childtarget.isDraft                              = true;    
                      userTargetList.add(childtarget);
                } 
            }
        }
        wraptargetinst.childTargetsWrapperList = userTargetList;
        
        return wraptargetinst;
    }


     /*******************************************************************************************************
    * @description Method to fetch targets and target line item of parent and child
    * @param 
    * @return TargetInfoWrapper 
    */

    public static TargetsDomain.TargetInfoWrapper createEmptyTargetInfo(List<SandDV1__Period__c> periodList,Map<Id,SandDV1__Period__c> periodMap,String periodId) {

      TargetsDomain.TargetInfoWrapper wrapTarget   = new TargetsDomain.TargetInfoWrapper();
      List<TargetsDomain.PeriodTLIWrapper> periodTliWrapList = new List<TargetsDomain.PeriodTLIWrapper>();
      List<TargetsDomain.TargetInfoWrapper> usertargetlist = new List<TargetsDomain.TargetInfoWrapper>();
      Map<Id,User> userMap = new Map<Id,User> (); 
      SandDV1__Target__c target = new SandDV1__Target__c();


      for(SandDV1__Period__c period:periodList){
            TargetsDomain.PeriodTLIWrapper periodTliWrap  = new TargetsDomain.PeriodTLIWrapper();
            SandDV1__Target_Line_Item__c targetLineItem = new SandDV1__Target_Line_Item__c(SandDV1__Period__c = period.Id,
                                                      SandDV1__Value__c = 0
            );
            periodTliWrap.periodName             = period.Name;
            periodTliWrap.periodEndDate          = String.valueOf(period.SandDV1__End_Date__c);
            periodTliWrap.periodStartDate        = String.valueOf(period.SandDV1__Start_Date__c);
            periodTliWrap.tliObj                 = targetLineItem;
            periodTliWrapList.add(periodTliWrap);
        }
       
        userMap = fetchSubordinates(); //Fetch Subordinates of the current user

        if(!userMap.isempty()){
            wrapTarget.hasSubordinates              = true;
        }
        
        target.SandDV1__Period__c                            = periodMap.get(periodId).ID;
        target.SandDV1__Value__c                             = 0;
        target.SandDV1__Aspect__c                            = '';
        target.SandDV1__Status__c                            = '';
        target.SandDV1__Assigned_To__c                       = userinfo.getUserId();
        target.SandDV1__Reason_For_Revision__c               = '';
        wrapTarget.targetObj                        = target;
        wrapTarget.periodTliWrapList                = periodTliWrapList;
        wrapTarget.childTargetsWrapperList          = usertargetlist;
        wrapTarget.hasParent                        = false;

        return wrapTarget;
    }



    /*******************************************************************************************************
    * @description :Method to save target
    * @param :targetInstance - Target Obj, isDraft isAssigned isSuspended of boolean
    * @return :TargetMessageInfo
    */

    public static TargetsDomain.TargetMessageInfo saveTarget(SandDV1__Target__c targetInstance, List<TargetsDomain.PeriodTLIWrapper> periodTLIWrapperList, 
        Boolean isDraft, Boolean isAssigned, Boolean isRevised, Boolean isAccepted) {

        Savepoint sp = Database.setSavepoint();
        SandDV1__Target__c lTargetInstance;
        String idToReturn;
        Boolean nothingChanged = true;
        try{
          
          TargetsDomain.TargetMessageInfo messageinfo = new TargetsDomain.TargetMessageInfo();
          
          if(targetInstance != null && (isAssigned || isDraft || isAccepted)) { 
              lTargetInstance = new SandDV1__Target__c();
              lTargetInstance = targetInstance;
              if(isAssigned){
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isAssigned');
              }
              else if(isDraft){
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isDraft');
              }
              else if(isAccepted) {
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isAccepted');
              }
              
              List<Schema.SObjectField> targetFieldsList = FieldAccessibilityUtility.fetchTargetFields();
              SecurityUtils.checkInsert(SandDV1__Target__c.SObjectType,targetFieldsList);
              SecurityUtils.checkUpdate(SandDV1__Target__c.SObjectType,targetFieldsList);
              upsert lTargetInstance;


              if(lTargetInstance.Id != null){
                  if(periodTLIWrapperList != null && !periodTLIWrapperList.isEmpty()) {
                      List<SandDV1__Target_Line_Item__c> tLIList = new List<SandDV1__Target_Line_Item__c>();
                      for(TargetsDomain.PeriodTLIWrapper pTLI : periodTLIWrapperList) {

                          if(pTLI.tliObj != null){
                              if(pTLI.tliObj.SandDV1__Target__c == null){
                                pTLI.tliObj.SandDV1__Target__c = lTargetInstance.ID;
                              }
                              tLIList.add(pTLI.tliObj);
                          }
                      }
                      if(!tLIList.isempty()){

                          List<Schema.SObjectField> targetsOLIFieldsList = FieldAccessibilityUtility.fetchTargetLineItemFields();
                          SecurityUtils.checkInsert(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
                          SecurityUtils.checkUpdate(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
                          upsert tLIList; 
                      }
                  }
              }
              if(targetInstance.SandDV1__Assigned_To__c != userinfo.getUserId() && lTargetInstance.SandDV1__Parent__c != null) {
                  idToReturn =  lTargetInstance.SandDV1__Parent__c;
              }
              else{
                  idToReturn =  lTargetInstance.Id;
              }
                  
          }
          else if(targetInstance.Id != null && targetInstance != null && (isAccepted || isRevised)) {
              lTargetInstance = new SandDV1__Target__c();
              lTargetInstance = targetInstance;
              if(isAccepted){
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isAccepted');
              }
              else if(isRevised){
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isRevised');
              }
              
              List<Schema.SObjectField> targetFieldsList = FieldAccessibilityUtility.fetchTargetFields();
              SecurityUtils.checkInsert(SandDV1__Target__c.SObjectType,targetFieldsList);
              SecurityUtils.checkUpdate(SandDV1__Target__c.SObjectType,targetFieldsList);
              upsert lTargetInstance;

              if(lTargetInstance.Id != null){
                  if(periodTLIWrapperList != null && !periodTLIWrapperList.isEmpty()) {
                        List<SandDV1__Target_Line_Item__c> tLIList = new List<SandDV1__Target_Line_Item__c>();
                        for(TargetsDomain.PeriodTLIWrapper pTLI : periodTLIWrapperList) {

                          if(pTLI.tliObj != null){
                          if(pTLI.tliObj.SandDV1__Target__c == null){
                            pTLI.tliObj.SandDV1__Target__c = lTargetInstance.ID;
                          }
                            tLIList.add(pTLI.tliObj);
                        }
                      }
                      if(!tLIList.isempty()){

                          List<Schema.SObjectField> targetsOLIFieldsList = FieldAccessibilityUtility.fetchTargetLineItemFields();
                          SecurityUtils.checkInsert(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
                          SecurityUtils.checkUpdate(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
                          upsert tLIList; 
                      }
                      
                  }
              }
              idToReturn = lTargetInstance.Id;
          }

          messageinfo.targetID =  idToReturn;
          messageinfo.targetReturnMessage = fetchStatusLabels(lTargetInstance);
          return messageinfo;
        }
        catch(Exception e) {

            Database.rollback(sp);
            String errorMessage = e.getMessage();
            if(errorMessage.contains('SandDV1__System_Unique_Target_Indentifier__c')) {
              errorMessage = Label.Target_exists;
            }
            throw new CustomException(errorMessage);
        }

    }

    /*******************************************************************************************************
    * @description Method to save all targets
    * @param 
    * @return TargetMessageInfo 
    */

    public static TargetsDomain.TargetMessageInfo saveAllTargets(List<TargetsDomain.TargetInfoWrapper> childTargetsWrapperList, 
        Boolean isDraft, Boolean isAssigned) {
        
        Savepoint sp = Database.setSavepoint();
        List<SandDV1__Target__c> masterTargetList = new List<SandDV1__Target__c>();
        List<SandDV1__Target_Line_Item__c> lineItemList = new List<SandDV1__Target_Line_Item__c>();
        Map<Integer,List<TargetsDomain.PeriodTLIWrapper>> targetLineItemMap = new Map<Integer,List<TargetsDomain.PeriodTLIWrapper>>();
        Map<Integer,ID> targetMap = new Map<Integer,ID>();
        SandDV1__Target__c lTargetInstance;
        String idToReturn;
        TargetsDomain.TargetMessageInfo messageinfo = new TargetsDomain.TargetMessageInfo();
        Boolean nothingChanged = true;

        try{
          
          if(childTargetsWrapperList != null && !childTargetsWrapperList.isEmpty()) {
           
              for(integer i = 0; i<childTargetsWrapperList.size();i++){
                  if(childTargetsWrapperList[i].targetObj != null && (isAssigned || isDraft)) { 
                          idToReturn = childTargetsWrapperList[i].targetObj.SandDV1__Parent__c;
                          targetLineItemMap.put(i,childTargetsWrapperList[i].periodTliWrapList);
                          lTargetInstance = new SandDV1__Target__c();
                          lTargetInstance = childTargetsWrapperList[i].targetObj;
                          if(isAssigned){
                              lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isAssigned');
                          }
                          else if(isDraft){
                              lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isDraft');
                          }
                          
                          masterTargetList.add(lTargetInstance);
                  }
                                  
              }
          }

          List<Schema.SObjectField> targetFieldsList = FieldAccessibilityUtility.fetchTargetFields();
          SecurityUtils.checkInsert(SandDV1__Target__c.SObjectType,targetFieldsList);
          SecurityUtils.checkUpdate(SandDV1__Target__c.SObjectType,targetFieldsList);
          List<Database.upsertResult> upsertTargetResultList = Database.upsert(masterTargetList,true); 
          for (integer i=0; i<upsertTargetResultList.size();i++) {
                    targetMap.put(i,upsertTargetResultList[i].getID());
          }
          
          for(Integer n : targetLineItemMap.keyset()){
              for(TargetsDomain.PeriodTLIWrapper tl : targetLineItemMap.get(n)){
                  if(tl.tliObj != null){
                  SandDV1__Target_Line_Item__c newline = new SandDV1__Target_Line_Item__c();
                      newline  = tl.tliObj;
                      if(newline.SandDV1__Target__c == null){
                          newline.SandDV1__Target__c = targetMap.get(n);
                      }
                      lineItemList.add(newline);
                  }
              }
          }
          if(!lineItemList.isempty()){

            List<Schema.SObjectField> targetsOLIFieldsList = FieldAccessibilityUtility.fetchTargetLineItemFields();
            SecurityUtils.checkInsert(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
            SecurityUtils.checkUpdate(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
            upsert lineItemList;   
          }

          messageinfo.targetID =  idToReturn;
          if(isAssigned){
               messageinfo.targetReturnMessage = Label.All_Target_Assigned_Message;
          }
          else if(isDraft){
              messageinfo.targetReturnMessage = Label.All_Target_Save_Draft_Message;
          }

          return messageinfo;
        }
        catch(Exception e) {

            Database.rollback(sp);
            throw new CustomException(e.getMessage());
        }
           
    }


    /*******************************************************************************************************
    * @description :Method to save child target of a target.
    * @param :cTargetData of ChildTargetsWrapper type and isDraft isAssigned isSuspended of boolean
    * @return :ChildTargetsWrapper type 
    */
    
    public static TargetsDomain.TargetInfoWrapper saveChildTarget(TargetsDomain.TargetInfoWrapper cTargetData, 
        Boolean isDraft, Boolean isAssigned, Boolean isSuspended) { 
        
        Savepoint sp = Database.setSavepoint();
        SandDV1__Target__c lTargetInstance;
        TargetsDomain.TargetInfoWrapper childtarget = new TargetsDomain.TargetInfoWrapper(); 
        List<TargetsDomain.PeriodTLIWrapper>   childTargetList = new List<TargetsDomain.PeriodTLIWrapper>();
        Boolean nothingChanged = true;

        try{

          if(cTargetData.targetObj != null && (isAssigned || isDraft || isSuspended)) { 
              lTargetInstance = new SandDV1__Target__c();
              lTargetInstance = cTargetData.targetObj;
              if(isAssigned){
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isAssigned');
              }
              else if(isDraft){
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isDraft');
              }
              else if(isSuspended) {
                  lTargetInstance.SandDV1__Status__c = ConfigurationService.getTargetStatusValues().get('isSuspended');
              }
            
              List<Schema.SObjectField> targetFieldsList = FieldAccessibilityUtility.fetchTargetFields();
              SecurityUtils.checkInsert(SandDV1__Target__c.SObjectType,targetFieldsList);
              SecurityUtils.checkUpdate(SandDV1__Target__c.SObjectType,targetFieldsList);
              upsert lTargetInstance;
              childtarget.targetObj = lTargetInstance;


              if(lTargetInstance.Id != null){
                  if(cTargetData.periodTliWrapList != null && !cTargetData.periodTliWrapList.isEmpty()) {

                      List<SandDV1__Target_Line_Item__c> tLIList = new List<SandDV1__Target_Line_Item__c>();
                      tLIList = unWrapTargetLineItem(cTargetData.periodTliWrapList,lTargetInstance.Id);

                      List<Schema.SObjectField> targetsOLIFieldsList = FieldAccessibilityUtility.fetchTargetLineItemFields();
                      SecurityUtils.checkInsert(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
                      SecurityUtils.checkUpdate(SandDV1__Target_Line_Item__c.SObjectType,targetsOLIFieldsList);
                      upsert tLIList; 
                      childtarget.periodTliWrapList = wrapTargetLineItem(lTargetInstance.Id);

                  }
              }
          }
        
          childtarget.isAssigned  = isAssigned;
          childtarget.isDraft     = isDraft;
          childtarget.isSuspended = isSuspended;
          childtarget.userInstance = cTargetData.userInstance;

          return childtarget;
        }
        catch(Exception e) {

            Database.rollback(sp);
            throw new CustomException(e.getMessage());
        }
        
    }


    private static List<SandDV1__Target_Line_Item__c> unWrapTargetLineItem(List<TargetsDomain.PeriodTLIWrapper> periodTliWrapList,Id targetInstanceId){

        List<SandDV1__Target_Line_Item__c> tLIList = new List<SandDV1__Target_Line_Item__c>(); 
        for(TargetsDomain.PeriodTLIWrapper pTLI : periodTliWrapList) {
            if(pTLI.tliObj.SandDV1__Target__c == null){
                pTLI.tliObj.SandDV1__Target__c = targetInstanceId;
            }
           
            tLIList.add(pTLI.tliObj);
        }
        return tLIList;

    }
    

    private static List<TargetsDomain.PeriodTLIWrapper> wrapTargetLineItem(Id targetInstanceId){

        List<TargetsDomain.PeriodTLIWrapper> periodTLIList = new List<TargetsDomain.PeriodTLIWrapper>();
        for(SandDV1__Target_Line_Item__c tarLI :[SELECT Id,SandDV1__Aspect__c,SandDV1__Period__c,SandDV1__Period__r.SandDV1__System_Start_and_End_Date__c,SandDV1__Period__r.Name,SandDV1__Period__r.SandDV1__Start_Date__c,SandDV1__Period__r.SandDV1__End_Date__c,SandDV1__Target__c,SandDV1__Value__c 
            FROM SandDV1__Target_Line_Item__c WHERE SandDV1__Target__c =:targetInstanceId]){

            TargetsDomain.PeriodTLIWrapper pwrap = new TargetsDomain.PeriodTLIWrapper();
            pwrap.tliObj            = tarLI;
            pwrap.periodName        = tarLI.SandDV1__Period__r.Name;
            pwrap.periodEndDate     = String.valueOf(tarLI.SandDV1__Period__r.SandDV1__End_Date__c);
            pwrap.periodStartDate     = String.valueOf(tarLI.SandDV1__Period__r.SandDV1__Start_Date__c);
            periodTLIList.add(pwrap);
        }
        return periodTLIList;

    }

  /*******************************************************************************************************
  * @description Method to check a target exists
  * @param periodId- period Id, aspect - aspect value
  * @return boolean 
  */
  
  public static Boolean checkExistingTarget(String periodId,String aspect){

    List<SandDV1__Target__c> targetList = [SELECT Id, Name,SandDV1__Period__c,SandDV1__Aspect__c FROM SandDV1__Target__c WHERE SandDV1__Period__c =: periodId AND SandDV1__Aspect__c =: aspect AND (SandDV1__Assigned_To__c =: userinfo.getUserId() OR CreatedById =:userinfo.getUserId())];

    return targetList.isEmpty();

  }
}