/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 19-Oct-2015
* @description This is a Test class for SalesOrderController.
*/

@isTest(seeAllData = false)
private class SalesOrderControllerTest {
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static Product2 testProduct0;
    static Product2 testProduct;
    static Product2 testProduct2;
    static Pricebook2 testPricebook;
    static PricebookEntry testPriceEntry;
    static PricebookEntry testPriceEntry2;
    static PricebookEntry testPriceEntry3;
    static PricebookEntry testPriceEntry4;
    static Account testAccount;
    static SandDV1__Shipping_Address__c testShippingAddress;
    static SandDV1__Sales_Order__c testSalesOrder;
    static SandDV1__Sales_Order__c testSalesOrderDel;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLI0;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLI;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLIDel;
    static SandDV1__Sales_Order_Line_Item__c testSalesOrderLIDel2;


    /*******************************************************************************************************
    * @description Intial values for the test run
    * @param null
    * @return null 
    */
    
    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.AssertEquals(testRole.name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        testUser.ManagerId = sysAdmin.Id;
        insert testUser;
        System.AssertEquals(testUser.ManagerId,sysAdmin.Id);

        
        System.runAs(sysAdmin){
            
            testProduct0 = Initial_Test_Data.createProduct('Test Product0','test0001');
            insert testProduct0;


            testProduct = Initial_Test_Data.createProduct('Test Product','test001');
            insert testProduct;

            testProduct2 = Initial_Test_Data.createProduct('Test Product 2','test002');
            insert testProduct2;

            testPricebook = Initial_Test_Data.createPriceBook('Test pricebook');
            System.AssertEquals(testPricebook.Name,'Test pricebook');

            insert testPricebook;

            Id stdpb = Test.getStandardPricebookId();

            testPriceEntry = Initial_Test_Data.createPriceBookEntry(stdpb, testProduct.Id);
            insert testPriceEntry;


            testPriceEntry2 = Initial_Test_Data.createPriceBookEntry(testPricebook.Id, testProduct.Id);
            insert testPriceEntry2;

            testPriceEntry3 = Initial_Test_Data.createPriceBookEntry(stdpb, testProduct2.Id);
            insert testPriceEntry3;

            testPriceEntry4 = Initial_Test_Data.createPriceBookEntry(testPricebook.Id, testProduct2.Id);
            insert testPriceEntry4;

            testAccount = Initial_Test_Data.createAccount('Test Account', 500,false);
            insert testAccount;

            testShippingAddress = Initial_Test_Data.createShippingAddress('Test Shipping',testAccount.Id );
            insert testShippingAddress;

            testSalesOrder = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',testPricebook.Id);
            insert testSalesOrder;

            testSalesOrderLI0 = Initial_Test_Data.createSalesOrderLI(testSalesOrder.Id,testProduct0.Id,testPricebook.Id );
            insert testSalesOrderLI0;

            testSalesOrderLI = Initial_Test_Data.createSalesOrderLI(testSalesOrder.Id,testProduct.Id,testPricebook.Id );
            insert testSalesOrderLI;

            testSalesOrderDel = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',testPricebook.Id);
            insert testSalesOrderDel;

            testSalesOrderLIDel = Initial_Test_Data.createSalesOrderLI(testSalesOrderDel.Id,testProduct.Id,testPricebook.Id );
            insert testSalesOrderLIDel;

            testSalesOrderLIDel2 = Initial_Test_Data.createSalesOrderLI(testSalesOrderDel.Id,testProduct2.Id,testPricebook.Id );
            insert testSalesOrderLIDel2;
        }

    }

    @isTest static void test_method_one() {

        init();
        
        System.runAs(testUser){
            Test.startTest();

            SalesOrderController.fetchAccountsList(testAccount.Name);
            SalesOrderController.fetchShippingList(testAccount.Id);
            SalesOrderController.fetchPriceBookEntries(testPricebook.Id, testProduct.Name, testProduct.productCode);
            SalesOrderController.fetchPriceBookEntries(testPricebook.Id, null, testProduct.productCode);
            List<String> tempProductList = new List<String>();
            tempProductList.add(testProduct.Id);
            SalesOrderController.fetchPriceBookEntriesSoli(tempProductList, testPricebook.Id);
            SalesOrderController.fetchPriceBooks(testPricebook.Name);
            SalesOrderController.fetchPriceBooks(null);

            SandDV1__Sales_Order__c testSalesOrder2 = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',testPricebook.Id);
            
            SandDV1__Sales_Order_Line_Item__c testSalesOrderLI2 = Initial_Test_Data.createSalesOrderLI(null,testProduct.Id,testPricebook.Id );
            
            List<SandDV1__Sales_Order_Line_Item__c> soList = new List<SandDV1__Sales_Order_Line_Item__c>();
            soList.add(testSalesOrderLI2);
            List<SandDV1__Sales_Order_Line_Item__c> emptydeleteList = new List<SandDV1__Sales_Order_Line_Item__c>();
            SalesOrderController.saveSalesOrder(testSalesOrder2, soList,emptydeleteList,false);


            SandDV1__Sales_Order__c testSalesOrder3 = Initial_Test_Data.createSalesOrder(testAccount.Id,null,testPricebook.Id);
            
            SandDV1__Sales_Order_Line_Item__c testSalesOrderLI3 = Initial_Test_Data.createSalesOrderLI(null,testProduct.Id,testPricebook.Id );
            
            List<SandDV1__Sales_Order_Line_Item__c> soList2 = new List<SandDV1__Sales_Order_Line_Item__c>();
            soList2.add(testSalesOrderLI3);

            SalesOrderController.saveSalesOrder(testSalesOrder3, soList2,emptydeleteList,true);

            SalesOrderDomain.SalesOrderWrapper fetchedData00 = SalesOrderController.fetchSalesOrderRecords(testSalesOrder.Id);

            SalesOrderController.fetchAccount(testAccount.Id);
            SalesOrderController.fetchAccount('');

            testSalesOrderLI.SandDV1__New_Quantity__c = 4;
            upsert testSalesOrderLI;
            List<SandDV1__Sales_Order_Line_Item__c> soList3 = new List<SandDV1__Sales_Order_Line_Item__c>();
            soList3.add(testSalesOrderLI);
            soList3.add(testSalesOrderLI0);

            SalesOrderController.deleteSOLineItem(testSalesOrderLIDel2.Id);
            SalesOrderController.checkOrderCreation(testAccount.Id);

            List<SandDV1__Sales_Order_Line_Item__c> deleteList = new List<SandDV1__Sales_Order_Line_Item__c>();
            
            for(integer i=0;i<soList3.size();i++) {
                if(soList3[i].SandDV1__Part__c == testProduct0.Id) {
                    deleteList.add(soList3[i]);
                }
            }
            try {
            SalesOrderController.saveSalesOrder(testSalesOrder,soList3,deleteList,true);
            }
            catch(Exception e) {
                System.debug('e..'+e);
            }

            Test.stopTest();
        } 
    }


    @isTest static void test_method_exception2() {

        init();
        
        System.runAs(testUser){
            Test.startTest();

            try{

                SandDV1__Sales_Order__c testSalesOrder2 = Initial_Test_Data.createSalesOrder(testAccount.Id,null,testPricebook.Id);
            
                SandDV1__Sales_Order_Line_Item__c testSalesOrderLI2 = Initial_Test_Data.createSalesOrderLI(null,testProduct.Id,testPricebook.Id );
                
                List<SandDV1__Sales_Order_Line_Item__c> soList = new List<SandDV1__Sales_Order_Line_Item__c>();
                soList.add(testSalesOrderLI2);

                SalesOrderController.saveSalesOrder(null,soList,null,false); 
            }catch(Exception e){

                //Boolean expectedExceptionThrown =  e.getMessage().contains('Script-thrown exception') ? true : false;
                //System.AssertEquals(expectedExceptionThrown, true);
            }

            Test.stopTest();
        }
    }
}