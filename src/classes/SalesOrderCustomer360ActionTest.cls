/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 08-Dec-2015
* @description This is a Test class for SalesOrderCustomer360ActionConroller
*/

@isTest(seeAllData = false)
private class SalesOrderCustomer360ActionTest {
    
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static Account testAccount;
    static Pricebook2 testPricebook;
    static SandDV1__Sales_Order__c testSalesOrder;

    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.assertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        insert testUser;

        System.runAs(sysAdmin){
            
            testAccount = Initial_Test_Data.createAccount('Test Account', 500,false);
            insert testAccount; 

            testPricebook = Initial_Test_Data.createPriceBook('Test pricebook');
            insert testPricebook;
            System.assertEquals(testPricebook.Name,'Test pricebook');

            testSalesOrder = Initial_Test_Data.createSalesOrder(testAccount.Id,'Draft',testPricebook.Id);
            insert testSalesOrder;

        }
    }

    @isTest static void test_method_one() {
        
        init();
        System.runAs(testUser){

            Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(testSalesOrder);
            SalesOrderCustomer360ActionConroller salObj = new SalesOrderCustomer360ActionConroller(sc);
            Test.stopTest(); 
        }
    }
    
}