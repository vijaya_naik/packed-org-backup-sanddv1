/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/*
Test Class is written to cover Scheme Module.
*/
@isTest(seeAllData = false)
private class SchemeTestClass {

    

    /*******************************************************************************************************
    * @description Intial values for the test run
    * @param null
    * @return null 
    */
    static Account accountObj;
    static Product2 productObj;
    static Pricebook2 pricebookObj;
    static SandDV1__Sales_Order__c salesOrderObj;
    static SandDV1__Scheme__c schemeObj;
    static SandDV1__Scheme_Benefit_Range__c SchemeBenefitRange;
    static SandDV1__Scheme_Benefit__c schemeBenefit;
    static SandDV1__Scheme_Condition__c schemeCondition;
    static SandDV1__Sales_Order_Item_Benefit__c  salesOrderBenefit;

    static List<SandDV1__Sales_Order_Line_Item__c> salesOrderLIList = new List<SandDV1__Sales_Order_Line_Item__c>();

    static void init() {
        //create dummy Account
        accountobj = InitializeTest.createAccount();
        insert accountobj;
        //create dummy product
        productObj = InitializeTest.createProduct();
        insert productObj;
        //create dummy price book
        pricebookObj = InitializeTest.createPricebook();
        insert pricebookObj;
        
        //create dummy sales order 
        salesOrderObj = InitializeTest.createSalesOrder(accountobj.Id,'Draft');
        insert salesOrderObj;
        System.assertEquals(salesOrderObj.SandDV1__Status__c,'Draft');

        // create multiple sales order line item under sales order 
        for(integer i=0;i<5;i++){
            SandDV1__Sales_Order_Line_Item__c salesOrderLIObj = InitializeTest.createSalesOrderLineItem(salesOrderObj.Id,productObj.Id,0,pricebookObj.Id,10,i);
            salesOrderLIList.add(salesOrderLIObj);
        }
        insert salesOrderLIList;

        //create Scheme
        schemeObj = InitializeTest.createScheme('TestScheme','Active',1);
        insert schemeObj;

        //create SchemeBenefitRange
        SchemeBenefitRange = InitializeTest.createSchemeBenefitRange(schemeObj.Id,30.00,1.00);
        insert SchemeBenefitRange;

        //create Scheme Benefit
        schemeBenefit = InitializeTest.createSchemeBenefit(productObj.Id,20,SchemeBenefitRange.Id,'FOC');
        insert schemeBenefit;

        String orderbenefitcombi = String.Valueof(salesOrderObj.Id) + String.Valueof(schemeBenefit.Id);
        //create Sales Order Benefit 
        salesOrderBenefit = InitializeTest.createSalesOrderBenefit(salesOrderLIList[1].Id,schemeBenefit.Id,orderbenefitcombi);
        insert salesOrderBenefit;
        System.assertEquals(salesOrderBenefit.SandDV1__Order_Item_Benefit_Combination__c,orderbenefitcombi);
        //create Scheme Condition
        schemeCondition = InitializeTest.createSchemeCondition('SandDV1__Part__c','SandDV1__Sales_Order_Line_Item__c',productObj.Id,schemeObj.Id,'','');
        insert schemeCondition;
    }

    @isTest static void SchemeTest() {

        init();

        List<SchemeDomain.SchemeBenefitsWrapperV2> schemebenefitWrapList = new List<SchemeDomain.SchemeBenefitsWrapperV2>();
        List<SchemeDomain.SchemeBenefitsWrapperV2> newSchemebenefitWrapList = new List<SchemeDomain.SchemeBenefitsWrapperV2>();
        List<SchemeDomain.BenefitWrapper> benefitWrapList = new List<SchemeDomain.BenefitWrapper>();

        SchemeDomain.BenefitWrapper  benefitWrap = new SchemeDomain.BenefitWrapper ();
        benefitWrap.schemeBenefit  = schemeBenefit;
        benefitWrap.isSelected = true;
        benefitWrap.isSelectedOld  = false;
        benefitWrapList.add(benefitWrap);
        System.assertEquals(benefitWrap.isSelectedOld,false);

        for(integer i=0;i<salesOrderLIList.size();i++) {
            SchemeDomain.SchemeBenefitsWrapperV2 schemebenefitWrap = new SchemeDomain.SchemeBenefitsWrapperV2();
            schemebenefitWrap.salesOrderLineItem  = salesOrderLIList[i];
            schemebenefitWrap.salesOrder = salesOrderObj;
            schemebenefitWrap.schemeBenefitsList = benefitWrapList;
            schemebenefitWrapList.add(schemebenefitWrap);
        }

        SchemeApplicationController.getApplicableSchemeBenefits(salesOrderObj.Id);
        SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = false;
            try {
                SchemeApplicationController.applySchemeBenefits(schemebenefitWrapList);

            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to insert field') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }

        SchemeDomain.BenefitWrapper  newBenefitWrap = new SchemeDomain.BenefitWrapper ();
        benefitWrap.schemeBenefit  = schemeBenefit;
        benefitWrap.isSelected = false;
        benefitWrap.isSelectedOld  = true;
        benefitWrapList.add(benefitWrap);
        

        for(integer i=0;i<salesOrderLIList.size();i++) {
            SchemeDomain.SchemeBenefitsWrapperV2 schemebenefitWrap = new SchemeDomain.SchemeBenefitsWrapperV2();
            schemebenefitWrap.salesOrderLineItem  = salesOrderLIList[i];
            schemebenefitWrap.salesOrder = salesOrderObj;
            schemebenefitWrap.schemeBenefitsList = benefitWrapList;
            newSchemebenefitWrapList.add(schemebenefitWrap);
        }
        SecurityUtils.BYPASS_INTERNAL_FLS_AND_CRUD = false;
            try {
                SchemeApplicationController.applySchemeBenefits(newSchemebenefitWrapList);
            }
            catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('You do not have permission to insert field') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
            }
    }
    
}