public with sharing class HttpCalloutsUtil {
	private HttpRequest req;
    private String responseBody;
    private HttpResponse resp;
    
    public HttpCalloutsUtil() {
	   responseBody = '';	
	}

    private void createHttpRequest(String endPoint, String method, Map<String, String> paramsMap, Map<String, String> headersMap ,String body, Integer timeOut){
        
        try {
            if(paramsMap != null) {
                endpoint+='?';
                for(String key:paramsMap.keySet()) {
                    endpoint += key+'='+paramsMap.get(key)+'&';
                }
                endpoint = endpoint.substring(0, endpoint.length()-1);  
            }
            System.debug(endpoint);
            // Set the HTTP endpoint
            this.req.setEndpoint(endPoint);
            // Set the HTTP method type (POST, GET ,PUT etc.)
            this.req.setMethod(method);
            // Set the HTTP body
            if(body != '') { 
                this.req.setBody(body);
            }
            // Set the Timeout of the request
            if(timeOut != null) {
                this.req.setTimeout(timeOut); 
            }   
            
            if (headersMap != null) {
                for(String headerKey : headersMap.keySet()) {
                    //Set the HTTP header to content type
                    this.req.setHeader(headerKey,headersMap.get(headerKey));
                    //System.debug(headerValue+' head '+headerKey);   
                }
            }
            System.debug('Request--'+this.req.getBody());
        }
        catch(Exception excptn) {
            throw excptn;
        }

    }

    private void sendHttpRequest() {

        Http httpObject = new Http();
        this.resp = httpObject.send(this.req);
        
    }

    private String getResponseBody() {
        return resp.getBody();
    } 

	public String makeHttpCallout(String endPoint, String method){
        return makeHttpCallout(endPoint, method, null, null, '', null);
    }

    public String makeHttpCallout(String endPoint, String method, Map<String, String> paramsMap){
        return makeHttpCallout(endPoint, method, paramsMap, null, '', null);
    }

    public String makeHttpCallout(String endPoint, String method,  Map<String, String> paramsMap, Integer timeOut){
        return makeHttpCallout(endPoint, method, paramsMap, null, '', timeOut);
    }

	public String makeHttpCallout(String endPoint, String method, Map<String, String> headersMap ,String body){
        return makeHttpCallout(endPoint, method, null, headersMap, body, null);
    }

    public String makeHttpCallout(String endPoint, String method, Map<String, String> headersMap ,String body, Integer timeOut){
        return makeHttpCallout(endPoint, method, null, headersMap, body, null);
    }

    public String makeHttpCallout(String endPoint, String method, Map<String, String> paramsMap, Map<String, String> headersMap ,String body){
        return makeHttpCallout(endPoint, method, paramsMap, headersMap, body, null);
    }

    public String makeHttpCallout(String endPoint, String method, Map<String, String> paramsMap, Map<String, String> headersMap ,String body, Integer timeOut) {
        
        try {
            
            if(this.req == null && this.resp == null) {
                
                this.req = new HttpRequest();
                createHttpRequest(endPoint, method, paramsMap, headersMap, body, timeOut);
                this.resp = new HttpResponse();
                sendHttpRequest();
                if(this.resp.getStatusCode() == 200 || this.resp.getStatusCode() == 201) {
                    
                    this.responseBody= getResponseBody();
                
                }
                else {
                    
                    throw new HttpCalloutException('Error: Callout failed - Status Code: '+this.resp.getStatusCode()+', Status: '+this.resp.getStatus());
                
                }

            }

            return this.responseBody;
        }
        catch(Exception excptn) {
            throw excptn;
        }
                
    }

    //Public method to fetch a specific value from the JSON response
    public String fetchJSONValue(String value) {
        
        try {
            if(this.responseBody != null) {
                JSONParser parser = JSON.createParser(this.responseBody);
                String matchedValue ='';      
                
                while (parser.nextToken() != null) {
                    
                    if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)){
                        String fieldName = parser.getText();
                        parser.nextToken();
                        if(fieldName == value) {
                            matchedValue = parser.getText();
                            break;
                        }
                    }
                }

                return matchedValue;
            }

            else {
                throw new responseBodyNotFoundException('Error: HttpRequest is not set');
            }
        }
        catch(Exception excptn) {
            throw excptn;
        }
    }   
    
    public class responseBodyNotFoundException extends Exception {}
    public class HttpCalloutException extends Exception {}   
}