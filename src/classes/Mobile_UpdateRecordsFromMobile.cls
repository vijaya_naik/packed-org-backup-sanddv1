@RestResource(urlmapping='/Mobile_UpdateRecordsFromMobile/*')
global class Mobile_UpdateRecordsFromMobile {
    
    /** Wrapper Class to Parse the data from Mobile**/
    global class DataFromMobile{        
        public SandDV1__Sales_Order__c orderRecord                          {get;set;}       
        public List<SandDV1__Sales_Order_Line_Item__c> orderLineItemRecord  {get;set;}        
    }
    
    global class SalesForceToMobile{                                 
        public List<SandDV1__Sales_Order__c> salesOrders                       {get;set;}                      
        public List<SandDV1__Sales_Order_Line_Item__c> salesOrderLineItems     {get;set;}  
        
        public  SalesForceToMobile(){
            salesOrders = new List<SandDV1__Sales_Order__c>();
            salesOrderLineItems = new List<SandDV1__Sales_Order_Line_Item__c>();
        }       
        
    }
    
    
    @HttpPost
    global static String getRecords(){
        RestRequest request         = RestContext.request;
        RestResponse response       = RestContext.response; 
        
        
        String SO_MOB_ID_CONTAINS = 'SO_NEW_';
        String SOLI_MOB_ID_CONTAINS = 'SOLI_NEW_';
        
        Map<String,Id> OfflineSalesOrderIdToSFId = new Map<String,Id>();
        List<Database.Upsertresult> SalesOrderUpsertResult = new List<Database.upsertResult>();
        List<Database.Upsertresult> SalesOrderLineItemResult = new List<Database.upsertResult>();
        
        //Schema.SObjectField offlineSalesOrderId = SandDV1__Sales_Order__c.Fields.SandDV1__Offline_SOID__c ;
        //Schema.SObjectField offlineSalesOrderLIId = SandDV1__Sales_Order_Line_Item__c.Fields.SandDV1__Offline_SOLIID__c ;
        
        List<Id> salesOrderLIList = new List<Id>();
        
        List<DataFromMobile> mobileData = new List<DataFromMobile>();
        //System.debug('-------------JSON Request---------------'+request.requestBody.toString());
        mobileData = (List<DataFromMobile>)JSON.deserialize(request.requestBody.toString(),List<DataFromMobile>.class);
        system.debug(mobileData + 'DEBUGGING MOBILE DATA FOR QUANTITY');
        
        System.Debug('***mobileData***'+mobileData);
        
        map<SandDV1__Sales_Order__c,List<SandDV1__Sales_Order_Line_Item__c>> newMap = new map<SandDV1__Sales_Order__c,List<SandDV1__Sales_Order_Line_Item__c>>();
        map<String,List<SandDV1__Sales_Order_Line_Item__c>> newMapDistID= new map<String,List<SandDV1__Sales_Order_Line_Item__c>>();
        map<String,String> offlineIDtoSFID = new map<String,String>();
        
        
        list<SandDV1__Sales_Order_Line_Item__c> newList = new list<SandDV1__Sales_Order_Line_Item__c>();
        list<SandDV1__Sales_Order__c> newOrder=new  list<SandDV1__Sales_Order__c>();
        String err;
        try{
            for(DataFromMobile dfm : mobileData){
                
                System.Debug('***dfm***'+dfm.orderRecord);
                System.Debug('***dfm***'+dfm.orderLineItemRecord);
                
                List<SandDV1__Sales_Order_Line_item__c> tempOliList = new List<SandDV1__Sales_Order_Line_item__c>();
                String OrderID ;     
                if(dfm.OrderRecord!=null){
                    dfm.OrderRecord.SandDV1__Date__c=System.Now(); 
                    dfm.OrderRecord.SandDV1__Status__c = 'Open'; 
                    newOrder.add(dfm.OrderRecord);                                                                                                                                                                                                                      
                    Database.SaveResult[] srList = Database.insert(new list<SandDV1__Sales_Order__c>{dfm.OrderRecord},false);
                    OrderID = srList[0].getID(); 
                    if(dfm.orderLineItemRecord!=null){
                        tempOliList  = dfm.orderLineItemRecord;
                        
                    }
                }                                                                                                               
                
                //insert dfm.OrderRecord;
                
                for(SandDV1__Sales_Order_Line_item__c oli :tempOliList){
                    oli.SandDV1__Sales_Order__c = OrderID;
                    /*if( oli.SandDV1__Packing_Size__c == 'KG' )
                    {
                        oli.SandDV1__Quantity_in_kgs__c = oli.SandDV1__Quantity__c;                                                                                                 
                    }
                    else
                    {
                        oli.SandDV1__Quantity_in_Packets__c = oli.SandDV1__Quantity__c;
                    }
                    oli.SandDV1__Quantity__c = null;*/
                    newList.add(oli);
                    //insert newList;
                }
                
            }     
            insert newList;
            return 'Success';
        }
        catch(Exception e){
            system.debug('***Exception Occured***'+e.getMessage());
            err = e.getMessage();
            return err;
        } 
        return err;
    }
}