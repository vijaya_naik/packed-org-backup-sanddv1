/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @description This is a Helper Class for AccountTrigger.
*/
public with sharing class OnBoardingAccountTriggerHelper {
  
  /*-- SINGLETON PATTERN --*/
    private static OnBoardingAccountTriggerHelper instance;
    public static OnBoardingAccountTriggerHelper getInstance() {
        if (instance == null) {
            instance = new OnBoardingAccountTriggerHelper ();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description : Method to execute before insertion of account. Check custom setting value and update
             BLock Order Creation in account.
    * @param : List<Account> newAccounts
    * @return : null
    */

  public void onBeforeInsert(List<Account> newAccounts,Map<Id,Account> oldMap) {
      Set<ID> accountSet = new Set<ID>();
      Set<ID> docAccountSet = new Set<ID>();
      Set<ID> configurationSet = new Set<ID>();
      Set<ID> notCompleteDocSet = new Set<ID>();
      Map<ID,Set<ID>> settingDocMap = new Map<ID,Set<ID>>();
      
      String accountActiveStatus = ConfigurationService.OnBoardingAccountActiveStatus;
      String onBoardingApprovedStatus = ConfigurationService.OnBoardingApprovedStatus;
      for(Account acc:newAccounts){
        if(oldMap != null) {
            if(acc.SandDV1__Status__c == accountActiveStatus && !oldMap.isempty() && oldMap.get(acc.Id).SandDV1__Status__c != accountActiveStatus) {
                accountSet.add(acc.Id);
            }
        }
        else if(oldMap == null && acc.SandDV1__Status__c == accountActiveStatus) {
             accountSet.add(acc.Id);
        } 
      }
      for(SandDV1__OnBoardingDocumentSetting__c docSet :ConfigurationService.fetchMandatoryOnBoardingDocSetting()) {
          configurationSet.add(docSet.Id);
      }
      
      for(SandDV1__OnBoarding_Document__c doc :OnBoardingService.fetchAccountDocs(accountSet)) {
              if(doc.SandDV1__Status__c != onBoardingApprovedStatus) {
                   notCompleteDocSet.add(doc.SandDV1__AccountId__c);
              }
             docAccountSet.add(doc.SandDV1__System_Custom_Setting_ID__c);
              if(settingDocMap.containskey(doc.SandDV1__AccountId__c)) {
                  settingDocMap.get(doc.SandDV1__AccountId__c).add(doc.SandDV1__System_Custom_Setting_ID__c);
              }
              else {
                  Set<Id> docSet = new Set<Id>();
                  docSet.add(doc.SandDV1__System_Custom_Setting_ID__c);
                  settingDocMap.put(doc.SandDV1__AccountId__c,docSet);
              }
      }
      
      for(Account acc : newAccounts) {
          if(notCompleteDocSet.contains(acc.Id)) {
               acc.adderror('Documents are not Approved.');
          }
          if(!settingDocMap.isempty() && !settingDocMap.get(acc.Id).containsAll(configurationSet)) {
              acc.adderror('Manadatory Documents are not Uploaded or Approved.');
          }
          if((!settingDocMap.isempty() && settingDocMap.get(acc.Id).isempty())) {
              acc.adderror('Manadatory Documents are not Uploaded or Approved.');
          }
          if(!configurationSet.isempty() && settingDocMap.isempty() && !accountSet.isempty()) {
              acc.adderror('Manadatory Documents are not Uploaded.');
          }
      }
  }
}