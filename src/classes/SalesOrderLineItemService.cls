public with sharing class SalesOrderLineItemService {

   public static List<SandDV1__Sales_Order_Line_Item__c> fetchOrderLineItemRecords(Set<Id> salesOrderIdSet){
        return new List<SandDV1__Sales_Order_Line_Item__c>(
            [SELECT Id, Name, SandDV1__Part__c FROM SandDV1__Sales_Order_Line_Item__c WHERE SandDV1__Sales_Order__c IN :salesOrderIdSet]
        );
    }
    
    public static List<SandDV1__Sales_Order_Line_Item__c> getSalesOrderLineItems(String salesOrderId){
        return new List<SandDV1__Sales_Order_Line_Item__c>(
            [SELECT Id, Name, SandDV1__Part__c,SandDV1__System_Product_Name__c,SandDV1__Quantity__c,SandDV1__Unit_Price__c,SandDV1__Sales_Price__c
                FROM SandDV1__Sales_Order_Line_Item__c
                WHERE SandDV1__Sales_Order__c = :salesOrderId]
        );
    }


    public static List<SandDV1__Sales_Order_Line_Item__c> getNonFOCSalesOrderLineItems(String salesOrderId){
        return new List<SandDV1__Sales_Order_Line_Item__c>(
            [SELECT Id, Name, SandDV1__Part__c,SandDV1__System_Product_Name__c,SandDV1__Quantity__c,SandDV1__Unit_Price__c,SandDV1__Sales_Price__c
                FROM SandDV1__Sales_Order_Line_Item__c
                WHERE SandDV1__Sales_Order__c = :salesOrderId
                AND SandDV1__Type__c != 'FOC']
        );
    }


    public static List<SandDV1__Sales_Order_Line_Item__c> getSalesOrderLineItems(Set<String> salesOrderLineItemIdSet){
        return new List<SandDV1__Sales_Order_Line_Item__c>(
            [SELECT Id, Name, SandDV1__Part__c,SandDV1__System_Product_Name__c,SandDV1__Quantity__c, SandDV1__Sales_Order__c, SandDV1__Discount__c, SandDV1__Discount_Percentage__c
                FROM SandDV1__Sales_Order_Line_Item__c
                WHERE Id IN :salesOrderLineItemIdSet]
        );
    }


    public static Map<String, SandDV1__Sales_Order_Line_Item__c> getSalesOrderLineItemsMap(Set<String> salesOrderLineItemIdSet){
        return new Map<String, SandDV1__Sales_Order_Line_Item__c>(getSalesOrderLineItems(salesOrderLineItemIdSet));
    }


    public static SandDV1__Sales_Order_Line_Item__c getSalesOrderLineItem(SandDV1__Scheme_Benefit__c schemeBenefit, String salesOrderItemBenefitId, String salesOrderId){
        SandDV1__Sales_Order_Line_Item__c salesOrderLineItem = new SandDV1__Sales_Order_Line_Item__c();
        salesOrderLineItem.SandDV1__Sales_Order_Item_Benefit__c = salesOrderItemBenefitId;
        salesOrderLineItem.SandDV1__Sales_Order__c = salesOrderId;
        salesOrderLineItem.SandDV1__Part__c = schemeBenefit.SandDV1__Product__c;
        salesOrderLineItem.SandDV1__Quantity__c = schemeBenefit.SandDV1__Value__c;
        salesOrderLineItem.SandDV1__Sales_Price__c = 0;
        salesOrderLineItem.SandDV1__Unit_Price__c = 0;
        salesOrderLineItem.SandDV1__Total_Price__c = 0;
        salesOrderLineItem.SandDV1__Type__c = 'FOC';
        return salesOrderLineItem;
    }


    public static List<SandDV1__Sales_Order_Line_Item__c> getSalesOrderLineItemForBenefits(Set<String> salesOrderItemBenefitIdSet){
        return new List<SandDV1__Sales_Order_Line_Item__c>(
            [SELECT Id 
            FROM SandDV1__Sales_Order_Line_Item__c 
            WHERE SandDV1__Sales_Order_Item_Benefit__c IN :salesOrderItemBenefitIdSet]
        );
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__Sales_Order_Line_Item__c Record from Sales Order Id.
    * @param :- Id.
    * @return :- List<SandDV1__Sales_Order_Line_Item__c>. 
    */
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSalesOrderLineItemRecords(Id salesOrderId) {
        return [Select Id,SandDV1__Part__c,SandDV1__System_Product_Code__c,SandDV1__Type__c,SandDV1__Net_Price__c,SandDV1__Sales_Price__c,SandDV1__Part__r.Name,SandDV1__Part__r.ProductCode,SandDV1__Fulfilled_Quantity__c,
                SandDV1__Unfulfilled_Quantity__c,SandDV1__Price_Book__c,SandDV1__Price_Book__r.Name,SandDV1__Quantity__c,SandDV1__Sales_Order__c,SandDV1__Unit_Price__c,SandDV1__UOM__c from SandDV1__Sales_Order_Line_Item__c
                where SandDV1__Sales_Order__c =: salesOrderId];
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__Sales_Order_Line_Item__c Record from Sales Order Id whose quantity greater 0.
    * @param :- Id.
    * @return :- List<SandDV1__Sales_Order_Line_Item__c>. 
    */
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSalesOrderLineItemRecordsGreaterQuantity(Id salesOrderId) {
        return [Select Id,SandDV1__Part__c,SandDV1__System_Product_Code__c,SandDV1__Type__c,SandDV1__Net_Price__c,SandDV1__Sales_Price__c,SandDV1__Part__r.Name,SandDV1__Part__r.ProductCode,SandDV1__Fulfilled_Quantity__c,
                SandDV1__Unfulfilled_Quantity__c,SandDV1__Price_Book__c,SandDV1__Price_Book__r.Name,SandDV1__Quantity__c,SandDV1__Sales_Order__c,SandDV1__Unit_Price__c,SandDV1__UOM__c from SandDV1__Sales_Order_Line_Item__c
                where SandDV1__Sales_Order__c =: salesOrderId AND SandDV1__Unfulfilled_Quantity__c > 0];
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__Sales_Order_Line_Item__c List from Sales Order Line Item Id Set.
    * @param :- Set<Id>.
    * @return :- List<SandDV1__Sales_Order_Line_Item__c>. 
    */
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSalesOLIRecords(Set<Id> salesOrderLineItemID) {
        return [Select Id,SandDV1__Part__c,SandDV1__Sales_Order__r.SandDV1__Status__c,SandDV1__System_Product_Code__c,SandDV1__Type__c,SandDV1__Net_Price__c,SandDV1__Sales_Price__c,SandDV1__Part__r.Name,SandDV1__Part__r.ProductCode,
                SandDV1__Fulfilled_Quantity__c,SandDV1__Unfulfilled_Quantity__c,SandDV1__Price_Book__c,SandDV1__Price_Book__r.Name,SandDV1__Quantity__c,SandDV1__Sales_Order__c,SandDV1__Unit_Price__c,SandDV1__UOM__c
                from SandDV1__Sales_Order_Line_Item__c where ID IN: salesOrderLineItemID];
    }

    /*******************************************************************************************************
    * @description :- Method to fetch SandDV1__Sales_Order_Line_Item__c List from Sales Order Id Set. 
    * @param :- set of Id.
    * @return :- List<SandDV1__Sales_Order_Line_Item__c>. 
    */
    public static List<SandDV1__Sales_Order_Line_Item__c> fetchSOLIRecords(Set<Id> salesOrderID) {
        return [Select Id,SandDV1__Part__c,SandDV1__System_Product_Code__c,SandDV1__Type__c,SandDV1__Net_Price__c,SandDV1__Sales_Price__c,SandDV1__Part__r.Name,SandDV1__Part__r.ProductCode,SandDV1__Fulfilled_Quantity__c,
                SandDV1__Unfulfilled_Quantity__c,SandDV1__Price_Book__c,SandDV1__Price_Book__r.Name,SandDV1__Quantity__c,SandDV1__Sales_Order__c,SandDV1__Unit_Price__c,SandDV1__UOM__c 
                from SandDV1__Sales_Order_Line_Item__c where SandDV1__Sales_Order__c IN: salesOrderID order by SandDV1__Sales_Order__c];
    }
}