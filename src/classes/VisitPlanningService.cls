/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author Extentor
* @date Aug 2015
* @description This class is for providing data to Visit Controller in the application.
*/

public with sharing class VisitPlanningService {
    
    
    /*******************************************************************************************************
* @description gets all Accounts accessible to the user.
* @return list<Account> - returns list of Accounts. 
*/
    
    public static List<Account> getAccountInfo(String accountCategory) {

        List<Account> acc;
        if(accountCategory != '' && accountCategory != null && accountCategory != Label.Select_Category) {
            acc = new List<Account>([select Id, Name, SandDV1__Account_Category__c, SandDV1__Credit_Limit__c from Account where SandDV1__Account_Category__c =: accountCategory ORDER BY Name limit 50000]);
        } else {
            acc = new List<Account>([select Id, Name, SandDV1__Account_Category__c, SandDV1__Credit_Limit__c from Account ORDER BY Name limit 50000]);
        }
        
        return acc;
    }
    
    
    
    /*******************************************************************************************************
* @description gets the Active Period sorted by Startdate
* @params periodId - recordid of Period object.
* @return list<SandDV1__Period__c> - returns specific(based on recordid) or list of Periods which are Active. 
*/
    
    public static List<SandDV1__Period__c> getActivePeriodInfo(Id periodId) {
        
        string periodQuery = 'Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true and SandDV1__Visit_Plan_Visible__c = true ';
        
        if(periodId != null) {
            periodQuery += 'and id =\''+ periodId+'\'';
        } else {
            periodQuery += 'ORDER BY SandDV1__Start_Date__c';
        }
        return (database.query(periodQuery));
    }
    
    /*******************************************************************************************************
* @description gets the Active Sub Period sorted by Startdate
* @params periodId - recordid of Period object.
* @return list<SandDV1__Period__c> - returns child periods of particular Period.*/
    
    public static List<SandDV1__Period__c> getActiveSubPeriodInfo(Id periodId) {
        
        string periodQuery = 'Select Id, Name, SandDV1__Start_Date__c, SandDV1__End_Date__c from SandDV1__Period__c where SandDV1__Active__c = true ';
        
        if(periodId != null) {
            periodQuery += 'and SandDV1__Master_Period__c =\''+ periodId+'\' ORDER BY SandDV1__Start_Date__c';
        } else {
            periodQuery += 'ORDER BY SandDV1__Start_Date__c';
        }
        return (database.query(periodQuery));
    }
    
    /*******************************************************************************************************
* @description gets the loggedin user's planned VisitPlan.
* @params userId - recordid of Period object.
* @return list<SandDV1__Visit_Plan__c> - returns the VisitPlan records of current user, which were planned & executed.*/
    
    public static List<SandDV1__Visit_Plan__c> getCurrentUserVisitPlan(Id userId) {
        
        return new List<SandDV1__Visit_Plan__c>([select Id, Name, SandDV1__Period__c from SandDV1__Visit_Plan__c where SandDV1__User__c =: userId]);
    }
    
    /*******************************************************************************************************
* @description gets the current VisitPlan.
* @params visitPlanId - recordid of Visitplan id object.
* @return list<SandDV1__Visit_Plan__c> - returns the VisitPlan records of current user, which were planned & executed.*/
    
    public static SandDV1__Visit_Plan__c getCurrentVisitPlan(Id visitPlanId) {
        
        return ([select Id, Name, SandDV1__Number_Of_Minimum_Visits__c, SandDV1__Period__c, SandDV1__User__c, SandDV1__User__r.Name, SandDV1__period__r.Name, SandDV1__period__r.SandDV1__Start_Date__c, SandDV1__period__r.SandDV1__End_Date__c, SandDV1__Status__c from SandDV1__Visit_Plan__c where id =: visitPlanId limit 1]);
    }

    /*******************************************************************************************************
* @description gets the current VisitPlan.
* @params visitPlanId - recordid of Visitplan id object.
* @return list<SandDV1__Visit__c> - returns the VisitPlan records of current user, which were planned & executed.*/
    
    public static List<SandDV1__Visit__c> getVisits(Id visitPlanId) {
        
        return new List<SandDV1__Visit__c>([select Id, Name, SandDV1__Account__c, SandDV1__Account__r.Name, SandDV1__Status__c, SandDV1__Account__r.SandDV1__Account_Category__c, SandDV1__Account__r.SandDV1__Credit_Limit__c from SandDV1__Visit__c where SandDV1__Visit_Plan__c =: visitPlanId  ORDER BY SandDV1__Account__r.Name]);
    }
    
    /*******************************************************************************************************
* @description gets the Active Route Info.
* @return list<SandDV1__Route__c> - returns list of routes which are Active. 
*/
    
    public static List<SandDV1__Route__c> getRouteInfo() {
        
        return new List<SandDV1__Route__c>([select Id, Name from SandDV1__Route__c where SandDV1__Active__c = true]);
    }
    
    
    /*******************************************************************************************************
* @description gets the Active Route Account Info.
* @return list<SandDV1__Route_Account__c> - returns list of routes which are Active. 
*/
    
    public static List<SandDV1__Route_Account__c> getRouteAccountInfo(Id routeId, String accountCategory) {
        
        List<SandDV1__Route_Account__c> routeAcc;
        if(accountCategory != null && accountCategory != '' && accountCategory != Label.Select_Category) {
            routeAcc = new List<SandDV1__Route_Account__c>([select SandDV1__Account__c, SandDV1__Account__r.Name, SandDV1__Account__r.SandDV1__Account_Category__c, SandDV1__Account__r.SandDV1__Credit_Limit__c from SandDV1__Route_Account__c where SandDV1__Account__r.SandDV1__Account_Category__c =: accountCategory and SandDV1__Route__c =: routeId and SandDV1__Active__c = true ORDER BY SandDV1__Account__r.Name]);
            } else {
             routeAcc = new List<SandDV1__Route_Account__c>([select SandDV1__Account__c, SandDV1__Account__r.Name, SandDV1__Account__r.SandDV1__Account_Category__c, SandDV1__Account__r.SandDV1__Credit_Limit__c from SandDV1__Route_Account__c where SandDV1__Route__c =: routeId and SandDV1__Active__c = true ORDER BY SandDV1__Account__r.Name]);  
            }
        return routeAcc;
    }
    
    
    /*******************************************************************************************************
* @description gets the Active Route Info.
* @params startDate  - StartDate of the Period.  
* @params endDate    - Enddate of the period.
* @return list<Date> - returns list of all dates between Start and End Date. 
*/
    
    public static List<Holiday> getHolidays(Date startDate, Date endDate) {
        
        return new List<Holiday>([SELECT Name, ActivityDate FROM Holiday where (ActivityDate >=: startDate) and (ActivityDate <=: endDate)]);
    }
    
    /*******************************************************************************************************
* @description - Query Business Hours of particular Org to find Weekends.
* @return BusinessHours - returns BusinessHours object values.. 
*/
    public static BusinessHours getWeekend() {
        
        return ([SELECT FridayEndTime,FridayStartTime,MondayEndTime,MondayStartTime,Name,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayEndTime,WednesdayStartTime FROM BusinessHours WHERE IsDefault = true limit 1]);
    }
    
    /*******************************************************************************************************
* @description gets the current user Manager.
* @params userId - passes UserId.
* @return User - returns user Detail of particular User. 
*/
    public static User getuserDetails(Id userId) {
        
        return ([select ManagerId from User where id =: userId]);
    }
    
    
    /*******************************************************************************************************
* @description gets the Active Route Info.
* @params startDate  - StartDate of the Period.  
* @params endDate    - Enddate of the period.
* @return list<Date> - returns list of all dates between Start and End Date. 
*/  
    public static List<Date> getDatesBetween(Date startDate, Date endDate) {
        
        List<Date> dateList      = new List<Date>();
        Integer daysBetween      = startDate.daysBetween(endDate);
        
        for(integer i=0, j=daysBetween; i <= j; i++) {
            
            dateList.add(startDate.adddays(i));
        }
        return dateList;
    }
    
    /*******************************************************************************************************
* @description gets the planned Visits.
* @params visitPlanId - selected visitPlan Id. 
* @return list<SandDV1__Visit__c> - returns list of visits under the particular VisitPlan. 
*/

    public static List<SandDV1__Visit__c> getPlannedVisits(Id visitPlanId) {
        
        return new List<SandDV1__Visit__c>([select Name, SandDV1__Account__c, SandDV1__Visit_Plan_Date__c from SandDV1__Visit__c where SandDV1__Visit_Plan__c =: visitPlanId ORDER BY SandDV1__Visit_Plan_Date__c]);
    }
    
    /*******************************************************************************************************
* @description gets the planned Visits for current Month.
* @return list<SandDV1__Visit__c> - returns list of visits under the particular user for the Current Month. 
*/  

    public static List<SandDV1__Visit__c> getPlannedVisitsCurrentMonth() {
        
        return new List<SandDV1__Visit__c>([select Name, SandDV1__Account__c, SandDV1__Visit_Plan_Date__c, SandDV1__Visit_Plan__r.SandDV1__User__c, SandDV1__Account__r.Name from SandDV1__Visit__c where SandDV1__Visit_Plan_Date__c = THIS_MONTH AND SandDV1__Visit_Plan__r.SandDV1__User__c =: userInfo.getUserId() ORDER BY SandDV1__Visit_Plan_Date__c]);
    }

 /*******************************************************************************************************
* @description gets the Visit Plan default target for the organisation.
* @return list<SandDV1__Visit_Plan_Default_Target__c> - returns list of SandDV1__Visit_Plan_Default_Target__c. 
*/  

    public static List<SandDV1__Visit_Plan_Default_Target__c> getVisitPlanTargetPerMonth() {

        return SandDV1__Visit_Plan_Default_Target__c.getall().values();
    }


 /*******************************************************************************************************
* @description gets the recordType information of any SObject using Schema.
* @params sObjectApiName - sObject api Name.
* @params recordTypeName - Record Type Name. 
* @return schema.RecordTypeInfo - returns Schema.RecordTypeInfo of any SObject. 
*/  

    public static schema.RecordTypeInfo getSobjectRecordTypeInfo(String sObjectApiName, String recordTypeName) {

        //Retrieve the describe result for the desired object
        DescribeSObjectResult result = Schema.getGlobalDescribe().get(sObjectApiName).getDescribe();

        //Generate a map of tokens for all the Record Types for the desired object
        Map<string, Schema.RecordTypeInfo>  recordTypeInfo = result.getRecordTypeInfosByName();
        
        return recordTypeInfo.get(recordTypeName);
    }

    public static boolean createChatterFeed(Id sobjectId, VisitPlanningModel.ApprovalStatus bodyMessage) {

        FeedItem feedPost   = new FeedItem();
        feedPost.ParentId   = sobjectId; //eg. sObjectId
        feedPost.Body       = '['+bodyMessage.status+'] - '+bodyMessage.comments;
        insert feedPost;
        return true;
    }

    public static List<selectOption> retreivePicklistValues(String object_name, String field_name) {

      Sobject sobj = Schema.getGlobalDescribe().get(object_name).newSObject();
      List<selectOption> options = new List<selectOption>();
      Schema.sObjectType sobject_type = sobj.getSObjectType(); //grab the sobject that was passed
      Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
      Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
      List<Schema.PicklistEntry> pick_list_values = field_map.get(field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
      for (Schema.PicklistEntry a : pick_list_values) { 
                  
            options.add(new selectOption(a.getValue(), a.getLabel()));
      }
      return options;
    }
}