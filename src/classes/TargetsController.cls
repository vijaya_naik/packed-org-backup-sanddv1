/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 24-Nov-2015
* @description This class is for TargetsMainPage
*/

public with sharing class TargetsController {
    
    public static Boolean allowTargetAcceptance {get; set;}
    public static Boolean allowAcceptedTargetsEdit {get; set;}
    public static Boolean allowTargetCreation {get; set;}
    public static Boolean allowSandBagging {get; set;}
    public static Boolean allowLowerTargets {get; set;}
    public static Integer digitLimit {get; set;}
    public static String  mainTargetID  {get;set;}
    
    public class myException extends Exception {}

    //Constructor
    public TargetsController() {

        allowTargetAcceptance = ConfigurationService.getAllowTargetAcceptance();
        allowAcceptedTargetsEdit = ConfigurationService.getAllowAcceptedTargetsEdit();
        allowTargetCreation = ConfigurationService.getAllowTargetCreation();
        allowSandBagging = ConfigurationService.getAllowSandBagging();
        allowLowerTargets = ConfigurationService.getAllowLowerTargets();
        digitLimit = ConfigurationService.getDigitLimit();

        mainTargetID = String.valueof(ApexPages.currentPage().getParameters().get('Id'));
    }
    
    //Method return minimum size of target list size for pagination 
    public static String gettargetViewSize(){
        return JSON.serialize(ConfigurationService.getTargetViewSize());
    }
    

    @RemoteAction
    public static Boolean allowTargetCreation() {
        return ConfigurationService.getAllowTargetCreation();
    }


    /*******************************************************************************************************
    * @description .Method to fetch Targets
    * @param <param name> 
    * @return <return type> .List of targets
    */
    @RemoteAction
    public static List<SandDV1__Target__c> getAllTargets() {    
        
        return TargetsService.fetchTargets();
    }

    /*******************************************************************************************************
    * @description .Method to fetch Periods based on a string
    * @param <param name> startWith
    * @return <return type> .List of periods
    */
    @RemoteAction
    public static List<SandDV1__Period__c> getPeriods(String startWith) {    
        
        return ServiceFacade.fetchPeriods(startWith);
    }

    /*******************************************************************************************************
    * @description .Method to fetch Periods
    * @param <param name> 
    * @return <return type> .List of periods
    */
    @RemoteAction
    public static List<SandDV1__Period__c> fetchPeriods() {
        
        return ServiceFacade.fetchPeriods();
    }

    //This method will bring Aspect values from ConstantsClass
    @RemoteAction
    public static Map<String,String> getAspectValues(){
        return ConfigurationService.getAspectValues();
    }

    /*******************************************************************************************************
    * @description This method will get sub periods and send a wrapper of Target,Target line item, Period Name
    * @param <param name> .
    * @return <return type> . 
    */
    
    public static TargetsDomain.TargetInfoWrapper createEmptyTargetData(String periodId){

        Map<Id,SandDV1__Period__c> periodMap                  = new Map<Id,SandDV1__Period__c> ();
        TargetsDomain.TargetInfoWrapper wrapTarget   = new TargetsDomain.TargetInfoWrapper();
        List<SandDV1__Period__c> periodList  = ServiceFacade.fetchPeriodsByMasterId(periodId);
        periodMap = ServiceFacade.periodListToMap(periodId);

        wrapTarget = TargetsService.createEmptyTargetInfo(periodList,periodMap,periodId);
        return wrapTarget;
    }
    

    /*******************************************************************************************************
    * @description This method will fetch target info if it exists or create a empty target data
    * @param <param name> .masterTargetId - targetId, periodId - period Id
    * @return <return type> . TargetInfoWrapper
    */
    @RemoteAction 
    public static TargetsDomain.TargetInfoWrapper fetchTargetInfo(String masterTargetId,String periodId) {
        
        TargetsDomain.TargetInfoWrapper wraptargetinst = new TargetsDomain.TargetInfoWrapper();

        if((masterTargetId == '' || masterTargetId == null) && (periodId != null && periodId != '')) {

            wraptargetinst = createEmptyTargetData(periodId);
        }
        else{
           
            
            wraptargetinst = TargetsService.fetchTargetData(masterTargetId, periodId);  
        }

        return wraptargetinst;
    }

    
    /*******************************************************************************************************
    * @description :Method to save target
    * @param :targetInstance - Target Obj, isDraft isAssigned isSuspended of boolean
    * @return :TargetMessageInfo
    */
    @RemoteAction
    public static TargetsDomain.TargetMessageInfo saveTarget(SandDV1__Target__c targetInstance, List<TargetsDomain.PeriodTLIWrapper> periodTLIWrapperList, 
        Boolean isDraft, Boolean isAssigned, Boolean isRevised, Boolean isAccepted) {

        
        return TargetsService.saveTarget(targetInstance, periodTLIWrapperList, isDraft, isAssigned, isRevised, isAccepted);

    }


    /*******************************************************************************************************
    * @description :Method to save All child target
    * @param :cTargetData of ChildTargetsWrapper type and isDraft isAssigned isSuspended of boolean
    * @return :ChildTargetsWrapper type 
    */
    @RemoteAction
    public static TargetsDomain.TargetMessageInfo saveAllTargets(List<TargetsDomain.TargetInfoWrapper> childTargetsList, 
        Boolean isDraft, Boolean isAssigned) {
        
        return TargetsService.saveAllTargets(childTargetsList, isDraft, isAssigned);
           
    }
    

    /*******************************************************************************************************
    * @description :Method to save child target of a target.
    * @param :cTargetData of ChildTargetsWrapper type and isDraft isAssigned isSuspended of boolean
    * @return :ChildTargetsWrapper type 
    */
    
    @RemoteAction
    public static TargetsDomain.TargetInfoWrapper saveChildTarget(TargetsDomain.TargetInfoWrapper cTargetData, 
        Boolean isDraft, Boolean isAssigned, Boolean isSuspended) { 
        
        return TargetsService.saveChildTarget(cTargetData, isDraft, isAssigned, isSuspended);
    }

    

    /*******************************************************************************************************
    * @description Method to delete a target
    * @param targetId- Target Id
    * @return null 
    */
    
    @RemoteAction
    public static void deleteTarget(String targetId) {


        if(targetId != null){

            TargetsService.deleteTarget(targetId);

        }
    }


    /*******************************************************************************************************
    * @description Method to check a target exists
    * @param periodId- period Id, aspect - aspect value
    * @return boolean 
    */
    
    @RemoteAction
    public static Boolean checkExistingTarget(String periodId,String aspect) {


        return TargetsService.checkExistingTarget(periodId,aspect);
    }
   
}