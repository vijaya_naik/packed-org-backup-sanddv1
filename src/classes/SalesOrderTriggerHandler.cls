/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 31-Oct-2015
* @description This is a helper class for SalesOrderTrigger.
*/
public with sharing class SalesOrderTriggerHandler {
    public SalesOrderTriggerHandler() {
        
    }

    private static SalesOrderTriggerHandler instance;

    
    public static SalesOrderTriggerHandler getInstance() {
        if (instance == null) {
            instance = new SalesOrderTriggerHandler();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description :-method to restrict user's to delete SandDV1__Sales_Order__c records. 
    * @param :- List<SandDV1__Sales_Order__c>.
    * @return :- void.
    */
    /*public void onDelete(List<SandDV1__Sales_Order__c> salesOrderList) {
        for(SandDV1__Sales_Order__c salesOrder : salesOrderList) {
            if(salesOrder.SandDV1__Status__c != System.Label.SalesOrderDraftStatus){
                salesOrder.adderror(+system.Label.Can_tDeleteRecordMessage);
            }
        }
    }*/
    public void onDelete(List<SandDV1__Sales_Order__c> salesOrderList) {
        Set<Id> orderIdSet = new Set<Id>();
        Set<String> statusSet = ConfigurationService.getStatusSet('SandDV1__Sales_Order__c','Allow_Delete');
        String ProfileId = UserInfo.getProfileId(); 
        String adminProfileId = [select Id,Name from Profile where Name=:'System Administrator'].Id ; 
        for(SandDV1__Sales_Order__c salesOrder : salesOrderList) { 
            if(!statusSet.contains(salesOrder.SandDV1__Status__c)){
            System.debug('show error');
                salesOrder.adderror(+system.Label.Can_tDeleteRecordMessage);
                return;
            }
            else {
                if(ProfileId  != adminProfileId) {
                    salesOrder.adderror(+system.Label.Can_tDeleteRecordMessage);
                    return;
                }
                else {
                    orderIdSet.add(salesOrder.Id);
                }
                
            }
            
        }
        if(!orderIdSet.isempty()) {
            fetchLineItemandDeleteSalesBenefit(orderIdSet);
        }
    }
    
    public void fetchLineItemandDeleteSalesBenefit(Set<Id> orderIdSet) {
        Set<Id> orderLineItemIdSet = new Set<Id>();
        for(SandDV1__Sales_Order_Line_Item__c orderLine : ServiceFacade.fetchOrderLineItemRecords(orderIdSet)) {
            orderLineItemIdSet.add(orderLine.Id);
        }
        if(!orderLineItemIdSet.isempty()) {
            ServiceFacade.deleteSalesBenefit(orderLineItemIdSet);
        }
    }

    /*******************************************************************************************************
    * @description This mehtod will be called to update sales order line item
    * @param Sales order id
    * @return  
    */
    public void updateSalesOrderLineItems(List<SandDV1__Sales_Order__c> salesOrderList){

        Set<id> soIds = new Set<id>();

        for(SandDV1__Sales_Order__c so:salesOrderList){

            if(so.SandDV1__Approval_Status__c == 'Approved' && so.SandDV1__New_Quantity__c > 0){

               soIds.add(so.id);  
            }
            
        }

        if(!soIds.isEmpty()){

            List<SandDV1__Sales_Order_Line_Item__c> soliList = [SELECT Id,SandDV1__Sales_Order__c,SandDV1__Quantity__c,SandDV1__New_Quantity__c,SandDV1__Sales_Price__c,SandDV1__Net_Price__c FROM SandDV1__Sales_Order_Line_Item__c 
                                                                                WHERE SandDV1__Sales_Order__c IN :soIds AND SandDV1__New_Quantity__c > 0];
            for(SandDV1__Sales_Order_Line_Item__c soli :soliList){

                System.debug('**********'+soli);
                soli.SandDV1__Quantity__c = soli.SandDV1__New_Quantity__c;
                soli.SandDV1__Net_Price__c = soli.SandDV1__Sales_Price__c * soli.SandDV1__Quantity__c;
                soli.SandDV1__New_Quantity__c = 0;
            }
            System.debug('***********Update list****************'+soliList);
            Update soliList;

        }
        
    }
}