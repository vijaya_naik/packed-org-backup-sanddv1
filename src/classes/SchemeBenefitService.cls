/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class SchemeBenefitService {

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Benefit records according to scheme benefit range and create a map
    * @param <param name> :- List<SandDV1__Scheme_Benefit_Range__c> schemeBenefitRangeList
    * @return :- Map<String, List<SandDV1__Scheme_Benefit__c>>
    */
    
    public static Map<String, List<SandDV1__Scheme_Benefit__c>> getSchemeBenefitsMap(List<SandDV1__Scheme_Benefit_Range__c> schemeBenefitRangeList){
        Map<String, List<SandDV1__Scheme_Benefit__c>> schemeBenefitRangeMap = new Map<String, List<SandDV1__Scheme_Benefit__c>>();
        
        for(SandDV1__Scheme_Benefit__c schemeBenefit :getSchemeBenefitList(schemeBenefitRangeList)){
            if(schemeBenefitRangeMap.containsKey(schemeBenefit.SandDV1__Scheme_Benefit_Range__c)){
                schemeBenefitRangeMap.get(schemeBenefit.SandDV1__Scheme_Benefit_Range__c).add(schemeBenefit);
            }
            else{
                schemeBenefitRangeMap.put(schemeBenefit.SandDV1__Scheme_Benefit_Range__c, new List<SandDV1__Scheme_Benefit__c>{schemeBenefit});
            }
        }
        
        return schemeBenefitRangeMap;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Benefit records according to scheme benefit range 
    * @param <param name> :- List<SandDV1__Scheme_Benefit_Range__c> schemeBenefitRangeList
    * @return :- List<SandDV1__Scheme_Benefit__c>
    */
    public static List<SandDV1__Scheme_Benefit__c> getSchemeBenefitList(List<SandDV1__Scheme_Benefit_Range__c> schemeBenefitRangeList){
        return new List<SandDV1__Scheme_Benefit__c>([SELECT Id, Name, SandDV1__Scheme_Benefit_Range__c,SandDV1__Scheme_Benefit_Range__r.SandDV1__Scheme__c,SandDV1__System_Product_Name__c,SandDV1__Level__c,SandDV1__Product__c,
            SandDV1__Type__c,SandDV1__Value__c,SandDV1__Product__r.Name, SandDV1__Range__c, SandDV1__Scheme_Name__c
            FROM SandDV1__Scheme_Benefit__c
            WHERE SandDV1__Scheme_Benefit_Range__c IN :schemeBenefitRangeList]);
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Benefit records
    * @param <param name> :- List<SchemeDomain.SchemeBenefitRangeWrapper> schemeBenefitRangeWrapperList, Map<String, List<SandDV1__Scheme_Benefit__c>> schemeBenefitsMap
    * @return :- List<SchemeDomain.SchemeBenefitsWrapperV1>
    */
    public static List<SchemeDomain.SchemeBenefitsWrapperV1> getSchemeBenefitsList(List<SchemeDomain.SchemeBenefitRangeWrapper> schemeBenefitRangeWrapperList, Map<String, List<SandDV1__Scheme_Benefit__c>> schemeBenefitsMap){
        List<SchemeDomain.SchemeBenefitsWrapperV1> schemeBenefitsWrapperList = new List<SchemeDomain.SchemeBenefitsWrapperV1>();

        for(SchemeDomain.SchemeBenefitRangeWrapper schemeBenefitRangeWrapper :schemeBenefitRangeWrapperList){
            SchemeDomain.SchemeBenefitsWrapperV1 schemeBenefitsWrapper = new SchemeDomain.SchemeBenefitsWrapperV1();
            for(SandDV1__Scheme_Benefit_Range__c schemeBenefitRange :schemeBenefitRangeWrapper.schemeBenefitRangeList){
                if(schemeBenefitsMap.containsKey(schemeBenefitRange.Id)){
                    schemeBenefitsWrapper.salesOrder = schemeBenefitRangeWrapper.salesOrder;
                    schemeBenefitsWrapper.salesOrderLineItem = schemeBenefitRangeWrapper.salesOrderLineItem;
                    schemeBenefitsWrapper.schemeBenefitsList.addAll(schemeBenefitsMap.get(schemeBenefitRange.Id));
                }
            }
            schemeBenefitsWrapperList.add(schemeBenefitsWrapper);
        }

        return schemeBenefitsWrapperList;
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Benefit records
    * @param <param name> :- Set<String> schemeBenefitsIdSet
    * @return :- List<SandDV1__Scheme_Benefit__c>
    */
    public static List<SandDV1__Scheme_Benefit__c> getSchemeBenefitsList(Set<String> schemeBenefitsIdSet){
        return new List<SandDV1__Scheme_Benefit__c>(
            [SELECT Id, Name, SandDV1__Scheme_Benefit_Range__c,SandDV1__System_Product_Name__c,SandDV1__Level__c,SandDV1__Product__c,
           SandDV1__Type__c,SandDV1__Value__c,SandDV1__Product__r.Name, SandDV1__Range__c, SandDV1__Scheme_Name__c
            FROM SandDV1__Scheme_Benefit__c
            WHERE Id IN :schemeBenefitsIdSet]
        );
    }

    /*******************************************************************************************************
    * @description :- Method to fetch Scheme Benefit records
    * @param <param name> :- Set<String> schemeBenefitsIdSet
    * @return :- Map<String, SandDV1__Scheme_Benefit__c>
    */
    public static Map<String, SandDV1__Scheme_Benefit__c> getSchemeBenefitsMap(Set<String> schemeBenefitsIdSet){
        return new Map<String, SandDV1__Scheme_Benefit__c>(getSchemeBenefitsList(schemeBenefitsIdSet));
    }

    /*******************************************************************************************************
    * @description :- 
    * @param <param name> :- SchemeDomain.SchemeBenefitsWrapperV1 schemeBenefitWrapperV1
    * @return :- SchemeDomain.SchemeBenefitsWrapperV2
    */
    public static SchemeDomain.SchemeBenefitsWrapperV2 convertSchemeBenefitWrapperToV2(SchemeDomain.SchemeBenefitsWrapperV1 schemeBenefitWrapperV1){
        SchemeDomain.SchemeBenefitsWrapperV2 schemeBenefitWrapperV2 = new SchemeDomain.SchemeBenefitsWrapperV2();

        schemeBenefitWrapperV2.salesOrder = schemeBenefitWrapperV1.salesOrder;
        schemeBenefitWrapperV2.salesOrderLineItem = schemeBenefitWrapperV1.salesOrderLineItem;
        for(SandDV1__Scheme_Benefit__c schemeBenefit :schemeBenefitWrapperV1.schemeBenefitsList){
            SchemeDomain.BenefitWrapper benefitWrapper = new SchemeDomain.BenefitWrapper();
            benefitWrapper.schemeBenefit = schemeBenefit;
            benefitWrapper.isSelected = false;
            benefitWrapper.isSelectedOld = false;
            schemeBenefitWrapperV2.schemeBenefitsList.add(benefitWrapper);
        }

        return schemeBenefitWrapperV2;
    }
    
    /*******************************************************************************************************
    * @description :- Method to fetch scheme benefit records according to scheme benefit range id
    * @param <param name> :- Set<Id> benfitRangeId
    * @return :- List<SandDV1__Scheme_Benefit__c>
    */
    public static List<SandDV1__Scheme_Benefit__c> fetchSchemeBenefit(Set<Id> benfitRangeId){
        return [Select Id,Name,SandDV1__Level__c,SandDV1__Scheme_Benefit_Range__r.SandDV1__Min__c,SandDV1__Scheme_Benefit_Range__r.SandDV1__Max__c,SandDV1__Product__c,SandDV1__Product__r.Name,SandDV1__Scheme_Benefit_Range__c,SandDV1__Type__c,SandDV1__Value__c from SandDV1__Scheme_Benefit__c where SandDV1__Scheme_Benefit_Range__c In: benfitRangeId];
    }
}