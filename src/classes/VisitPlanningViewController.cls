/*************************************************************************
* 
* EXTENTOR CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] Extentor Solutions Pvt. Ltd. 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of Extentor Solutions Pvt. Ltd and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to Extentor Solutions Pvt. Ltd
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from Extentor Solutions Pvt. Ltd.
*
*/

/**
* @author Extentor
* @date Aug 2015
* @description This class is controller for VisitPlanning Page.
*/

public with sharing class VisitPlanningViewController {
    
    public Id currentUserId {get; set;}
    public VisitPlanningViewController(ApexPages.StandardController con) {
        currentUserId                   = UserInfo.getUserId();
        Id userManagerId                = VisitPlanningService.getuserDetails(currentUserId).ManagerId;
    }
    
    public static List<Integer> dateStringList;
    public static Set<String> monthStringList; //set
    public static List<Integer> daysMonthList;
    public static List<VisitPlanningModel.TotalVisitPlannedWrapper> numberMap;
    
    private static Map<Integer, String> monthsMap = new Map<Integer, String>{1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April', 5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August', 9 => 'September', 10 => 'October', 11 => 'November', 12 => 'December'};
    private static Map<Integer, String> weekMap   = new Map<Integer, String>{1 => 'Sunday', 2 => 'Monday', 3 => 'Tuesday', 4 => 'Wednesday', 5 => 'Thursday', 6 => 'Friday', 7 => 'Saturday'};    
            
    /*******************************************************************************************************
* @description: Populate Period picklist in the VisitPlanning Page from VisitPlan.
* @return list<SandDV1__Visit_Plan__c> - returns list of Visit Plan. */
            
            @remoteAction
            public static SandDV1__Visit_Plan__c retreiveVisitPlan(Id VisitPlanId) {
                
                return VisitPlanningService.getCurrentVisitPlan(VisitPlanId);
            }
    
    
    /*******************************************************************************************************
* @description: Populate Route picklist in the VisitPlanning Page.
* @return list<SandDV1__Route__c> - returns list of Period. 
*/
    @remoteAction
    public static List<SandDV1__Route__c> retreiveRoutes() {
        
        return(VisitPlanningService.getRouteInfo());
    }


    @remoteAction
    public static List<selectOption> retreiveAccountCategory() {

        return (VisitPlanningService.retreivePicklistValues('Account', 'Account_Category__c'));
    }
    
    /*******************************************************************************************************
* @description: Get Accounts related to particular route.
* @param: PeriodId - Selected Period Value.
* @param: routeId  - Selected route Value.
* @param: AccountCategory - Selected Account Category.
* @param: visitPlanId - Selected VisitPlan Id.
* @return VisitPlanningModel.ResponseWrapper data. 
*/
    @remoteAction
    public static VisitPlanningModel.VisitPlanResponseWrapper retreiveAccounts(String periodId, String routeId, String AccountCategory, String visitPlanId) {
        
        
        VisitPlanningModel.VisitPlanResponseWrapper responseWrapperInstance;
        list<sObject> sobjectList;
        SandDV1__Period__c periodInstance;
        SandDV1__Visit_Plan__c visitPlanRes;
        List<Date> dateList                                         =  new List<Date>();
        dateStringList                                              =  new List<Integer>();
        map<date, boolean> dateValueMap                             =  new Map<date, boolean>();
        List<VisitPlanningModel.CustomerWrapper> customerList       =  new List<VisitPlanningModel.CustomerWrapper>();
        numberMap                                                   =  new List<VisitPlanningModel.TotalVisitPlannedWrapper>();
        List<VisitPlanningModel.VisitPlanWrapper> VisitPlanList     =  new List<VisitPlanningModel.VisitPlanWrapper>();
        monthStringList                                             =  new Set<String>();
        daysMonthList                                               =  new List<Integer>();
        Map<date, list<String>> holidaysMap                         =  new Map<date, list<String>>();
        Map<Id, list<SandDV1__Visit__c>> visitDetailsMap                     =  new Map<Id, list<SandDV1__Visit__c>>();
        Set<String> WeekendHolidaySet                               =  new Set<String>();
        Set<Id> accountIdSet                                        =  new set<Id>();
        List<VisitPlanningModel.VisitTargetWrapper> totalVisitTargetList         =  new List<VisitPlanningModel.VisitTargetWrapper>();
        Integer visitTargetVal                                                   =  0;


        if(periodId != Label.Select_Period && periodId != null && periodId != '') {
            
            //periodInstance = new SandDV1__Period__c();
            //periodInstance = VisitPlanningService.getActivePeriodInfo(Id.valueOf(periodId))[0];

            visitPlanRes  = new SandDV1__Visit_Plan__c();
            visitPlanRes  = VisitPlanningService.getCurrentVisitPlan(visitPlanId);
            if(visitPlanRes.SandDV1__Number_Of_Minimum_Visits__c != null) {
            visitTargetVal = Integer.valueOf(visitPlanRes.SandDV1__Number_Of_Minimum_Visits__c);
            }

            if(routeId != Label.Select_Route) {
                sobjectList = new list<SandDV1__Route_Account__c>();
                sobjectList = VisitPlanningService.getRouteAccountInfo(routeId, accountCategory);     
            } else {
                Id loggedInUserId = UserInfo.getUserId();
                if(((visitPlanRes.SandDV1__Status__c == 'Submitted' || visitPlanRes.SandDV1__Status__c == 'Approved') && (visitPlanRes.SandDV1__User__c == loggedInUserId)) || (visitPlanRes.SandDV1__User__c != loggedInUserId)){
                sobjectList = new list<SandDV1__Visit__c>();
                sobjectList = VisitPlanningService.getVisits(visitPlanId);
                    } else {
                sobjectList = new list<Account>();
                sobjectList = VisitPlanningService.getAccountInfo(accountCategory);
            }
            }
            }
            
            //Getting all the dates in between start & end date of selected Period.
            dateList = VisitPlanningService.getDatesBetween(visitPlanRes.SandDV1__Period__r.SandDV1__Start_Date__c, visitPlanRes.SandDV1__Period__r.SandDV1__End_Date__c);
            
            /* Getting all holidays in between start & end date of selected Period.*/
            for(Holiday h : VisitPlanningService.getHolidays(visitPlanRes.SandDV1__Period__r.SandDV1__Start_Date__c, visitPlanRes.SandDV1__Period__r.SandDV1__End_Date__c)) {
                if(holidaysMap.containsKey(h.ActivityDate)) {
                    holidaysMap.get(h.ActivityDate).add(h.Name);
                } else {
                    holidaysMap.put(h.ActivityDate, new list<String>());
                    holidaysMap.get(h.ActivityDate).add(h.Name);
                }
            }
            
            BusinessHours businessHoursIns = VisitPlanningService.getWeekend();
            if(businessHoursIns != null) {
                if(businessHoursIns.SundayStartTime == null && businessHoursIns.SundayEndTime == null) WeekendHolidaySet.add(weekMap.get(1));
                if(businessHoursIns.MondayStartTime == null && businessHoursIns.MondayEndTime == null) WeekendHolidaySet.add(weekMap.get(2));
                if(businessHoursIns.TuesdayStartTime == null && businessHoursIns.TuesdayEndTime == null) WeekendHolidaySet.add(weekMap.get(3));
                if(businessHoursIns.WednesdayStartTime == null && businessHoursIns.WednesdayEndTime == null) WeekendHolidaySet.add(weekMap.get(4));
                if(businessHoursIns.ThursdayStartTime == null && businessHoursIns.ThursdayEndTime == null) WeekendHolidaySet.add(weekMap.get(5));
                if(businessHoursIns.FridayStartTime == null && businessHoursIns.FridayEndTime == null) WeekendHolidaySet.add(weekMap.get(6));
                if(businessHoursIns.SaturdayStartTime == null && businessHoursIns.SaturdayEndTime == null) WeekendHolidaySet.add(weekMap.get(7));
            }
            
            //Getting all the Visits related to the selected Visit Plan.
            
            for(SandDV1__Visit__c visit : VisitPlanningService.getPlannedVisits(visitPlanId)) {
                
                if(visitDetailsMap.containsKey(visit.SandDV1__Account__c)) {
                    visitDetailsMap.get(visit.SandDV1__Account__c).add(visit);
                } else {
                    visitDetailsMap.put(visit.SandDV1__Account__c, new list<SandDV1__Visit__c>());
                    visitDetailsMap.get(visit.SandDV1__Account__c).add(visit);
                    
                }
            }
            
            integer counter = 0;
            integer counterVal =0;
            integer oldMonthVal;
            integer oldYearVal;
            integer newMonthVal;
            integer newYearVal;
            
            for(Date d : dateList) {
                
                String datetoString = d.format(); 
                newMonthVal = d.Month();
                newYearVal  = d.year();
                dateStringList.add(d.Day()); //datetoString
                Boolean isHoliday = holidaysMap.containsKey(d);
                //numberMap.add(new VisitPlanningModel.TotalVisitPlannedWrapper(counter, 0, isHoliday));

               
                
                Datetime dt = DateTime.newInstance(d, Time.newInstance(0, 0, 0, 0));
                String dayOfWeek = dt.format('EEEE');
                if(WeekendHolidaySet.contains(dayOfWeek)) {
                    if(isHoliday) {
                        holidaysMap.get(d).add(dayOfWeek);
                    } else {
                        holidaysMap.put(d, new list<String>());
                        holidaysMap.get(d).add(dayOfWeek);
                    }
                }

                  isHoliday = holidaysMap.containsKey(d);
                  numberMap.add(new VisitPlanningModel.TotalVisitPlannedWrapper(counter, 0, isHoliday));
                  totalVisitTargetList.add(new VisitPlanningModel.VisitTargetWrapper(counter, visitTargetVal, isHoliday));

                // if(!isHoliday) {
                   
                //    totalVisitTargetList.add(new VisitPlanningModel.VisitTargetWrapper(counter, visitTargetVal, isHoliday));
                //} else {

                //    totalVisitTargetList.add(new VisitPlanningModel.VisitTargetWrapper(counter, 0, isHoliday));
                //}

                counter++;


                
                if(newMonthVal != oldMonthVal) {
                    if(counterVal != 0)  daysMonthList.add(counterVal);
                    CounterVal = 1;
                    monthStringList.add(monthsMap.get(newMonthVal)+' - '+newYearVal);
                    
                    oldMonthVal = newMonthVal;
                    oldYearVal  = newYearVal;
                } else {
                    counterVal++;
                }
            }
            daysMonthList.add(counter);
            
            string sObjectTypeValue = string.valueOf(sobjectList.getSobjectType());
            for(sObject objInstance : sobjectList) {
                
                if(sObjectTypeValue == 'Account') {
                    Account acc = new Account();
                    acc = (Account)objInstance;
                    
                    VisitPlanningModel.CustomerWrapper customerInfo = new VisitPlanningModel.CustomerWrapper(acc.Name, acc.Id, acc.SandDV1__Account_Category__c, acc.SandDV1__Credit_Limit__c);
                    customerInfo.visitPlanWrapperList         =  getPopulateDates(dateList, holidaysMap, acc.Id, visitDetailsMap);
                    //VisitPlanList.clone();
                    customerList.add(customerInfo);
                    
                } else if(sObjectTypeValue == 'SandDV1__Route_Account__c') {
                    SandDV1__Route_Account__c routeAccount = new SandDV1__Route_Account__c();
                    routeAccount = (SandDV1__Route_Account__c)objInstance;
                    VisitPlanningModel.CustomerWrapper customerInfo = new VisitPlanningModel.CustomerWrapper(routeAccount.SandDV1__Account__r.Name, routeAccount.SandDV1__Account__c, routeAccount.SandDV1__Account__r.SandDV1__Account_Category__c, routeAccount.SandDV1__Account__r.SandDV1__Credit_Limit__c);
                    customerInfo.visitPlanWrapperList         =  getPopulateDates(dateList, holidaysMap, routeAccount.SandDV1__Account__c, visitDetailsMap);
                    customerList.add(customerInfo);
                } else if(sObjectTypeValue == 'SandDV1__Visit__c') {
                    
                    SandDV1__Visit__c visitInstance = new SandDV1__Visit__c();
                    visitInstance = (SandDV1__Visit__c)objInstance;
                    if(!accountIdSet.contains(visitInstance.SandDV1__Account__c)) {
                    VisitPlanningModel.CustomerWrapper customerInfo = new VisitPlanningModel.CustomerWrapper(visitInstance.SandDV1__Account__r.Name, visitInstance.SandDV1__Account__c, visitInstance.SandDV1__Account__r.SandDV1__Account_Category__c, visitInstance.SandDV1__Account__r.SandDV1__Credit_Limit__c);
                    customerInfo.visitPlanWrapperList         =  getPopulateDates(dateList, holidaysMap, visitInstance.SandDV1__Account__c, visitDetailsMap);
                    customerList.add(customerInfo);
                    accountIdSet.add(visitInstance.SandDV1__Account__c);
                }
                }
            }
        
        
        responseWrapperInstance                                     =  new VisitPlanningModel.VisitPlanResponseWrapper();
        responseWrapperInstance.periodValues.tableName              =  'Days';
        responseWrapperInstance.periodValues.dayOfMonth             =  dateStringList;
        responseWrapperInstance.periodValues.monthWithYear          =  new List<String>(monthStringList);
        responseWrapperInstance.periodValues.numberOfDaysInMonth    =  daysMonthList;
        responseWrapperInstance.totalVisitValues                    =  numberMap;
        responseWrapperInstance.visitTargetValues                   =  totalVisitTargetList;
        responseWrapperInstance.customerValues                      =  customerList;
        
        return responseWrapperInstance;
    }
    
    public static list<VisitPlanningModel.VisitPlanWrapper> getPopulateDates(list<Date> dateList, map<date, list<String>> holidaysMap, Id accountId, map<id, list<SandDV1__Visit__c>> visitDetailsMap) {
        
        integer counter = 0;
        integer oldMonthVal;
        integer oldYearVal;
        integer newMonthVal;
        integer newYearVal;
        list<VisitPlanningModel.VisitPlanWrapper> VisitPlanList     =  new list<VisitPlanningModel.VisitPlanWrapper>();
        
        //list<SandDV1__Visit__c> visitList = new list<SandDV1__Visit__c>();
        map<Date, SandDV1__Visit__c> visitDateMap = new map<Date, SandDV1__Visit__c>();
        if(visitDetailsMap.containsKey(accountId)) {
            //visitList =   
            for(SandDV1__Visit__c visit : visitDetailsMap.get(accountId)) {
                visitDateMap.put(visit.SandDV1__Visit_Plan_Date__c, visit);
            }
        }
        
        for(Date d : dateList) {
            
            String datetoString = d.format();
            newMonthVal = d.Month();
            newYearVal  = d.year();
            SandDV1__Visit__c visitData = new SandDV1__Visit__c();
            visitData = visitDateMap.get(d);
            string visitId = '';
            if(visitData != null) {
                visitId = visitData.Id;
            } else {
                visitId = null;
            }
            
            if(newMonthVal != oldMonthVal) {
                monthStringList.add(monthsMap.get(newMonthVal)+' - '+newYearVal);       
                oldMonthVal = newMonthVal;
                oldYearVal  = newYearVal;
            }
            
            Boolean holidayFlag  = holidaysMap.containsKey(d) ? true : false;
            Boolean isVisitedFlag = (!visitDateMap.isEmpty() && visitData != null) ? true : false;
            list<String> holidayReason = new list<String>();
            holidayReason = holidayFlag ? holidaysMap.get(d) : null;
            if(isVisitedFlag) {
                numberMap[counter].checkedRowCount += 1;
            }
            counter++;
            VisitPlanList.add(new VisitPlanningModel.VisitPlanWrapper(isVisitedFlag, holidayFlag, holidayReason, datetoString, visitId)); 
        }
        return VisitPlanList;       
    }
    
    
    @remoteAction
    public static SandDV1__Visit_Plan__c saveVisitPlan(VisitPlanningModel.VisitPlanResponseWrapper visitPlanRes, String visitPlanId, String Status, List<String> removeVisitIdList) {
        
        List<SandDV1__Visit__c> visitsToDelete = new List<SandDV1__Visit__c>();
        for(String visitId : removeVisitIdList) {
            visitsToDelete.add(new SandDV1__Visit__c(Id=visitId));
        }
        
        if(visitsToDelete.size() > 0) {
            delete visitsToDelete;
        }
        
        VisitPlanningModel.VisitPlanResponseWrapper res = visitPlanRes;
        //(VisitPlanningModel.VisitPlanResponseWrapper) JSON.deserialize(visitPlanRes, VisitPlanningModel.VisitPlanResponseWrapper.class);
        
        Id currentUserId                = userinfo.getUserId();
        list<SandDV1__Visit__c> VisitList        = new list<SandDV1__Visit__c>();
        SandDV1__Visit_Plan__c visitPlanInstance = new SandDV1__Visit_Plan__c();
        visitPlanInstance.Id            = visitPlanId;
        visitPlanInstance.SandDV1__Status__c     = (status == 'Planning' ? 'Planning & Inprogress' : status);        
        Id userManagerId                = VisitPlanningService.getuserDetails(currentUserId).ManagerId;
        Id visitRecordTypeId            = VisitPlanningService.getSobjectRecordTypeInfo('SandDV1__Visit__c', 'Check In').getRecordTypeId();
        Boolean isSucessFlag            = false;
        
        if(userManagerId != null || Status == 'Planning') {
            
            for(VisitPlanningModel.CustomerWrapper cusWrap : res.customerValues) { 
                for(VisitPlanningModel.VisitPlanWrapper visitWrap : cusWrap.visitPlanWrapperList) {
                    if(visitWrap.isVisitPlanned) {
                        SandDV1__Visit__c visitInstance            =  new SandDV1__Visit__c();
                        visitInstance.put('Id', visitWrap.VisitId);
                        visitInstance.put('SandDV1__Account__c', cusWrap.customerId);
                        visitInstance.put('recordTypeId', visitRecordTypeId);
                        visitInstance.put('SandDV1__Type__c', 'Planned');
                        if(visitWrap.VisitId == null) {
                            visitInstance.put('SandDV1__Visit_Plan__c', visitPlanId);
                        }
                        visitInstance.put('SandDV1__Visit_Plan_Date__c', Date.parse(visitWrap.dateValue));  //dateList[counter]
                        VisitList.add(visitInstance);
                    }
                }
            }
            
            upsert VisitList;
            update visitPlanInstance;
            isSucessFlag = true;
        } else {
            isSucessFlag = false;
            throw new VisitPlanningException(Label.Manager_Not_Defined);  
        }
        return visitPlanInstance;
    }

    @remoteAction
    public static boolean visitPlanApprovalRejection(VisitPlanningModel.ApprovalStatus visitAppValue, string visitPlanId) {
        
        ProcessInstance ProcessInstanceObj = [SELECT Id, Status,TargetObjectId FROM ProcessInstance WHERE TargetObjectId =: visitPlanId and Status = 'Pending' limit 1];
        ProcessInstanceWorkitem processInstanceWorkItemObj = [SELECT Id,ProcessInstanceId FROM ProcessInstanceWorkitem where ProcessInstanceId =: ProcessInstanceObj.Id limit 1];
        Approval.ProcessWorkitemRequest appReq = new Approval.ProcessWorkitemRequest();
        appReq.setComments(visitAppValue.comments);
        appReq.setAction(visitAppValue.status); //This is the action that is approve in your case, you can set it to Reject also
        //appReq.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        // Use the ID from the newly created item to specify the item to be worked  
    
        appReq.setWorkitemId(processInstanceWorkItemObj.Id);

        // Submit the request for approval  
            
        Approval.ProcessResult result2 =  Approval.process(appReq);
        //VisitPlanningService.createChatterFeed(Id.valueOf(visitPlanId), visitAppValue);
          return true;
    }   
}