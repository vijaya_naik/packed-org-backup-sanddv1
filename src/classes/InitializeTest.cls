/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs....
*
*/
public with sharing class InitializeTest {
    public InitializeTest() {
        
    }

    /*******************************************************************************************************
    * @description :- create dummy user.
    * @param :- string .
    * @return :- User. 
    */
    public static User createUser(string dummyusername) {
        String user_id = userinfo.getUserId();
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        
        User dummyUser = new User(Alias = 'abcuser', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_IN', ProfileId = p.Id,TimeZoneSidKey='Asia/Kolkata', 
                          UserName=dummyusername);
        return dummyUser;
    }

    /*******************************************************************************************************
    * @description :- create dummy Group.
    * @param :- String,String .
    * @return :- Group. 
    */
    public static Group createGroup(string groupname,String bucketID) {
        Group grp = new Group(Type='Regular',Name=groupname,DeveloperName=bucketID);
        return grp;
    }

    /*******************************************************************************************************
    * @description :- create dummy Product.
    * @param :- null .
    * @return :- Product2. 
    */
    public static Product2 createProduct() {
        
        Product2 prod = new Product2(Name = 'Laptop X200',Family = 'Hardware');
        return prod;
    }

    /*******************************************************************************************************
    * @description :- create dummy Product.
    * @param :- null .
    * @return :- Pricebook2. 
    */
    public static Pricebook2 createPricebook() {
        Pricebook2 pricebookObj = new Pricebook2(Name='Custom Pricebook', isActive=true);
        return pricebookObj;
    }


    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Bucket__c .
    * @param :- Boolean,String,Boolean .
    * @return :- SandDV1__Bucket__c. 
    */
    public static SandDV1__Bucket__c createBucket(Boolean bucketactive,String parentid,Boolean futurecall) {
        SandDV1__Bucket__c bucket = new SandDV1__Bucket__c(SandDV1__Access_Level__c='Edit',SandDV1__Active__c=bucketactive,SandDV1__Parent__c=parentid,
            SandDV1__System_Future_Call__c=futurecall,SandDV1__System_RootId__c='',SandDV1__System_Group_ID__c='',SandDV1__System_Route_Path__c='',SandDV1__System_Level_Index__c=0);
        return bucket;
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__User_Bucket_Member__c .
    * @param :- String,String,Boolean .
    * @return :- SandDV1__User_Bucket_Member__c. 
    */
    public static SandDV1__User_Bucket_Member__c createUserBucketMember(String userid,String bucketid,Boolean memberactive) {
        SandDV1__User_Bucket_Member__c userbucket = new SandDV1__User_Bucket_Member__c(SandDV1__User__c= userid,SandDV1__Bucket__c=bucketid
            ,SandDV1__System_Active__c = memberactive);
        return userbucket;
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__System_Object_Bucket__c .
    * @param :- String,String .
    * @return :- SandDV1__System_Object_Bucket__c. 
    */
    public static SandDV1__System_Object_Bucket__c createObjectBucket(String name,String parentId,String bucketId) {
        SandDV1__System_Object_Bucket__c objectbucket = new SandDV1__System_Object_Bucket__c(Name=name,SandDV1__Parent_Id__c = parentId,
            SandDV1__Bucket__c=bucketId);
        return objectbucket;
    }

    /*******************************************************************************************************
    * @description :- create dummy Account .
    * @param :- no .
    * @return :- Account. 
    */
    public static Account createAccount() {
        Account account = new Account(Name = 'testaccount');
        return account;
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Period__c .
    * @param :- Boolean .
    * @return :- SandDV1__Period__c. 
    */
    public static SandDV1__Period__c createPeriod(Boolean active) {
        SandDV1__Period__c period = new SandDV1__Period__c(Name = 'testperiod',SandDV1__End_Date__c=System.today()+10,
            SandDV1__Start_Date__c = system.today(),SandDV1__Active__c=active);
        return period;
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Period__c .
    * @param :- Integer,Boolean .
    * @return :- SandDV1__Period__c. 
    */
    public static SandDV1__Period__c createTargetPeriod(Integer i,Boolean active) {
        Integer enddate = i+1;
        SandDV1__Period__c period = new SandDV1__Period__c(Name = 'Period'+i,SandDV1__End_Date__c=System.today()+enddate,
            SandDV1__Start_Date__c = system.today(),SandDV1__Active__c=active);
        return period;
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Target__c .
    * @param :- String,Boolean,String,Id.
    * @return :- SandDV1__Target__c. 
    */
    public static SandDV1__Target__c createTarget(String periodId,Boolean active,String status,Id assignedto) {
        SandDV1__Target__c target = new SandDV1__Target__c(SandDV1__Period__c=periodId,SandDV1__Active__c=active,SandDV1__Status__c = status,
            SandDV1__Assigned_To__c = assignedto);
        return target;
    }

    /*******************************************************************************************************
    * @description :- create dummy Target__Share .
    * @param :- Id,Id,String.
    * @return :- Target__Share. 
    */
    public static Target__Share createTargetShare(Id parentId,Id assignedto,String accesslevel) {
        Target__Share trgtShare = new Target__Share(ParentId= parentId,UserOrGroupId=assignedto,
            AccessLevel = accesslevel,
            RowCause = Schema.Target__Share.RowCause.SandDV1__Target_Assignment_Access__c);
        return trgtShare;        
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Sales_Order__c .
    * @param :- Id,String.
    * @return :- SandDV1__Sales_Order__c. 
    */
    public static SandDV1__Sales_Order__c createSalesOrder(Id buyerId,String status) {
        SandDV1__Sales_Order__c salesorder = new SandDV1__Sales_Order__c();
            salesorder.SandDV1__Buyer__c = buyerId;
            salesorder.SandDV1__Date__c = System.today();
            salesorder.SandDV1__No_of_Parts__c = 10;
            //salesorder.SandDV1__Order_Amount__c = 100.00;
            salesorder.SandDV1__PO_Reference__c = 'poo';
            salesorder.SandDV1__PO_Status__c = 'postatus';
            salesorder.SandDV1__Scheme_Amount__c = 100.90;
            salesorder.SandDV1__Status__c = status;
            //salesorder.Name = 'sales1';
            salesorder.SandDV1__Shipping_City__c = 'shipcity';
            salesorder.SandDV1__Shipping_Country__c = 'ship country';
            salesorder.SandDV1__Shipping_State_Province__c = 'state';
            salesorder.SandDV1__Shipping_Street__c = 'street';
            salesorder.SandDV1__Shipping_Zip_Postal_Code__c = '8977';
            salesorder.SandDV1__Shipping_Address_Name__c = 'name';
        return salesorder;        
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Sales_Order_Line_Item__c .
    * @param :- Id,Id,String.
    * @return :- SandDV1__Sales_Order_Line_Item__c. 
    */
    public static SandDV1__Sales_Order_Line_Item__c createSalesOrderLineItem(Id salesOrderId,Id prodId,Integer fullQnantity,Id pricebookID,
                                Integer quan,Integer i) {
        SandDV1__Sales_Order_Line_Item__c salesOLI = new SandDV1__Sales_Order_Line_Item__c();
            salesOLI.SandDV1__Fulfilled_Quantity__c = fullQnantity;
            salesOLI.SandDV1__Part__c = prodId;
            salesOLI.SandDV1__Price_Book__c = pricebookID;
            salesOLI.SandDV1__Quantity__c = quan;
            salesOLI.SandDV1__Sales_Order__c = salesOrderId;
            salesOLI.SandDV1__Total_Price__c = 100;
            salesOLI.SandDV1__Type__c = 'type';
            salesOLI.SandDV1__Unit_Price__c = 100;
            salesOLI.SandDV1__UOM__c = 'Meter';
            
        return salesOLI;        
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Delivery_Order__c .
    * @param :- Id,String.
    * @return :- SandDV1__Delivery_Order__c. 
    */
    public static SandDV1__Delivery_Order__c createDeliveryOrder(Id buyerId,Id salesOrderID,String status) {
        SandDV1__Delivery_Order__c deliveryOrder = new SandDV1__Delivery_Order__c();
            deliveryOrder.SandDV1__Buyer__c = buyerId;
            deliveryOrder.SandDV1__Date__c = System.today();
            deliveryOrder.SandDV1__No_of_Parts__c = 10;
            //deliveryOrder.SandDV1__Order_Amount__c = 100.00;
            deliveryOrder.SandDV1__PO_Reference__c = 'poo';
            deliveryOrder.SandDV1__PO_Status__c = 'postatus';
            deliveryOrder.SandDV1__Sales_Order__c = salesOrderID;
            deliveryOrder.SandDV1__Status__c = status;
            deliveryOrder.SandDV1__Shipping_City__c = 'shipcity';
            deliveryOrder.SandDV1__Shipping_Country__c = 'ship country';
            deliveryOrder.SandDV1__Shipping_State_Province__c = 'state';
            deliveryOrder.SandDV1__Shipping_Street__c = 'street';
            deliveryOrder.SandDV1__Shipping_Zip_Postal_Code__c = '8977';
            deliveryOrder.SandDV1__Shipping_Address_Name__c = 'name';
        return deliveryOrder;        
    }

    /*******************************************************************************************************
    * @description :- create dummy SandDV1__Delivery_Order_Line_Item__c .
    * @param :- Id,Id,String.
    * @return :- SandDV1__Delivery_Order_Line_Item__c. 
    */
    public static SandDV1__Delivery_Order_Line_Item__c createDeliveryOrderLineItem(Id deliveryOID,Id prodId,Id pricebookID,Id salesOLIID,
                                                        Integer quan,Integer i) {
        SandDV1__Delivery_Order_Line_Item__c deliveryOLI = new SandDV1__Delivery_Order_Line_Item__c();
            deliveryOLI.SandDV1__Delivery_Order__c = deliveryOID;
            deliveryOLI.SandDV1__Part__c = prodId;
            deliveryOLI.SandDV1__Price_Book__c = pricebookID;
            deliveryOLI.SandDV1__Quantity__c = quan;
            deliveryOLI.SandDV1__Sales_Price__c = 100;
            deliveryOLI.SandDV1__System_SalesOrderLine_Item__c = salesOLIID;
            deliveryOLI.SandDV1__Unit_Price__c = 100;
            deliveryOLI.SandDV1__UOM__c = 'Meter';
            deliveryOLI.SandDV1__Net_Amount__c = deliveryOLI.SandDV1__Quantity__c * deliveryOLI.SandDV1__Sales_Price__c;
            
        return deliveryOLI;        
    }

    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__Scheme__c createScheme(String schemename,String status,Integer i) {
        SandDV1__Scheme__c schemeObj = new SandDV1__Scheme__c();
            schemeObj.SandDV1__Available_Balance__c = 100.90;
            schemeObj.SandDV1__Budget_Limit__c = 100.90;
            schemeObj.SandDV1__Description__c = 'description';
            schemeObj.SandDV1__Expires_On__c = System.today() + 10;
            schemeObj.SandDV1__Scheme_Name__c = schemename+i;
            schemeObj.SandDV1__Status__c = status;
            schemeObj.SandDV1__Total_Orders_Count__c = 10;
            schemeObj.SandDV1__Type__c = 'Scheme Type 1';
            schemeObj.SandDV1__Usage_Count__c = 100;
            schemeObj.SandDV1__Valid_From__c = System.today();

        return schemeObj;
    }

    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__Scheme_Benefit_Range__c createSchemeBenefitRange(Id schemeId,Decimal max,Decimal min) {
        SandDV1__Scheme_Benefit_Range__c schemeBenefitRangeObj = new SandDV1__Scheme_Benefit_Range__c();
            schemeBenefitRangeObj.SandDV1__Max__c = max;
            schemeBenefitRangeObj.SandDV1__Min__c = min;
            schemeBenefitRangeObj.SandDV1__Scheme__c = schemeId;

        return schemeBenefitRangeObj;
    }

    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__Scheme_Benefit__c createSchemeBenefit(Id productId,Integer quantity,Id schemeBenefitRangeId,String type) {
        SandDV1__Scheme_Benefit__c schemeBenefitObj = new SandDV1__Scheme_Benefit__c();
            schemeBenefitObj.SandDV1__Level__c = 'Line Item';
            schemeBenefitObj.SandDV1__Product__c = productId;
            //schemeBenefitObj.SandDV1__Quantity__c = quantity;
            schemeBenefitObj.SandDV1__Scheme_Benefit_Range__c = schemeBenefitRangeId;
            schemeBenefitObj.SandDV1__Type__c = type;
            schemeBenefitObj.SandDV1__Value__c = 100.90;
            
        return schemeBenefitObj;
    }

   /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__Sales_Order_Item_Benefit__c createSalesOrderBenefit(Id salesorderLIId,Id schemeBenefitId,String orderbenefitcombi) {
        SandDV1__Sales_Order_Item_Benefit__c salesBenefitObj = new SandDV1__Sales_Order_Item_Benefit__c();
            salesBenefitObj.SandDV1__Order_Item_Benefit_Combination__c = orderbenefitcombi;
            salesBenefitObj.SandDV1__Sales_Order_Line_Item__c = salesorderLIId;
            salesBenefitObj.SandDV1__Scheme_Benefit__c = schemeBenefitId;
            
        return salesBenefitObj;
    }

    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__Scheme_Condition__c createSchemeCondition(String fieldApi,String objectApi,String value,Id schemeId,String parameter,String schemeType) {
        SandDV1__Scheme_Condition__c schemeCondition = new SandDV1__Scheme_Condition__c();
            schemeCondition.SandDV1__FieldApi__c = fieldApi;
            schemeCondition.SandDV1__ObjectApi__c = objectApi;
            schemeCondition.SandDV1__Value__c = value;
            schemeCondition.SandDV1__Scheme__c = schemeId;
            schemeCondition.SandDV1__Parameter__c = parameter;
            schemeCondition.SandDV1__System_Scheme_Type__c = schemeType;
            
        return schemeCondition;
    }
    
    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__OnBoarding_Document__c createOnBoardingDocument(String accountId,String status,String customId) {
        SandDV1__OnBoarding_Document__c doc = new SandDV1__OnBoarding_Document__c();
            doc.Name= 'name';
            doc.SandDV1__AccountId__c=  accountId;
            doc.SandDV1__Comments__c = 'Comments';
            doc.SandDV1__Description__c= 'description';
            doc.SandDV1__Is_Mandatory__c = true;
            doc.SandDV1__Status__c = status;
            doc.SandDV1__System_Custom_Setting_ID__c = customId;
            doc.SandDV1__Type__c = 'type';
            
        return doc;
    }
    
    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__OnBoardingDocumentSetting__c createOnBoardingDocumentSetting(String type,Boolean mandatory,String description,String name) {
        SandDV1__OnBoardingDocumentSetting__c docSetting = new SandDV1__OnBoardingDocumentSetting__c();
            docSetting.SandDV1__Is_Mandatory__c = mandatory;
            docSetting.SandDV1__Type__c =  type;
            docSetting.SandDV1__Description__c= description;
            docSetting.Name = name;
          
            
        return docSetting;
    }
    
    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static SandDV1__OnBoardingStatusColor__c createOnBoardingStatusColor(String Name,String ColorCode) {
        SandDV1__OnBoardingStatusColor__c colorsetting = new SandDV1__OnBoardingStatusColor__c();
            colorsetting.Name = Name;
            colorsetting.SandDV1__Color_Code__c =  ColorCode;         
        return colorsetting;
    }
    
    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static Attachment createAttachment(String parentId,String body) {
        Attachment attachObj = new Attachment ();
            attachObj.ParentId = parentId;
            attachObj.Body =  blob.valueof(body);
            attachObj.Name = 'dummy';
        return attachObj;
    }
    
    
    /*******************************************************************************************************
    * @description .
    * @param <param name> .
    * @return <return type> . 
    */
    public static FeedItem createFeed(Id ParentId) {
        FeedItem post = new FeedItem();
        post.ParentId = ParentId;
        post.Body = 'This is Test Feed';
        return post;
    }


}