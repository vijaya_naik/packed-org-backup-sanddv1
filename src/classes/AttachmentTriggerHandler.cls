/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 12-11-2015
* @description This is a Trigger on SandDV1__OnBoarding_Document__c.
*/
public with sharing class AttachmentTriggerHandler {
    public AttachmentTriggerHandler() {
        
    }
    private static AttachmentTriggerHandler instance;

    
    public static AttachmentTriggerHandler getInstance() {
        if (instance == null) {
            instance = new AttachmentTriggerHandler();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description :- on delete of attachment check parent id belongs to onboarding doc and if onboarding 
                        doc status == aproved then don't allow delete.
    * @param :- List<Attachment> oldObjects
    * @return :- void.
    */
    public void onBeforeDelete(List<Attachment> oldObjects) {
        Map<Id,List<Attachment>> attachmentMap = new Map<Id,List<Attachment>>();
        for(Attachment attach : oldObjects) {
            if(attach.ParentId.getSObjectType() == SandDV1__OnBoarding_Document__c.SobjectType) {
                if(attachmentMap.containskey(attach.ParentId)) {
                    attachmentMap.get(attach.ParentId).add(attach);
                }
                else {
                    List<Attachment> attachList = new List<Attachment>();
                    attachList.add(attach);
                    attachmentMap.put(attach.ParentId,attachList);
                }
            }
         }

            List<SandDV1__OnBoarding_Document__c> docList = OnBoardingService.fetchOnBoardingDocuments(attachmentMap.keyset());
            for(SandDV1__OnBoarding_Document__c doc : docList) {
                if(doc.SandDV1__Status__c == ConfigurationService.OnBoardingApprovedStatus) {
                    for(Attachment attachObj :attachmentMap.get(doc.Id)) {
                        attachObj.adderror(+system.Label.Can_tDeleteRecordMessage);
                    }
                }
            }
    }

}