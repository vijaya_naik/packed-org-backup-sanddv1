/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/
public with sharing class DeliveryOrderDomain {
    
    public class salesOrderWrapper{
        public String salesOrderDate;
        public String salesAddress;
        public Boolean isAdd;
        public SandDV1__Sales_Order__c salesOrderObj;
        public List<SandDV1__Sales_Order_Line_Item__c> orderLI;

        public salesOrderWrapper(){
            isAdd = false;
        }
    }

    public class deliveryOrderWrapper{
        public String deliveryOrderDate;
        public String deliveryAddress;
        public Boolean isAdd;
        public SandDV1__Delivery_Order__c deliverOrderObj;
        public List<SandDV1__Delivery_Order_Line_Item__c> deliveryOrderLI;
        public List<SandDV1__Sales_Order_Line_Item__c> salesOrderLI;

        public deliveryOrderWrapper(){
            isAdd = false;
        }
    }

   /* public class orderWrapper{
        public String OrderDate;
        public Boolean isAdd;
        public sobject orderObj;
        public List<sobject> orderLineItem;

        public orderWrapper(){
            isAdd = false;
        }
    }*/


}