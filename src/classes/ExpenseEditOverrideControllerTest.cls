/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 31-May-2016
* @description This is a Test class of ExpenseEditOverrideController
*/

@isTest(seeAllData = false)
private class ExpenseEditOverrideControllerTest {
    
    static UserRole testRole;
    static User sysAdmin;
    static User testUser;
    static SandDV1__Expense__c testExpense;

    static void init() {

        testRole = Initial_Test_Data.createRole('Parent role', null);
        insert testRole;
        System.AssertEquals(testRole.Name,'Parent role');

        sysAdmin = Initial_Test_Data.createUser('Super','User','System Administrator',testRole.Id);
        insert sysAdmin;

        testUser = Initial_Test_Data.createUser('Bryan','Adams','System Administrator',testRole.Id);
        insert testUser;

        System.runAs(sysAdmin){
            
            testExpense = Initial_Test_Data.createExpense('Test Expense',testUser.Id,'Draft');
            insert testExpense;


        }
    }
    @isTest static void test_method_one() {
        
        init();

        System.runAs(testUser){
            Test.startTest();

            ApexPages.StandardController sc = new ApexPages.StandardController(testExpense);
            ApexPages.currentPage().getParameters().put('retURL','/'+testExpense.Id);
            ApexPages.currentPage().getParameters().put('id',testExpense.Id);
            ExpenseEditOverrideController trc = new ExpenseEditOverrideController(sc); 
            System.AssertEquals(trc.eStatus, testExpense.SandDV1__Status__c);
            Test.stopTest();  
        } 
    }
    
}