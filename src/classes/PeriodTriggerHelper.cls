/*************************************************************************
* 
* ET Marlabs CONFIDENTIAL
* __________________
* 
*  [2011] - [2015] ET Marlabs 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of ET Marlabs and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to ET Marlabs
* and its suppliers and may be covered Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from ET Marlabs.
*
*/

/**
* @author ET Marlabs
* @date 20-Oct-2015
* @description This is a Helper Class for PeriodTrigger.
*/
public with sharing class PeriodTriggerHelper {
	
	/*-- SINGLETON PATTERN --*/
    private static PeriodTriggerHelper instance;
    public static PeriodTriggerHelper getInstance() {
        if (instance == null) {
            instance = new PeriodTriggerHelper();  
        }
        return instance;
    }

    /*******************************************************************************************************
    * @description :Method to execute before updation of period. If period is decativated it shouldn't be
					updated.
    * @param : List<SandDV1__Period__c> updatedPeriodsList
    * @return : null
    */

	public void onBeforeUpdate(List<SandDV1__Period__c> updatedPeriodsList) {

		for(SandDV1__Period__c period:updatedPeriodsList){

			if(!period.SandDV1__Active__c){
				period.adderror('Decativated period cannot be modified');
			}
			
		}
	}


    /***************************************************************************************
    * @description: This method checks that two sub periods under a master period are not overlapping

    * @param: List<SandDV1__Period__c>
    @return: null
    */

    public static void checkIfTheSubPeriodsAreOverlapping(List<SandDV1__Period__c> periodsList){
        Set<Id> masterPeriodsIdSet = new Set<Id>();
        for(SandDV1__Period__c period :periodsList){
            if(period.SandDV1__Master_Period__c != null){
                masterPeriodsIdSet.add(period.SandDV1__Master_Period__c);
            }
        }

        if(masterPeriodsIdSet.size() > 0){
            Map<Id, List<SandDV1__Period__c>> subPeriodsMap = PeriodService.getSubPeriodsMap(masterPeriodsIdSet);
            
            for(SandDV1__Period__c period :periodsList){
                if(subPeriodsMap.containsKey(period.SandDV1__Master_Period__c)){
                    for(SandDV1__Period__c subPeriod :subPeriodsMap.get(period.SandDV1__Master_Period__c)){
                        if(period.Id != subPeriod.Id){
                            if(period.SandDV1__Start_Date__c <= subPeriod.SandDV1__End_Date__c && period.SandDV1__End_Date__c >= subPeriod.SandDV1__Start_Date__c){
                                period.addError(Label.OverlappingSubPeriodsError);
                            } 
                        }
                    }
                } 
            }
        }
    }
}