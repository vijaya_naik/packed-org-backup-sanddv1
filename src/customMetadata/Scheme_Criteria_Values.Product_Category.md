<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Product Category</label>
    <values>
        <field>ActiveAPI__c</field>
        <value xsi:type="xsd:string">IsActive</value>
    </values>
    <values>
        <field>ActiveApiValue__c</field>
        <value xsi:type="xsd:string">True</value>
    </values>
    <values>
        <field>CriteriaValue__c</field>
        <value xsi:type="xsd:string">Product Category</value>
    </values>
    <values>
        <field>FieldApi__c</field>
        <value xsi:type="xsd:string">Category__c</value>
    </values>
    <values>
        <field>IsAssignment__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>IsPickList__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>ObjectApi__c</field>
        <value xsi:type="xsd:string">Product2</value>
    </values>
    <values>
        <field>ReturnType__c</field>
        <value xsi:type="xsd:string">list</value>
    </values>
    <values>
        <field>SchemeType__c</field>
        <value xsi:type="xsd:string">search</value>
    </values>
</CustomMetadata>
